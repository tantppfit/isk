<?php

namespace App\Helpers;
use App\Models\Site;
use App\Models\PostSlider;
use App\Models\Media;
use Illuminate\Support\Facades\Storage;
use App\Intervention\Image\Filters\Filter;
use Image;
use KubAT\PhpSimple\HtmlDomParser;
use App\Helpers\LinkCard;
class Helper {
	public static function uploadMedia($image){
		$locate = 'media/';
		$extension = $image->getClientOriginalExtension();
		$imageName = $image->getClientOriginalName();
		$imageNamePath = time(). '_' . $imageName;
		$sizes = config('adiva.sizes_generate');
		$sizePaths = [];
		//ratio3x2
		$path = $locate .'ratio3x2/' . $imageNamePath;
		$imgObject = Image::make($image);
		$info = $imgObject->width() . 'x' . $imgObject->height() . ' - ' . self::formatSizeUnits($imgObject->fileSize()) . ' - ' .$image->getMimeType();
		$realWidth = $imgObject->width();
		$realHeight = $imgObject->height();
		if(($realWidth/3) <= ($realHeight/2)){
			$imgObject->fit($realWidth, round($realWidth*(2/3)));
		}else{
			$imgObject->fit(round($realHeight*(3/2)), $realHeight);
		}
		$sizePaths['ratio3x2'] = $path;
		Storage::disk('public')->put($path, $imgObject->stream($extension, 100)->__toString());	
		//ratio3x2
		//ratio4x3
		$path = $locate .'ratio4x3/' . $imageNamePath;
		$imgObject = Image::make($image);
		if(($realWidth/4) <= ($realHeight/3)){
			$imgObject->fit($realWidth, round($realWidth*(3/4)));
		}else{
			$imgObject->fit(round($realHeight*(4/3)), $realHeight);
		}
		$sizePaths['ratio4x3'] = $path;
		Storage::disk('public')->put($path, $imgObject->stream($extension, 100)->__toString());	
		//ratio4x3
		foreach ($sizes as $key => $size) {
			$path = $locate . $key .'/' . $imageNamePath;
			$imgObject = Image::make($image);
			$realWidth = $imgObject->width();
			if(!is_null($size['width']) && $size['width'] <= $realWidth ){
				if(is_null($size['height'])){
					$imgObject->filter(new Filter($size['width']));
				}else{
					$imgObject->fit($size['width'], $size['height']);
				}				
			}elseif(!is_null($size['width']) && $size['width'] > $realWidth){
				continue;
			}
			$sizePaths[$key] = $path;
			Storage::disk('public')->put($path, $imgObject->stream($extension, 100)->__toString());			 
		}
		return ['file_name' => $imageName, 'info' => $info, 'size_paths' => $sizePaths];
	}

	public static function getMediaUrl($media, $size='original'){
		if(isset($media->{$size.'_path'}) && !empty($media->{$size.'_path'})){
			$imagePath = Storage::url($media->{$size.'_path'});
		}else{
			$imagePath = Storage::url($media->original_path);
		}
		return $imagePath;
	}

	public static function getMediaUrlById($id, $size='original'){
		$media = Media::where('id', $id)->first();
		if($media){
			if(isset($media->{$size.'_path'}) && !empty($media->{$size.'_path'})){
				$imagePath = Storage::url($media->{$size.'_path'});
			}else{
				$imagePath = Storage::url($media->original_path);
			}
		}else{
			$imagePath = '';
		}
		return $imagePath;
	}

	public static function deleteMedia($media){
		$sizes = config('adiva.sizes_generate');
		$imagePaths = [];
		foreach ($sizes as $key => $size) {		
			$imagePaths[] = $media->{$key.'_path'};
		}
		Storage::disk('public')->delete($imagePaths);
		return true;
	}
	public static function uploadAvatar($image){
		$locate = 'media/';
		$extension = $image->getClientOriginalExtension();
		$imageName = $image->getClientOriginalName();
		$imageNamePath = time(). '_' . $imageName;
		$path = $locate . $imageNamePath;
		$imgObject = Image::make($image);
		$imgObject->fit(150, 150);
		Storage::disk('public')->put($path, $imgObject->stream($extension, 100)->__toString());
		return $path;
	}
	public static function uploadSlideImages($images, $post_id){
		$locate = 'slide/' . $post_id .'/';
		$paths = [];
		foreach ($images as $image) {
			$extension = $image->getClientOriginalExtension();
			$imageName = $image->getClientOriginalName();
			$imageNamePath = time(). '_' . $imageName;
			$path = $locate . $imageNamePath;
			$imgObject = Image::make($image);
			Storage::disk('public')->put($path, $imgObject->stream($extension, 100)->__toString());
			$paths[] = $path;
		}
		return $paths;
	}

	public static function getImageUrl($path){
			$imagePath = Storage::url($path);
		return $imagePath;
	}

	public static function deleteImage($path){
		Storage::disk('public')->delete($path);
		return true;
	}

	public static function getUserAvatarUrl($path){
		$imagePath = Storage::url($path);
		return $imagePath;
	}

	public static function formatSizeUnits($bytes)
  {
    if ($bytes >= 1073741824)
    {
      $bytes = number_format($bytes / 1073741824, 2) . ' GB';
    }
    elseif ($bytes >= 1048576)
    {
      $bytes = number_format($bytes / 1048576, 2) . ' MB';
    }
    elseif ($bytes >= 1024)
    {
      $bytes = number_format($bytes / 1024, 2) . ' KB';
    }
    elseif ($bytes > 1)
    {
      $bytes = $bytes . ' bytes';
    }
    elseif ($bytes == 1)
    {
      $bytes = $bytes . ' byte';
    }
    else
    {
      $bytes = '0 bytes';
    }
    return $bytes;
	}

	public static function getPostSummary($content){
		$summaryText = '';
		$html = HtmlDomParser::str_get_html($content);
		if($html){
			$summaryTag = $html->find('.summary',0);
			if($summaryTag){
				$summaryText = $summaryTag->innertext;
			}
		}
		return $summaryText;
	}
	public static function generatePostContent($content){
		$html = HtmlDomParser::str_get_html($content);
		if($html){
			$tabTotal = count($html->find('.break-page'));
			if($tabTotal > 0){
				$html = self::generatePaginationPostContent($html, $tabTotal);
			}
			$html = self::generateLinkCardContent($html);
			$imgs = $html->find('img');
			foreach($imgs as $img) {
	      $img->setAttribute('alt', '');
			}
			$sliders = $html->find('.post-slider');
			foreach($sliders as $slider) {
				$id = $slider->getAttribute ('data-id');
				$slider_html = self::getContentSlider($id);
	      $slider->innertext = $slider_html;
			}
			return $html;
		}else{
			return $html;
		}
		
	}

	public static function generateLinkCardContent($html){
		foreach ($html->find('*') as $key => $element) {
			$string = $element->plaintext;
			if(preg_match('/^\[linkcard url="(.+)" \/]/', $string, $match)){
				if(isset($match[1]) && str_replace(' ', '', $match[1]) !== ''){
					$url = str_replace(' ', '', $match[1]);
					$linkCard = LinkCard::getHtml($url);
					$element->outertext = $linkCard;
				}
			}
		}
		return $html;
	}
	public static function generatePaginationPostContent($html, $tabTotal){
		$tabContents = $tabLinks = $tabContent = [];
		$tabCount = 1;
		$elements = $html->find('*');
		foreach ($html->find('*') as $key => $element) {
			if(!($element->parent())->parent()){
				$classAttr = $element->class;
				$classes = explode(" ",$classAttr);
				if(in_array('break-page', $classes)){
					$activeTab = ($tabCount == 1) ? ' active' : '';
					$tabContents[] = '<div class="tab-content '. $activeTab .'" data-tab="'.$tabCount.'">'. implode('', $tabContent) .'</div>';
					$tabContent = [];
					$tabCount++;
				}else{
					$tabContent[] = $element->outertext;
					if(($key+1) == count($html->find('*'))){
						$tabContents[] = '<div class="tab-content" data-tab="'.$tabCount.'">'. implode('', $tabContent) .'</div>';
					}
				}
			}else{
				if(($key+1) == count($html->find('*'))){
					$tabContents[] = '<div class="tab-content" data-tab="'.$tabCount.'">'. implode('', $tabContent) .'</div>';
				}
			}
		}
		for ($i=1; $i <= $tabCount ; $i++) {
			$activeTab = ($i == 1) ? 'active' : '';
			$tabLinks[] = '<a href="#" class="'.$activeTab.'" data-tab="'. $i .'">'. $i .'</a>';
		}
		return HtmlDomParser::str_get_html('<div class="tab-contents">'. implode('', $tabContents) .'</div><div class="tab-links">'. implode('', $tabLinks) .'</div>');
	}

	public static function getContentSlider($id){
		$slider = PostSlider::find($id);
		if($slider && is_array(json_decode($slider->slide_images, true))){
			$slide_images = json_decode($slider->slide_images, true);
			$li = [];
			foreach($slide_images as $key => $slide_image){
				if(Media::where('id', $key)->exists()){
					$li[] = '<li><img src="'.$slide_image.'" /></li>';
				}
			}
			/*$html = '<div class="post-slide-images" data-slider-id="'.$id.'">
									<div id="slider-'.$id.'" class="flexslider slider">
									  <ul class="slides">'. implode('', $li) .'</ul>
									</div>
									<div class="wrap-control">
										<div id="carousel-'.$id.'" class="flexslider carousel">
										  <ul class="slides">'. implode('', $li) .'</ul>
										</div>
										<div class="slider-pauseplay">
											<a href="#" class="prev-slide" data-slider-id="'.$id.'"><i class="fas fa-step-backward"></i></a>
											<a href="#" class="pauseplay-slide" data-slider-id="'.$id.'"><i class="fas fa-play"></i></a>
										</div>
									</div>
								</div>';*/
			$html = '<div class="post-slide-images" data-slider-id="'.$id.'">
									<div id="slider-'.$id.'" class="flexslider slider">
									  <ul class="slides">'. implode('', $li) .'</ul>
									</div>
									<div class="wrap-control">
										<div id="carousel-'.$id.'" class="flexslider carousel">
										  <ul class="slides">'. implode('', $li) .'</ul>
										</div>
									</div>
								</div>';
			return $html;
		}else{
			return '';
		}
	}

	//generate slug
	public static function _toSlugTransliterate($string) {
    // Lowercase equivalents found at:
    // https://github.com/kohana/core/blob/3.3/master/utf8/transliterate_to_ascii.php
    $lower = [
        'à'=>'a','ô'=>'o','ď'=>'d','ḟ'=>'f','ë'=>'e','š'=>'s','ơ'=>'o',
        'ß'=>'ss','ă'=>'a','ř'=>'r','ț'=>'t','ň'=>'n','ā'=>'a','ķ'=>'k',
        'ŝ'=>'s','ỳ'=>'y','ņ'=>'n','ĺ'=>'l','ħ'=>'h','ṗ'=>'p','ó'=>'o',
        'ú'=>'u','ě'=>'e','é'=>'e','ç'=>'c','ẁ'=>'w','ċ'=>'c','õ'=>'o',
        'ṡ'=>'s','ø'=>'o','ģ'=>'g','ŧ'=>'t','ș'=>'s','ė'=>'e','ĉ'=>'c',
        'ś'=>'s','î'=>'i','ű'=>'u','ć'=>'c','ę'=>'e','ŵ'=>'w','ṫ'=>'t',
        'ū'=>'u','č'=>'c','ö'=>'o','è'=>'e','ŷ'=>'y','ą'=>'a','ł'=>'l',
        'ų'=>'u','ů'=>'u','ş'=>'s','ğ'=>'g','ļ'=>'l','ƒ'=>'f','ž'=>'z',
        'ẃ'=>'w','ḃ'=>'b','å'=>'a','ì'=>'i','ï'=>'i','ḋ'=>'d','ť'=>'t',
        'ŗ'=>'r','ä'=>'a','í'=>'i','ŕ'=>'r','ê'=>'e','ü'=>'u','ò'=>'o',
        'ē'=>'e','ñ'=>'n','ń'=>'n','ĥ'=>'h','ĝ'=>'g','đ'=>'d','ĵ'=>'j',
        'ÿ'=>'y','ũ'=>'u','ŭ'=>'u','ư'=>'u','ţ'=>'t','ý'=>'y','ő'=>'o',
        'â'=>'a','ľ'=>'l','ẅ'=>'w','ż'=>'z','ī'=>'i','ã'=>'a','ġ'=>'g',
        'ṁ'=>'m','ō'=>'o','ĩ'=>'i','ù'=>'u','į'=>'i','ź'=>'z','á'=>'a',
        'û'=>'u','þ'=>'th','ð'=>'dh','æ'=>'ae','µ'=>'u','ĕ'=>'e','ı'=>'i',
    ];
    return str_replace(array_keys($lower), array_values($lower), $string);
	}
	public static function toSlug($string, $separator = '-') {
	    // Work around this...
	    #$string = iconv('UTF-8', 'ASCII//TRANSLIT//IGNORE', $string);
	    $string = self::_toSlugTransliterate($string);

	    // Remove unwanted chars + trim excess whitespace
	    // I got the character ranges from the following URL:
	    // https://stackoverflow.com/questions/6787716/regular-expression-for-japanese-characters#10508813
	    $regex = '/[^一-龠ぁ-ゔァ-ヴーａ-ｚＡ-Ｚ０-９a-zA-Z0-9々〆〤.+ -]|^\s+|\s+$/u';
	    $string = preg_replace($regex, '', $string);

	    // Using the mb_* version seems safer for some reason
	    $string = mb_strtolower($string);

	    // Same as before
	    $string = preg_replace("/[ {$separator}]+/", $separator, $string);
	    return $string;
	}

	public static function getDefaultCover($post=null){
		if(is_null($post))
			return false;
		$site = Site::find($post->site_id);
		if($site && !empty($site->cover_image_url)){
			return $site->cover_image_url;
		}else{
			return false;
		}
	}

	public static function api_curl_connect( $api_url ) {

		$connection_c = curl_init(); // initializing

		curl_setopt( $connection_c, CURLOPT_URL, $api_url ); // API URL to connect

		curl_setopt( $connection_c, CURLOPT_RETURNTRANSFER, 1 ); // return the result, do not print

		curl_setopt( $connection_c, CURLOPT_TIMEOUT, 20 );

		$json_return = curl_exec( $connection_c ); // connect and get json data

		curl_close( $connection_c ); // close connection

		

		return json_decode( $json_return, true ); // decode and return

	}

	public static function get_instagram_feed( $number = null ) {
		$access_token = '7019285044.1677ed0.8001503a018c47dd8f938d187832567e';
		$return = self::api_curl_connect( "https://api.instagram.com/v1/users/self/media/recent/?access_token=" . $access_token );
		if(isset($return['data'])) {
			if(is_null($number)){
				return $return['data'];
			}else{
				return array_slice($return['data'], 0, $number);
			}
		} else {
			return null;
		}
	}

}