<?php

namespace App\Policies;

use App\Models\User;
use App\Policies\AbstractPolicy\AbstractPolicy;

class UserPolicy extends AbstractPolicy {
    protected $model = 'user';

    public function index(User $user) {
        return $user->ability('admin.' . $this->model . '.index');
    }

    public function indexOfficialWriter(User $user) {
        return $user->ability('admin.' . $this->model . '.indexofficialwriter');
    }

    public function edit(User $user, $item) {
        if($user->role->slug == 'admin'){
            return $user->ability('admin.' . $this->model . '.edit');
        }else{
            return ($item->id == $user->id) ? true : false;
        }
    }

    public function create(User $user) {

        return $user->ability('admin.' . $this->model . '.create');
    }

    public function update(User $user, $item) {
        if($user->role->slug == 'admin'){
            return $user->ability('admin.' . $this->model . '.update');
        }else{
            return ($item->id == $user->id) ? true : false;
        }
    }

    public function update_avatar(User $user, $item) {
        if($user->role->slug == 'admin'){
            return $user->ability('admin.' . $this->model . '.update_avatar');
        }else{
            return ($item->id == $user->id) ? true : false;
        }        
    }

    public function store(User $user) {
        return $user->ability('admin.' . $this->model . '.store');
    }

    public function destroy(User $user, $item) {
        if($user->role->slug == 'admin'){
            if($user->site->user_id == $item->id || $user->id == $item->id){
                return false;
            }else{
                return $user->ability('admin.' . $this->model . '.destroy');
            }
        }else{
            return $user->ability('admin.' . $this->model . '.destroy');
        }
        
    }
}
