<?php

namespace App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;
use Kreait\Firebase;
use Kreait\Firebase\Factory;
use Kreait\Firebase\ServiceAccount;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\Hash;
use App\Rules\OldPassword;
use App\Rules\UserEmail;
use App\Models\User;
use App\Models\Genre;
use App\Models\Role;
use App\Models\PostView;
use Helper;
use Auth;

class UserController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    protected $userModel;
    protected $roleModel;
    protected $postViewModel;
    protected $genreModel;
    protected $userRepository;
    protected $database;
    protected $firebase;
    public function __construct(User $userModel, Role $roleModel, PostView $postViewModel, Genre $genreModel)
    {
        $this->userModel = $userModel;
        $this->roleModel = $roleModel;
        $this->postViewModel = $postViewModel;
        $this->genreModel = $genreModel;
        
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index(Request $request)
    {
        $this->authorize('index', $this->userModel);
        $users = $this->userModel->getUsers($request);
        if ($request->sort == 'created_at' || $request->sort == 'updated_at') {
            $users = $this->userModel->getUsers($request);
        }
        $genres = $this->genreModel->all();
        // echo $users;die();
        //$users  = user::orderBy('created_at', 'desc')->paginate(5);
        return view('admin.templates.user.index')->with([
            'users' => $users,
            'genres' => $genres
        ]);
    }

    public function indexOfficialWriter(Request $request)
    {
        $this->authorize('indexOfficialWriter', $this->userModel);
        $users = $this->userModel->getOfficialWriters();
        return view('admin.templates.user.index_official_writer')->with([
            'users' => $users
        ]);
    }
    public function store(Request $request) {
        $this->authorize('store', $this->userModel);
        $this->validate($request, [
            'name' => 'required',
            'genre' => 'required'
        ],[],[
            'name' =>  trans('table_fields.users.name'),
            'genre' =>  trans('table_fields.users.genre')
        ]);
        $pwd_random = str_random(10);
        $user = User::Create(
            [
                'name' => $request->name,
                'genre' => $request->genre,
                'password' => Hash::make($pwd_random),
                'password_end'  => base64_encode($pwd_random)
            ]
        );
        // print_r($user->password);
        // die();
        return redirect()->route('admin.user.index')->with('success', trans('messages.success.create', ['Module' => trans('module.entry.user')]));
    }

    public function update(Request $request, $id)
    {
        $user = $this->userModel->findOrFail($id);
        $this->authorize('update', $user);
        $postData = $request->only([
            'name',
            'genre'
        ]);
        if ($request->has('name') && empty($request->name)) {
            $postData['name'] = $user->name;
        }
        if ($request->has('genre') && empty($request->genre)) {
            $postData['genre'] = $user->genre;
        }
        // print_r(decrypt($user->password));
        // die();
        $user->update($postData);
        return redirect()->route('admin.user.index')->with('success', trans('messages.success.update', ['Module' => trans('module.entry.user')]));
    }
    public function updateAvatar(Request $request, $id)
    {
        $this->validate($request, [
            'file' => 'image|required|mimes:jpeg,png,jpg'
        ],[],[
            'file' =>  trans('table_fields.users.file')
        ]);
        $user = $this->userModel->findOrFail($id);
        $this->authorize('update_avatar', $user);
        if($user->avatar){
            Helper::deleteImage($user->avatar);
        }
        $path = Helper::uploadAvatar($request->file);
        $user->avatar = $path;
        $user->save();
        return redirect()->route('admin.user.edit', [$user])->with('success', trans('messages.success.update', ['Module' => trans('module.entry.user')]));
    }
    public function getViewLog(Request $request, $id){
        $user = $this->userModel->findOrFail($id);
        $userStatistic = $this->postViewModel->getViewLogByUser($user);
        return response()->json([
            'status' => true,
            'data' => $userStatistic
        ]);
    }
    public function destroy($id)
    {
        $user = $this->userModel->findOrFail($id);
        $this->authorize('destroy', $user);
        $this->userModel->destroy($id);   
        $serviceAccount = ServiceAccount::fromJsonFile(__DIR__.'/isk-chat-firebase-adminsdk-ci29k-6457f7bed0.json');
        $firebase = (new Factory)
            ->withServiceAccount($serviceAccount)
            ->withDatabaseUri('https://isk-chat.firebaseio.com/')
            ->create();
        $database = $firebase->getDatabase();     
        $deleteUser = $database
        ->getReference('Messagers/'.$id)
        ->remove();
        return redirect()->route('admin.user.index')->with('success', trans('messages.success.delete', ['Module' => trans('module.entry.user')]));
    }
    // public function create(Request $request)
    // {
    //     $this->authorize('create', $this->userModel);
    //     $roles = $this->roleModel->all();
    //     return view('admin.templates.user.create')->with([
    //         'roles' => $roles
    //     ]);
    // }

    // public function store(Request $request)
    // {
    //     $this->authorize('store', $this->userModel);
    //     $this->validate($request, [
    //         'email' => ['required','email', new UserEmail()],
    //         'password' => 'Hash::make(str_random(10))',
    //         'role_id' => 'required',
    //         'file' => 'image|mimes:jpeg,png,jpg'
    //     ],[],[
    //         'email' =>  trans('table_fields.users.email'),
    //         'password' =>  trans('table_fields.users.password'),
    //         'role_id' =>  trans('table_fields.users.role_id'),
    //         'file' =>  trans('table_fields.users.file'),
    //     ]);
    //     $postData = $request->only(
    //         'email',
    //         'name',
    //         'receive_notification',
    //         'writer_profile_html',
    //         'role_id',
    //         'disable_article_publishing'
    //     );
    //     print_r($postData);
    //     die();
    //     if($request->has('password') && !empty($request->password)){
    //         $postData['password'] = bcrypt($request->password);
    //     }
    //     $postData['site_id'] = Auth::user()->site_id;
    //     $user = $this->userModel->create($postData);
    //     if($request->has('file')){
    //         $path = Helper::uploadAvatar($request->file);
    //         $user->avatar = $path;
    //         $user->save();
    //     }        
    //     return redirect()->route('admin.user.index')->with('success', trans('messages.success.create', ['Module' => trans('module.entry.user')]));
    // }
    
    

    // public function testDecrypt()
    // {
    //     $string = 'eyJpdiI6InpVMFwvb2Z0M3lWSGs0cklpaTZ5cG5BPT0iLCJ2YWx1ZSI6IjNJNTlcLzlRclUxRTh6UHdvcWowazJOQWxBandrMHVtMmt0Uk5DY0FsXC9SZz0iLCJtYWMiOiIwMjM0MWY5NTVkODRlMmNiNWVmOWVkZDRiMjZkMjU2MjE3NmQzNzE4MDA5MTFkMGRlOWNjM2NmZDgzOTdlYzgxIn0';
    //     return decrypt($string);
    // }

    // public function decrypt(Request $request, $id)
    // {
    //     $user = $this->userModel->findOrFail($id);
    //     return decrypt($user->password);
    //     print_r($user);
    //     die();
    // }

    // public function edit(Request $request, $id)
    // {
    //     $user = $this->userModel->findOrFail($id);
    //     $this->authorize('edit', $user);
    //     $roles = $this->roleModel->all();
    //     return view('admin.templates.user.edit')->with([
    //         'user' => $user,
    //         'roles' => $roles
    //     ]);
    // }

    // public function update(Request $request, $id)
    // {
    //     $user = $this->userModel->findOrFail($id);
    //     $this->authorize('update', $user);
    //     if($request->has('password') && empty($request->password)){
    //         unset($request['password']);
    //         unset($request['current_password']);
    //     }
    //     if(!$request->has('receive_notification')){
    //         $request->merge(['receive_notification'=>false]);
    //     }
    //     if(!$request->has('disable_article_publishing')){
    //         $request->merge(['disable_article_publishing'=>false]);
    //     }
    //     $this->validate($request, [
    //         'email' => ['required','email', new UserEmail($user)],
    //         'current_password' => ['required_with:password', new OldPassword($user)],
    //         'password' => 'nullable|confirmed|min:6',
    //     ],[],[
    //         'email' =>  trans('table_fields.users.email'),
    //         'current_password' =>  trans('table_fields.users.current_password'),
    //         'password' =>  trans('table_fields.users.password')
    //     ]);
    //     $postData = $request->only(
    //         'email',
    //         'name',
    //         'receive_notification',
    //         'writer_profile_html',
    //         'role_id',
    //         'disable_article_publishing'
    //     );
    //     if($request->has('password') && !empty($request->password)){
    //         $postData['password'] = bcrypt($request->password);
    //     }
    //     $user->update($postData);
    //     return redirect()->route('admin.user.edit', [$user])->with('success', trans('messages.success.update', ['Module' => trans('module.entry.user')]));
    // }
}
