<?php

namespace App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use App\Models\Tag;
use App\Models\MenuTag;

class TagController extends Controller
{
    protected $tagModel;
    protected $menuTagModel;
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct( Tag $tagModel, MenuTag $menuTagModel )
    {
        $this->tagModel = $tagModel;
        $this->menuTagModel = $menuTagModel;
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index(Request $request)
    {
        $this->authorize('index', $this->tagModel);
        $tags = $this->tagModel->orderBy('order', 'asc')->get();
        $menuTags = $this->menuTagModel->orderBy('order', 'asc')->get();
        return view('admin.templates.tag.index')->with([
            'tags' => $tags,
            'menuTags' => $menuTags
        ]);
    }
    public function store(Request $request)
    {
        $this->authorize('store', $this->tagModel);
        $this->validate($request, [
          'name' => 'required',
        ],[],[
            'name' =>  trans('table_fields.tags.name')
        ]);
        $tagNames = preg_split('/[,、]/', $request->name);
        $tagNames = array_filter($tagNames, function($value) { return !is_null($value) && $value !== ''; });
        $tags = $this->tagModel->createTags($tagNames);
        if($request->has('redirect')){
            return redirect($request->redirect)->with('success', trans('messages.success.create', ['Module' => trans('module.entry.tag')]));
        }
        return redirect()->route('admin.tag.index')->with('success', trans('messages.success.create', ['Module' => trans('module.entry.tag')]));
    }
    public function edit(Request $request, $id)
    {
        $tag = $this->tagModel->findOrFail($id);
        $this->authorize('edit', $tag);
        return view('admin.templates.tag.edit')->with([
            'tag' => $tag
        ]);
    }
    public function update(Request $request, $id)
    {
        $tag = $this->tagModel->findOrFail($id);
        $this->authorize('update', $tag);
        $this->validate($request, [
          'name' => 'required|unique:tags,name,'.$tag->id,
        ],[],[
            'name' =>  trans('table_fields.tags.name')
        ]);
        $postData = $request->except(['_token']);
        $tag->update($postData);
        return redirect()->route('admin.tag.edit', $tag)->with('success', trans('messages.success.update', ['Module' => trans('module.entry.tag')]));
    }
    public function destroy($id)
    {
        $tag = $this->tagModel->findOrFail($id);
        $this->authorize('destroy', $tag);
        $order = $tag->order;
        $this->tagModel->destroy($id);
        foreach ($this->tagModel->where('order', '>', $order)->cursor() as $record) {
            $order = $record->order - 1;
            $record->order = $order;
            $record->save();
        }
        return redirect()->route('admin.tag.index')->with('success', trans('messages.success.delete', ['Module' => trans('module.entry.tag')]));
    }

    /*
    * $action: increase, decrease
    */
    public function reOrder($id, $action)
    {
        $tag = $this->tagModel->findOrFail($id);
        $this->authorize('reorder', $tag);
        $oldOrder = $tag->order;
        if($action == 'increase'){
            $newOrder = $oldOrder - 1;
        }else{
            $newOrder = $oldOrder + 1;
        }
        foreach ($this->tagModel->where('order', $newOrder)->cursor() as $record) {
            $record->order = $oldOrder;
            $record->save();
        }
        $tag->order = $newOrder;
        $tag->save();
        return redirect()->route('admin.tag.index')->with('success', trans('messages.success.update', ['Module' => trans('module.entry.tag')]));
    }
}
