<?php

namespace App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Crypt;
use App\Rules\OldPassword;
use App\Rules\UserEmail;
use App\Models\User;
use App\Models\Genre;
use App\Models\Role;
use App\Models\PostView;
use App\Models\ChatManage;
use Helper;
use Auth;

class ChatManageController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    protected $chatmanageModel;
    protected $userModel;
    protected $roleModel;
    protected $postViewModel;
    protected $genreModel;
    public function __construct(User $userModel, Role $roleModel, PostView $postViewModel, Genre $genreModel, ChatManage $chatmanageModel)
    {
        $this->chatmanageModel  = $chatmanageModel;
        $this->userModel        = $userModel;
        $this->roleModel        = $roleModel;
        $this->postViewModel    = $postViewModel;
        $this->genreModel       = $genreModel;
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index(Request $request)
    {
        $this->authorize('index', $this->userModel);
        $users = $this->userModel->getUsers($request);
        $genres = $this->genreModel->all();
        // echo $users;die();
        //$users  = user::orderBy('created_at', 'desc')->paginate(5);
        return view('admin.chatmanage.index')->with([
            'users' => $users,
            'genres' => $genres
        ]);
    }
}
