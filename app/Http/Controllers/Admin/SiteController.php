<?php

namespace App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;

use App\Models\Site;
use App\Models\Media;
use Helper;

class SiteController extends Controller
{
    protected $siteModel;
    protected $mediaModel;
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(Site $siteModel, Media $mediaModel)
    {
        $this->siteModel = $siteModel;
        $this->mediaModel = $mediaModel;
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function edit(Request $request, $id)
    {
        $site = $this->siteModel->findOrFail($id);
        $this->authorize('edit', $site);
        return view('admin.templates.site.edit')->with(['site'=>$site]);
    }

    public function update(Request $request, $id)
    {
        $site = $this->siteModel->findOrFail($id);
        $this->authorize('update', $site);
        $postData = $request->only(
            'title',
            'description',
            'cover_image_url'
        );
        $site->update($postData);
        return redirect()->route('admin.site.edit', [$site])->with('success', '編集しました。');
    }
}
