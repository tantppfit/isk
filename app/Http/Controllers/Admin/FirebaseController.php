<?php

namespace App\Http\Controllers\Admin;


use App\Http\Controllers\Controller;
use Kreait\Firebase;
use Kreait\Firebase\Factory;
use Kreait\Firebase\ServiceAccount;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\Storage;
use App\Rules\OldPassword;
use App\Rules\UserEmail;
use App\Models\User;
use App\Models\Genre;
use App\Models\Group;
use App\Models\Role;
use App\Models\PostView;
use App\Models\ChatManage;
use App\Models\FileManage;
use App\Models\Media;
use App\Models\Post;
use DB;
use Helper;
use Auth;
use Mail;

class FirebaseController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    protected $chatmanageModel;
    protected $userModel;
    protected $roleModel;
    protected $postViewModel;
    protected $genreModel;
    protected $fileModel;
    protected $groupModel;
    
    public function __construct(User $userModel, Role $roleModel, PostView $postViewModel, Genre $genreModel, ChatManage $chatmanageModel,  FileManage $fileModel, Media $mediaModel, Group $groupModel)
    {
        $this->chatmanageModel  = $chatmanageModel;
        $this->userModel        = $userModel;
        $this->roleModel        = $roleModel;
        $this->postViewModel    = $postViewModel;
        $this->genreModel       = $genreModel;
        $this->fileModel        = $fileModel;
        $this->mediaModel       = $mediaModel;
        $this->groupModel       = $groupModel;
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index(Request $request)
    {
        $this->authorize('index', $this->userModel);
        // $users = $this->userModel->getUsers($request);
        $searchresult = User::orderByDesc('updated_at')->get();
        $genres = Genre::orderByDesc('updated_at')->get();
        $groups = $this->groupModel->all();
        // echo $users;die();
        //$users  = user::orderBy('created_at', 'desc')->paginate(5);
        return view('admin.chatmanage.index')->with([
            //'users'             => $users,
            'searchresult'      => $searchresult,
            'genres'            => $genres,
            'groups'            => $groups
        ]);
    }

    public function search(Request $request)
    {
        if ($request->ajax()) {
            $output = '';
            // $users = DB::table('users')->where('name','LIKE','%' . $request->search . '%')->get();
            // $genres = DB::table('genre')->where('name', 'LIKE', '%' . $request->search . '%')->get();
            // $datas = $users->merge($genres);
            // $datas = $this->userModel->getUsersBySearch($request);
            // $datas = DB::table('users')->join('genre', 'users.genre', '=', 'genre.id')
            //                            ->where('users.name', 'like', '%' . $request->search . '%')
            //                            ->orWhere('genre.name', 'like', '%' . $request->search . '%')
            //                            ->select('users.*')
            //                            // where('name', 'like', '%' . $request->search . '%')
            //                            ->get();
           $datas = DB::table('users')->where('users.name', 'like', '%' . $request->search . '%')
                                       ->select('users.*')
                                       // where('name', 'like', '%' . $request->search . '%')
                                       ->get();
           $listGenres = DB::table('genre')->where('genre.name', 'like', '%' . $request->search . '%')->select('genre.*')
                                       // where('name', 'like', '%' . $request->search . '%')
                                       ->get();
           $groups = $this->groupModel->all();
            if ($datas) {
                foreach ($datas as $key => $data) {
                    if ($data->id != '10001') {
                        $output .= '<tr style="height: 30px;">' . 
                                        '<td>' . '<a class="user-chat" href="/admin/chatmanage/chatfirebase/'.$data->id.'">'.
                                            '<div class="float-left"><img id="changeimg" src="/storage/avatar/default1.png" style="border-radius:25px;height: auto;width: 45px; object-fit: cover;"></div>' .
                                            '<div class="float-left text-left" style="margin-left:3%;">'.$data->name.
                                                '<p style="color: black;">' . substr($data->created_at, 0, -9) . '</p>' . 
                                            '</div>' .
                                            '<input style="float: right;margin: 6% 3%;transform: scale(2);" class="file-checkbox" type="checkbox" data-file-id="'.$data->id .'" name="">' .'</a>'.
                                        '</td>' .
                                    '</tr>';
                    }
                }
            }
            if ($listGenres) {
                foreach ($listGenres as $key => $genre) {
                    $output .= '<tr style="height: 30px;">' . 
                                    '<td>' . '<a class="user-chat" style="color: #007bff;">'.
                                        '<div class="float-left"><img id="changeimg" src="/storage/avatar/default2.png" style="border-radius:25px;height: auto;width: 45px; object-fit: cover;"></div>' .
                                        '<div class="float-left text-left" style="margin-left:3%;">'.$genre->name.
                                            '<p style="color: black;">' . substr($genre->updated_at, 0, -9) . '</p>' . 
                                        '</div>' .
                                        '<input style="float: right;margin: 6% 3%;transform: scale(2);" class="file-checkbox" type="checkbox" data-file-id="'. $genre->id .'" name="">' .'</a>'.
                                    '</td>' .
                                '</tr>';
                }
            }
            if ($groups) {
                foreach ($groups as $key => $group) {
                    $output .= '<tr style="height: 30px;">' . 
                                    '<td>' . '<a class="user-chat" style="color: #007bff;">'.
                                        '<div class="float-left"><img id="changeimg" src="/storage/avatar/default2.png" style="border-radius:25px;height: auto;width: 45px; object-fit: cover;"></div>' .
                                        '<div class="float-left text-left" style="margin-left:3%;">'.$group->name.
                                            '<p style="color: black;">' . substr($group->updated_at, 0, -9) . '</p>' . 
                                        '</div>' .
                                        '<input style="float: right;margin: 6% 3%;transform: scale(2);" class="file-checkbox" type="checkbox" data-file-id="'. $group->listid .'" name="">' .'</a>'.
                                    '</td>' .
                                '</tr>';
                }
            }
            // echo $output;
            return Response($output);
        }
    }
    public function searchdetail(Request $request)
    {
        if ($request->ajax()) {
            $output = '';
           $datas = DB::table('users')->where('users.name', 'like', '%' . $request->search . '%')
                                       ->select('users.*')
                                       // where('name', 'like', '%' . $request->search . '%')
                                       ->get();
           $listGenres = DB::table('genre')->where('genre.name', 'like', '%' . $request->search . '%')->select('genre.*')
                                       // where('name', 'like', '%' . $request->search . '%')
                                       ->get();
           $groups = $this->groupModel->all();
            if ($datas) {
                foreach ($datas as $key => $data) {
                    if ($data->id != '10001') {
                        $output .= '<tr style="height: 30px;">' . 
                                        '<td>' . '<a class="user-chat" href="/admin/chatmanage/chatfirebase/'.$data->id.'">'.
                                            '<div class="float-left"><img id="changeimg" src="/storage/avatar/default1.png" style="border-radius:25px;height: auto;width: 45px; object-fit: cover;"></div>' .
                                            '<div class="float-left text-left" style="margin-left:3%;">'.$data->name.
                                                '<p style="color: black;">' . substr($data->created_at, 0, -9) . '</p>' . 
                                            '</div>' .'</a>'.
                                        '</td>' .
                                    '</tr>';
                    }
                }
            }
            // if ($listGenres) {
            //     foreach ($listGenres as $key => $genre) {
            //         $output .= '<tr style="height: 30px;">' . 
            //                         '<td>' . 
            //                             '<div class="float-left"><img id="changeimg" src="/storage/avatar/default.png" style="border-radius:25px;height: auto;width: 45px; object-fit: cover;"></div>' .
            //                             '<div class="float-left text-left">'.'<a class="user-chat" href="">'.$genre->name.'</a>'.
            //                                 '<p style="color: black;">' . substr($genre->updated_at, 0, -9) . '</p>' . 
            //                             '</div>' .
            //                         '</td>' .
            //                     '</tr>';
            //     }
            // }
            // if ($groups) {
            //     foreach ($groups as $key => $group) {
            //         $output .= '<tr style="height: 30px;">' . 
            //                         '<td>' . 
            //                             '<div class="float-left"><img id="changeimg" src="/storage/avatar/default.png" style="border-radius:25px;height: auto;width: 45px; object-fit: cover;"></div>' .
            //                             '<div class="float-left text-left">'.'<a class="user-chat" href="">'.$group->name.'</a>'.
            //                                 '<p style="color: black;">' . substr($group->updated_at, 0, -9) . '</p>' . 
            //                             '</div>' .
            //                         '</td>' .
            //                     '</tr>';
            //     }
            // }
            // echo $output;
            return Response($output);
        }
    }

    public function chatfirebase(Request $request, $id)
    {     
        $chatData = $request->all();
        if ($chatData == null) {
            $chatData['file'] = '';
            $chatData['text'] = '';
        } else {
            $chatData = $request->all();
        }
        $user = Auth::user(); 
        $_id = $id;
        //print_r($chatData);
        //die;
        //$jData = json_encode($chatData);
        //$data = json_decode($jData);
        $created_at = date("Y-m-d H:i:s",time());
        //$created_at = str_replace("T"," ",$data->created_at);
        $serviceAccount = ServiceAccount::fromJsonFile(__DIR__.'/isk-chat-firebase-adminsdk-ci29k-6457f7bed0.json');
        $firebase = (new Factory)
            ->withServiceAccount($serviceAccount)
            ->withDatabaseUri('https://isk-chat.firebaseio.com/')
            ->create();
        $database = $firebase->getDatabase();

        if ($chatData['file'] != null) {
            $this->validate($request, [
            'file' => 'required|file|mimes:pdf,docx,xlsx,pptx,csv|max:10240',
            ]);
            //print_r($request->file_url);
            //die;
            
            $image = $request->file('file');           
            $imageName = $image->getClientOriginalName();
            $imageFormat = $image->getClientOriginalExtension(); 
            $newName = time().'_file.'.$imageFormat;
            $destinationPath = 'media/'.$_id.'/file/'.$newName;
            $movePath = 'storage/media/'.$_id.'/file';
            $image->move($movePath, $newName);
            $attributes = [
                'name'          => $imageName,
                'original_path' => $destinationPath,
                'user_id'       => $_id,
                'created_at'    => $created_at,
                'updated_at'    => null
            ];
            $this->fileModel->create($attributes);
            $url = asset(Storage::url($destinationPath));
            
            $dataPush = ([  
                        'created_at'    => $created_at, 
                        'file_url'      => $url,
                        'file_name'     => $imageName,
                        'text'          => "a",
                        'user'          => 
                                            [   
                                                '_id'      => '1',
                                                'name'      => 'Admin'
                                            ]
                        ]);

            $newPost = $database
            ->getReference('Messagers/'.$_id)
            ->push($dataPush);
            $user = $this->userModel->getUserById($_id);
            $user->updated_at = $created_at;
            $user->save();
        }
           
        if ($chatData['text'] != null) {
                
                $dataPush = ([  
                                'created_at'    => $created_at, 
                                'text'          => $chatData['text'], 
                                'user'          => 
                                                    [   
                                                        '_id'      => '1',
                                                        'name'      => 'Admin'
                                                    ]
                            ]);

                $newPost = $database
                ->getReference('Messagers/'.$_id)
                ->push($dataPush);
                $user = $this->userModel->getUserById($_id);
                $user->updated_at = $created_at;
                $user->save();
        }

        $getFirstChat = $database
            ->getReference('Messagers/'.$_id)
            ->getSnapshot();
            
        $messages = $getFirstChat->getvalue();
        $this->authorize('index', $this->userModel);
        $users = User::orderByDesc('updated_at')->get();
        if ($request->username != null) {
            $searchresult = $this->userModel->getUsers($request);
        } else {
            $searchresult = [];
        }
        //$genres = $this->genreModel->all(); 
        $user = $this->userModel->getUserById($id); 
        $genres = Genre::orderByDesc('updated_at')->get();
        $groups = $this->groupModel->all();
        $files = $this->fileModel->getFilesbyId($id);
        $url = asset(Storage::url(''));  
        return view('admin.chatmanage.chatfirebase')->with([
            'messages'          => $messages,
            'users'             => $users,
            'searchresult'      => $searchresult,
            'groups'            => $groups,
            'user'              => $user,            
            'id'                => $id,                     
            'url'               => $url,
            'genres'            => $genres,
            'files'             => $files
        ]);
    }
    public function chatfb(Request $request, $id)
    {     
        // if($request->has('post_ids') && !empty($request->post_ids)){
        //     $post_ids = $request->post_ids;
        // }
        $chatData = $request->all();
        if ($chatData == null) {
            $chatData['file'] = '';
            $chatData['text'] = '';
        } else {
            $chatData = $request->all();
        }
        $user = Auth::user(); 
        $_id = $id;
        $created_at = date("Y-m-d H:i:s",time());
        //$created_at = str_replace("T"," ",$data->created_at);
        $serviceAccount = ServiceAccount::fromJsonFile(__DIR__.'/isk-chat-firebase-adminsdk-ci29k-6457f7bed0.json');
        $firebase = (new Factory)
            ->withServiceAccount($serviceAccount)
            ->withDatabaseUri('https://isk-chat.firebaseio.com/')
            ->create();
        $database = $firebase->getDatabase();

        if ($request->hasFile('file')) {
            // $this->validate($request, [
            // 'file' => 'required|file|mimes:pdf,docx,xlsx,pptx,csv|max:10240',
            // ]);
            //print_r($request->file_url);
            //die;
            
            $image = $request->file('file');           
            $imageName = $image->getClientOriginalName();
            $imageFormat = $image->getClientOriginalExtension(); 
            if ($imageFormat == 'jpeg' || $imageFormat == 'JPEG' || $imageFormat == 'jpg' || $imageFormat == 'JPG' || $imageFormat == 'bmp' || $imageFormat == 'BMP' || $imageFormat == 'gif' ||$imageFormat == 'GIF' ||$imageFormat == 'png' ||$imageFormat == 'PNG') {

                $newName = time().'_img.'.$imageFormat;
                $destinationPath = 'media/'.$_id.'/img/'.$newName;
                $movePath = 'storage/media/'.$_id.'/img';
                $image->move($movePath, $newName.'.jpg');
                $attributes = [
                    'name'          => $imageName.'.jpg',
                    'original_path' => $destinationPath.'.jpg',
                    'thumbnail_path' => null,
                    'user_id'       => $_id,
                    'created_at'    => $created_at,
                    'updated_at'    => null
                ];
                $this->mediaModel->create($attributes);
                $url = asset(Storage::url($destinationPath));
            
                
                $dataPush = ([  
                        'created_at'    => $created_at, 
                        'image'         => $url.'.jpg',
                        'user'          => 
                                            [   
                                                '_id'      => '1',
                                                // 'avatar'    => $data->avatar,
                                                'name'      => 'Admin'
                                            ]
                    ]);

                $newPost = $database
                ->getReference('Messagers/'.$_id)
                ->push($dataPush);
                $user = $this->userModel->getUserById($_id);
                $user->updated_at = $created_at;
                $user->save();

            } else {
                $newName = time().'_file.'.$imageFormat;
                $destinationPath = 'media/'.$_id.'/file/'.$newName;
                $movePath = 'storage/media/'.$_id.'/file';
                $image->move($movePath, $newName);
                $attributes = [
                    'name'          => $imageName,
                    'original_path' => $destinationPath,
                    'user_id'       => $_id,
                    'created_at'    => $created_at,
                    'updated_at'    => null
                ];
                $this->fileModel->create($attributes);
                $url = asset(Storage::url($destinationPath));
                
                $dataPush = ([  
                            'created_at'    => $created_at, 
                            'file_url'      => $url,
                            'file_name'     => $imageName,
                            'text'          => "a",
                            'user'          => 
                                                [   
                                                    '_id'      => '1',
                                                    'name'      => 'Admin'
                                                ]
                            ]);
                
                $newPost = $database
                ->getReference('Messagers/'.$_id)
                ->push($dataPush);
                $user = $this->userModel->getUserById($_id);
                $user->updated_at = $created_at;
                $user->save();
            }
        }
          
        if ($chatData['text'] != null) {
                
                $dataPush = ([  
                                'created_at'    => $created_at, 
                                'text'          => $chatData['text'], 
                                'user'          => 
                                                    [   
                                                        '_id'      => '1',
                                                        'name'      => 'Admin'
                                                    ]
                            ]);

                $newPost = $database
                ->getReference('Messagers/'.$_id)
                ->push($dataPush);
                $user = $this->userModel->getUserById($_id);
                $user->updated_at = $created_at;
                $user->save();
        }

        $getFirstChat = $database
            ->getReference('Messagers/'.$_id)
            ->getSnapshot();
           
        $messages = $getFirstChat->getvalue();
        $this->authorize('index', $this->userModel);
        $users = User::orderByDesc('updated_at')->get();
        if ($request->username != null) {
            $searchresult = $this->userModel->getUsers($request);
        } else {
            $searchresult = [];
        }
        $user = $this->userModel->getUserById($id); 
        $genres = Genre::orderByDesc('updated_at')->get();
        $groups = $this->groupModel->all();
        $files = $this->fileModel->getFilesbyId($id);
        $url = asset(Storage::url(''));     
        return view('admin.chatmanage.chatfirebase')->with([
            'messages'          => $messages,
            'users'             => $users,
            'searchresult'      => $searchresult,
            'groups'            => $groups,
            'user'              => $user,            
            'id'                => $id,                     
            'url'               => $url,
            'genres'            => $genres,
            'files'             => $files
        ]);
    }

    public function chatfbmulti(Request $request)
    {             
        $chatData = $request->all();
        if ($chatData == null) {
            $chatData['file'] = '';
            $chatData['text'] = '';
        } else {
            $chatData = $request->all();
        }
        $user = Auth::user(); 
        $created_at = date("Y-m-d H:i:s",time());
        //$created_at = str_replace("T"," ",$data->created_at);
        $serviceAccount = ServiceAccount::fromJsonFile(__DIR__.'/isk-chat-firebase-adminsdk-ci29k-6457f7bed0.json');
        $firebase = (new Factory)
            ->withServiceAccount($serviceAccount)
            ->withDatabaseUri('https://isk-chat.firebaseio.com/')
            ->create();
        $database = $firebase->getDatabase();

        if ($request->hasFile('file')) {
            
            if($request->has('post_ids') && !empty($request->post_ids)){
                $post_ids = $request->post_ids;
            }
            $post_ids = explode(",", $post_ids);            
            $image = $request->file('file');  
            $imageName = $image->getClientOriginalName();
            $imageFormat = $image->getClientOriginalExtension(); 
            if ($imageFormat == 'jpeg' || $imageFormat == 'JPEG' || $imageFormat == 'jpg' ||$imageFormat == 'JPG' || $imageFormat == 'bmp' || $imageFormat == 'BMP' || $imageFormat == 'gif' ||$imageFormat == 'GIF' ||$imageFormat == 'png' ||$imageFormat == 'PNG') {

                $movePaths = array();    
                //$newNames = array();  
                $newName = time().'_img.'.$imageFormat;     
                $count = 1;                
                foreach ($post_ids as $_id) {
                    if ($_id != 'all' && $_id != '1' && $_id != '2'&& $_id != '3'&& $_id != '4') {
                            $destinationPath = 'media/'.$_id.'/img/'.$newName;
                            $movePath = 'storage/media/'.$_id.'/img';   
                            if ($count == 1) {                                
                                $image->move($movePath, $newName.'.jpg'); 
                                $oldpath = $movePath;
                            } 
                            if ($count > 1) {
                                array_push($movePaths, $movePath);       
                            }                 
                            //array_push($newNames, $newName);
                            $attributes = [
                                'name'          => $imageName.'.jpg',
                                'original_path' => $destinationPath.'.jpg',
                                'thumbnail_path' => null,
                                'user_id'       => $_id,
                                'created_at'    => $created_at,
                                'updated_at'    => null
                            ];
                            $this->mediaModel->create($attributes);
                            $url = asset(Storage::url($destinationPath));
                        
                            
                            $dataPush = ([  
                                    'created_at'    => $created_at, 
                                    'image'         => $url.'.jpg',
                                    'user'          => 
                                                        [   
                                                            '_id'      => '1',
                                                            // 'avatar'    => $data->avatar,
                                                            'name'      => 'Admin'
                                                        ]
                                ]);

                            $newPost = $database
                            ->getReference('Messagers/'.$_id)
                            ->push($dataPush);
                            $user = $this->userModel->getUserById($_id);
                            $user->updated_at = $created_at;
                            $user->save();
                            $count++;
                    }
                    if ($_id == '1') {
                        $_ids = $this->userModel->getUserByGenreId($_id);
                        foreach ($_ids as $key => $arr) {
                            $_id = $arr['id'];
                            //$newName = time().'_img.'.$imageFormat;
                            $destinationPath = 'media/'.$_id.'/img/'.$newName;
                            $movePath = 'storage/media/'.$_id.'/img';
                            if ($count == 1) {                                
                                $image->move($movePath, $newName.'.jpg'); 
                                $oldpath = $movePath;
                            } 
                            if ($count > 1) {
                                array_push($movePaths, $movePath);       
                            }   
                            $attributes = [
                                'name'          => $imageName.'.jpg',
                                'original_path' => $destinationPath.'.jpg',
                                'thumbnail_path' => null,
                                'user_id'       => $_id,
                                'created_at'    => $created_at,
                                'updated_at'    => null
                            ];
                            $this->mediaModel->create($attributes);
                            $url = asset(Storage::url($destinationPath));
                        
                            
                            $dataPush = ([  
                                    'created_at'    => $created_at, 
                                    'image'         => $url.'.jpg',
                                    'user'          => 
                                                        [   
                                                            '_id'      => '1',
                                                            // 'avatar'    => $data->avatar,
                                                            'name'      => 'Admin'
                                                        ]
                                ]);

                            $newPost = $database
                            ->getReference('Messagers/'.$_id)
                            ->push($dataPush);
                            $user = $this->userModel->getUserById($_id);
                            $user->updated_at = $created_at;
                            $user->save();                            
                            DB::table('genre')
                            ->where("genre.id", '=',  '1')
                            ->limit(1)
                            ->update(['genre.updated_at'=> $created_at]);
                            $count++;
                        }
                    } 
                    if ($_id == '2') {
                        $_ids = $this->userModel->getUserByGenreId($_id);
                        foreach ($_ids as $key => $arr) {
                            $_id = $arr['id'];
                            //$newName = time().'_img.'.$imageFormat;
                            $destinationPath = 'media/'.$_id.'/img/'.$newName;
                            $movePath = 'storage/media/'.$_id.'/img';
                            if ($count == 1) {                                
                                $image->move($movePath, $newName.'.jpg'); 
                                $oldpath = $movePath;
                            } 
                            if ($count > 1) {
                                array_push($movePaths, $movePath);       
                            }   
                            $attributes = [
                                'name'          => $imageName.'.jpg',
                                'original_path' => $destinationPath.'.jpg',
                                'thumbnail_path' => null,
                                'user_id'       => $_id,
                                'created_at'    => $created_at,
                                'updated_at'    => null
                            ];
                            $this->mediaModel->create($attributes);
                            $url = asset(Storage::url($destinationPath));
                        
                            
                            $dataPush = ([  
                                    'created_at'    => $created_at, 
                                    'image'         => $url.'.jpg',
                                    'user'          => 
                                                        [   
                                                            '_id'      => '1',
                                                            // 'avatar'    => $data->avatar,
                                                            'name'      => 'Admin'
                                                        ]
                                ]);

                            $newPost = $database
                            ->getReference('Messagers/'.$_id)
                            ->push($dataPush);
                            $user = $this->userModel->getUserById($_id);
                            $user->updated_at = $created_at;
                            $user->save();                           
                            DB::table('genre')
                            ->where("genre.id", '=',  '2')
                            ->limit(1)
                            ->update(['genre.updated_at'=> $created_at]);
                            $count++;
                        }
                    }
                    if ($_id == '3') {
                        $_ids = $this->userModel->getUserByGenreId($_id);
                        foreach ($_ids as $key => $arr) {
                            $_id = $arr['id'];
                            //$newName = time().'_img.'.$imageFormat;
                            $destinationPath = 'media/'.$_id.'/img/'.$newName;
                            $movePath = 'storage/media/'.$_id.'/img';
                            if ($count == 1) {                                
                                $image->move($movePath, $newName.'.jpg'); 
                                $oldpath = $movePath;
                            } 
                            if ($count > 1) {
                                array_push($movePaths, $movePath);       
                            }   
                            $attributes = [
                                'name'          => $imageName.'.jpg',
                                'original_path' => $destinationPath.'.jpg',
                                'thumbnail_path' => null,
                                'user_id'       => $_id,
                                'created_at'    => $created_at,
                                'updated_at'    => null
                            ];
                            $this->mediaModel->create($attributes);
                            $url = asset(Storage::url($destinationPath));
                        
                            
                            $dataPush = ([  
                                    'created_at'    => $created_at, 
                                    'image'         => $url.'.jpg',
                                    'user'          => 
                                                        [   
                                                            '_id'      => '1',
                                                            // 'avatar'    => $data->avatar,
                                                            'name'      => 'Admin'
                                                        ]
                                ]);

                            $newPost = $database
                            ->getReference('Messagers/'.$_id)
                            ->push($dataPush);
                            $user = $this->userModel->getUserById($_id);
                            $user->updated_at = $created_at;
                            $user->save();                           
                            DB::table('genre')
                            ->where("genre.id", '=',  '3')
                            ->limit(1)
                            ->update(['genre.updated_at'=> $created_at]);
                            $count++;
                        }

                    }
                    if ($_id == '4') {
                        $_ids = $this->userModel->getUserByGenreId($_id);
                        foreach ($_ids as $key => $arr) {
                            $_id = $arr['id'];
                            //$newName = time().'_img.'.$imageFormat;
                            $destinationPath = 'media/'.$_id.'/img/'.$newName;
                            $movePath = 'storage/media/'.$_id.'/img';
                            if ($count == 1) {                                
                                $image->move($movePath, $newName.'.jpg'); 
                                $oldpath = $movePath;
                            } 
                            if ($count > 1) {
                                array_push($movePaths, $movePath);       
                            }   
                            $attributes = [
                                'name'          => $imageName.'.jpg',
                                'original_path' => $destinationPath.'.jpg',
                                'thumbnail_path' => null,
                                'user_id'       => $_id,
                                'created_at'    => $created_at,
                                'updated_at'    => null
                            ];
                            $this->mediaModel->create($attributes);
                            $url = asset(Storage::url($destinationPath));
                        
                            
                            $dataPush = ([  
                                    'created_at'    => $created_at, 
                                    'image'         => $url.'.jpg',
                                    'user'          => 
                                                        [   
                                                            '_id'      => '1',
                                                            // 'avatar'    => $data->avatar,
                                                            'name'      => 'Admin'
                                                        ]
                                ]);

                            $newPost = $database
                            ->getReference('Messagers/'.$_id)
                            ->push($dataPush);
                            $user = $this->userModel->getUserById($_id);
                            $user->updated_at = $created_at;
                            $user->save();                           
                            DB::table('genre')
                            ->where("genre.id", '=',  '4')
                            ->limit(1)
                            ->update(['genre.updated_at'=> $created_at]);
                            $count++;
                        }
                    }
                    if ($_id == 'all') {
                        $_ids = $this->userModel->all();                     
                        foreach ($_ids as $key => $arr) {
                            $_id = $arr['id'];
                            //$newName = time().'_img.'.$imageFormat;
                            $destinationPath = 'media/'.$_id.'/img/'.$newName;
                            $movePath = 'storage/media/'.$_id.'/img';
                            if ($count == 1) {                                
                                $image->move($movePath, $newName.'.jpg'); 
                                $oldpath = $movePath;
                            } 
                            if ($count > 1) {
                                array_push($movePaths, $movePath);       
                            }   
                            $attributes = [
                                'name'          => $imageName.'.jpg',
                                'original_path' => $destinationPath.'.jpg',
                                'thumbnail_path' => null,
                                'user_id'       => $_id,
                                'created_at'    => $created_at,
                                'updated_at'    => null
                            ];
                            $this->mediaModel->create($attributes);
                            $url = asset(Storage::url($destinationPath));
                        
                            
                            $dataPush = ([  
                                    'created_at'    => $created_at, 
                                    'image'         => $url.'.jpg',
                                    'user'          => 
                                                        [   
                                                            '_id'      => '1',
                                                            // 'avatar'    => $data->avatar,
                                                            'name'      => 'Admin'
                                                        ]
                                ]);

                            $newPost = $database
                            ->getReference('Messagers/'.$_id)
                            ->push($dataPush);
                            $user = $this->userModel->getUserById($_id);
                            $user->updated_at = $created_at;
                            $user->save();                           
                            DB::table('group')
                            ->where("group.listid", '=',  'all')
                            ->limit(1)
                            ->update(['group.updated_at'=> $created_at]);
                            $count++;
                        }
                    }
                }
                foreach ($movePaths as $newPath) {
                    if (!is_dir($newPath)) {
                        mkdir($newPath, 0777, true);
                    }
                    copy($oldpath.'/'.$newName.'.jpg', $newPath.'/'.$newName.'.jpg');
                }

            } else {
                $movePaths = array();
                $newName = time().'_file.'.$imageFormat;
                $count = 1;
                foreach ($post_ids as $_id) {
                    if ($_id != 'all' && $_id != '1' && $_id != '2'&& $_id != '3'&& $_id != '4') {
                    //$newName = time().'_file.'.$imageFormat;
                    $destinationPath = 'media/'.$_id.'/file/'.$newName;
                    $movePath = 'storage/media/'.$_id.'/file';
                    if ($count == 1) {                                
                        $image->move($movePath, $newName); 
                        $oldpath = $movePath;
                    } 
                    if ($count > 1) {
                        array_push($movePaths, $movePath);       
                    }   
                    $attributes = [
                        'name'          => $imageName,
                        'original_path' => $destinationPath,
                        'user_id'       => $_id,
                        'created_at'    => $created_at,
                        'updated_at'    => null
                    ];
                    $this->fileModel->create($attributes);
                    $url = asset(Storage::url($destinationPath));
                    
                    $dataPush = ([  
                                'created_at'    => $created_at, 
                                'file_url'      => $url,
                                'file_name'     => $imageName,
                                'text'          => "a",
                                'user'          => 
                                                    [   
                                                        '_id'      => '1',
                                                        'name'      => 'Admin'
                                                    ]
                                ]);

                    $newPost = $database
                    ->getReference('Messagers/'.$_id)
                    ->push($dataPush);
                    $user = $this->userModel->getUserById($_id);
                    $user->updated_at = $created_at;
                    $user->save();
                    $count++;
                }
                if ($_id == '1') {
                    $_ids = $this->userModel->getUserByGenreId($_id);
                    foreach ($_ids as $key => $arr) {
                        $_id = $arr['id'];
                        //$newName = time().'_file.'.$imageFormat;
                        $destinationPath = 'media/'.$_id.'/file/'.$newName;
                        $movePath = 'storage/media/'.$_id.'/file';
                        if ($count == 1) {                                
                            $image->move($movePath, $newName); 
                            $oldpath = $movePath;
                        } 
                        if ($count > 1) {
                            array_push($movePaths, $movePath);       
                        }  
                        $attributes = [
                            'name'          => $imageName,
                            'original_path' => $destinationPath,
                            'user_id'       => $_id,
                            'created_at'    => $created_at,
                            'updated_at'    => null
                        ];
                        $this->fileModel->create($attributes);
                        $url = asset(Storage::url($destinationPath));
                        
                        $dataPush = ([  
                                    'created_at'    => $created_at, 
                                    'file_url'      => $url,
                                    'file_name'     => $imageName,
                                    'text'          => "a",
                                    'user'          => 
                                                        [   
                                                            '_id'      => '1',
                                                            'name'      => 'Admin'
                                                        ]
                                    ]);

                        $newPost = $database
                        ->getReference('Messagers/'.$_id)
                        ->push($dataPush);
                        $user = $this->userModel->getUserById($_id);
                        $user->updated_at = $created_at;
                        $user->save();                           
                        DB::table('genre')
                        ->where("genre.id", '=',  '1')
                        ->limit(1)
                        ->update(['genre.updated_at'=> $created_at]);
                        $count++;
                    }
                }
                if ($_id == '2') {
                    $_ids = $this->userModel->getUserByGenreId($_id);
                    foreach ($_ids as $key => $arr) {
                        $_id = $arr['id'];
                        //$newName = time().'_file.'.$imageFormat;
                        $destinationPath = 'media/'.$_id.'/file/'.$newName;
                        $movePath = 'storage/media/'.$_id.'/file';
                        if ($count == 1) {                                
                            $image->move($movePath, $newName); 
                            $oldpath = $movePath;
                        } 
                        if ($count > 1) {
                            array_push($movePaths, $movePath);       
                        }
                        $attributes = [
                            'name'          => $imageName,
                            'original_path' => $destinationPath,
                            'user_id'       => $_id,
                            'created_at'    => $created_at,
                            'updated_at'    => null
                        ];
                        $this->fileModel->create($attributes);
                        $url = asset(Storage::url($destinationPath));
                        
                        $dataPush = ([  
                                    'created_at'    => $created_at, 
                                    'file_url'      => $url,
                                    'file_name'     => $imageName,
                                    'text'          => "a",
                                    'user'          => 
                                                        [   
                                                            '_id'      => '1',
                                                            'name'      => 'Admin'
                                                        ]
                                    ]);

                        $newPost = $database
                        ->getReference('Messagers/'.$_id)
                        ->push($dataPush);
                        $user = $this->userModel->getUserById($_id);
                        $user->updated_at = $created_at;
                        $user->save();
                        DB::table('genre')
                        ->where("genre.id", '=',  '2')
                        ->limit(1)
                        ->update(['genre.updated_at'=> $created_at]);
                        $count++;
                    }
                }
                if ($_id == '3') {
                    $_ids = $this->userModel->getUserByGenreId($_id);
                    foreach ($_ids as $key => $arr) {
                        $_id = $arr['id'];
                        //$newName = time().'_file.'.$imageFormat;
                        $destinationPath = 'media/'.$_id.'/file/'.$newName;
                        $movePath = 'storage/media/'.$_id.'/file';
                        if ($count == 1) {                                
                            $image->move($movePath, $newName); 
                            $oldpath = $movePath;
                        } 
                        if ($count > 1) {
                            array_push($movePaths, $movePath);       
                        }
                        $attributes = [
                            'name'          => $imageName,
                            'original_path' => $destinationPath,
                            'user_id'       => $_id,
                            'created_at'    => $created_at,
                            'updated_at'    => null
                        ];
                        $this->fileModel->create($attributes);
                        $url = asset(Storage::url($destinationPath));
                        
                        $dataPush = ([  
                                    'created_at'    => $created_at, 
                                    'file_url'      => $url,
                                    'file_name'     => $imageName,
                                    'text'          => "a",
                                    'user'          => 
                                                        [   
                                                            '_id'      => '1',
                                                            'name'      => 'Admin'
                                                        ]
                                    ]);

                        $newPost = $database
                        ->getReference('Messagers/'.$_id)
                        ->push($dataPush);
                        $user = $this->userModel->getUserById($_id);
                        $user->updated_at = $created_at;
                        $user->save();
                        DB::table('genre')
                        ->where("genre.id", '=',  '3')
                        ->limit(1)
                        ->update(['genre.updated_at'=> $created_at]);
                        $count++;
                    }
                }
                if ($_id == '4') {
                    $_ids = $this->userModel->getUserByGenreId($_id);
                    foreach ($_ids as $key => $arr) {
                        $_id = $arr['id'];
                        //$newName = time().'_file.'.$imageFormat;
                        $destinationPath = 'media/'.$_id.'/file/'.$newName;
                        $movePath = 'storage/media/'.$_id.'/file';
                        if ($count == 1) {                                
                            $image->move($movePath, $newName); 
                            $oldpath = $movePath;
                        } 
                        if ($count > 1) {
                            array_push($movePaths, $movePath);       
                        }
                        $attributes = [
                            'name'          => $imageName,
                            'original_path' => $destinationPath,
                            'user_id'       => $_id,
                            'created_at'    => $created_at,
                            'updated_at'    => null
                        ];
                        $this->fileModel->create($attributes);
                        $url = asset(Storage::url($destinationPath));
                        
                        $dataPush = ([  
                                    'created_at'    => $created_at, 
                                    'file_url'      => $url,
                                    'file_name'     => $imageName,
                                    'text'          => "a",
                                    'user'          => 
                                                        [   
                                                            '_id'      => '1',
                                                            'name'      => 'Admin'
                                                        ]
                                    ]);

                        $newPost = $database
                        ->getReference('Messagers/'.$_id)
                        ->push($dataPush);
                        $user = $this->userModel->getUserById($_id);
                        $user->updated_at = $created_at;
                        $user->save();
                        DB::table('genre')
                        ->where("genre.id", '=',  '4')
                        ->limit(1)
                        ->update(['genre.updated_at'=> $created_at]);
                        $count++;
                    }
                }
                if ($_id == 'all') {
                    $_ids = $this->userModel->all();                     
                    foreach ($_ids as $key => $arr) {
                        $_id = $arr['id'];
                        //$newName = time().'_file.'.$imageFormat;
                        $destinationPath = 'media/'.$_id.'/file/'.$newName;
                        $movePath = 'storage/media/'.$_id.'/file';
                        if ($count == 1) {                                
                            $image->move($movePath, $newName); 
                            $oldpath = $movePath;
                        } 
                        if ($count > 1) {
                            array_push($movePaths, $movePath);       
                        }
                        $attributes = [
                            'name'          => $imageName,
                            'original_path' => $destinationPath,
                            'user_id'       => $_id,
                            'created_at'    => $created_at,
                            'updated_at'    => null
                        ];
                        $this->fileModel->create($attributes);
                        $url = asset(Storage::url($destinationPath));
                        
                        $dataPush = ([  
                                    'created_at'    => $created_at, 
                                    'file_url'      => $url,
                                    'file_name'     => $imageName,
                                    'text'          => "a",
                                    'user'          => 
                                                        [   
                                                            '_id'      => '1',
                                                            'name'      => 'Admin'
                                                        ]
                                    ]);

                        $newPost = $database
                        ->getReference('Messagers/'.$_id)
                        ->push($dataPush);
                        $user = $this->userModel->getUserById($_id);
                        $user->updated_at = $created_at;
                        $user->save();                         
                        DB::table('group')
                        ->where("group.listid", '=',  'all')
                        ->limit(1)
                        ->update(['group.updated_at'=> $created_at]);
                        $count++;
                    }
                }
            }
            foreach ($movePaths as $newPath) {
                if (!is_dir($newPath)) {
                    mkdir($newPath, 0777, true);
                }
                copy($oldpath.'/'.$newName, $newPath.'/'.$newName);
            }
        }
    }
            //print_r($chatData['file-input']);
                    //die;
            //$image = $request->file('file-input'); 
            //print_r($chatData['text']);
            //print_r($request->hasFile('file-input'));
            //die;
        if ($chatData['text'] != null) {
            if($request->has('post_ids') && !empty($request->post_ids)){
                $post_ids = $request->post_ids;
            }
            $post_ids = explode(",", $post_ids);
            foreach ($post_ids as $_id) {
                if ($_id != 'all' && $_id != '1' && $_id != '2'&& $_id != '3'&& $_id != '4') {    
                    $dataPush = ([  
                                    'created_at'    => $created_at, 
                                    'text'          => $chatData['text'], 
                                    'user'          => 
                                                        [   
                                                            '_id'      => '1',
                                                            'name'      => 'Admin'
                                                        ]
                                ]);

                    $newPost = $database
                    ->getReference('Messagers/'.$_id)
                    ->push($dataPush);
                    $user = $this->userModel->getUserById($_id);
                    $user->updated_at = $created_at;
                    $user->save();
                }
                if ($_id == '1') {
                    $_ids = $this->userModel->getUserByGenreId($_id);
                    foreach ($_ids as $key => $arr) {
                        $_id = $arr['id'];
                        $dataPush = ([  
                                    'created_at'    => $created_at, 
                                    'text'          => $chatData['text'], 
                                    'user'          => 
                                                        [   
                                                            '_id'      => '1',
                                                            'name'      => 'Admin'
                                                        ]
                                ]);

                        $newPost = $database
                        ->getReference('Messagers/'.$_id)
                        ->push($dataPush);
                        $user = $this->userModel->getUserById($_id);
                        $user->updated_at = $created_at;
                        $user->save();
                        DB::table('genre')
                        ->where("genre.id", '=',  '1')
                        ->limit(1)
                        ->update(['genre.updated_at'=> $created_at]);
                    }
                }

                if ($_id == '2') {
                    $_ids = $this->userModel->getUserByGenreId($_id);
                    foreach ($_ids as $key => $arr) {
                        $_id = $arr['id'];
                        $dataPush = ([  
                                    'created_at'    => $created_at, 
                                    'text'          => $chatData['text'], 
                                    'user'          => 
                                                        [   
                                                            '_id'      => '1',
                                                            'name'      => 'Admin'
                                                        ]
                                ]);

                        $newPost = $database
                        ->getReference('Messagers/'.$_id)
                        ->push($dataPush);
                        $user = $this->userModel->getUserById($_id);
                        $user->updated_at = $created_at;
                        $user->save();
                        DB::table('genre')
                        ->where("genre.id", '=',  '2')
                        ->limit(1)
                        ->update(['genre.updated_at'=> $created_at]);
                    }
                }
                
                if ($_id == '3') {                    
                    $_ids = $this->userModel->getUserByGenreId($_id);
                    foreach ($_ids as $key => $arr) {
                        $_id = $arr['id'];
                            $dataPush = ([  
                                        'created_at'    => $created_at, 
                                        'text'          => $chatData['text'], 
                                        'user'          => 
                                                            [   
                                                                '_id'      => '1',
                                                                'name'      => 'Admin'
                                                            ]
                                    ]);

                        $newPost = $database
                        ->getReference('Messagers/'.$_id)
                        ->push($dataPush);
                        $user = $this->userModel->getUserById($_id);
                        $user->updated_at = $created_at;
                        $user->save();
                        DB::table('genre')
                        ->where("genre.id", '=',  '3')
                        ->limit(1)
                        ->update(['genre.updated_at'=> $created_at]);
                    }
                }
                if ($_id == '4') {
                    $_ids = $this->userModel->getUserByGenreId($_id);
                    foreach ($_ids as $key => $arr) {
                        $_id = $arr['id'];
                        $dataPush = ([  
                                        'created_at'    => $created_at, 
                                        'text'          => $chatData['text'], 
                                        'user'          => 
                                                            [   
                                                                '_id'      => '1',
                                                                'name'      => 'Admin'
                                                            ]
                                    ]);

                        $newPost = $database
                        ->getReference('Messagers/'.$_id)
                        ->push($dataPush);
                        $user = $this->userModel->getUserById($_id);
                        $user->updated_at = $created_at;
                        $user->save();
                        DB::table('genre')
                        ->where("genre.id", '=',  '4')
                        ->limit(1)
                        ->update(['genre.updated_at'=> $created_at]);
                    }
                }
                if ($_id == 'all') {
                    $_ids = $this->userModel->all();                     
                    foreach ($_ids as $key => $arr) {
                        $_id = $arr['id'];
                        $dataPush = ([  
                                        'created_at'    => $created_at, 
                                        'text'          => $chatData['text'], 
                                        'user'          => 
                                                            [   
                                                                '_id'      => '1',
                                                                'name'      => 'Admin'
                                                            ]
                                    ]);

                        $newPost = $database
                        ->getReference('Messagers/'.$_id)
                        ->push($dataPush);
                        $user = $this->userModel->getUserById($_id);
                        $user->updated_at = $created_at;
                        $user->save();                         
                        DB::table('group')
                        ->where("group.listid", '=',  'all')
                        ->limit(1)
                        ->update(['group.updated_at'=> $created_at]);
                    }
                }
            }

        }

        $getFirstChat = $database
            ->getReference('Messagers/'.$_id)
            ->getSnapshot();
           
        $messages = $getFirstChat->getvalue();
        $searchresult = User::orderByDesc('updated_at')->get();
        $genres = Genre::orderByDesc('updated_at')->get();
        $groups = $this->groupModel->all();
        return view('admin.chatmanage.index')->with([
            'searchresult'      => $searchresult,
            'genres'            => $genres,
            'groups'            => $groups
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
