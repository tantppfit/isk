<?php

namespace App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;

use App\Models\Setting;
use App\Models\Page;
use App\Models\Vote;
use App\Models\Category;

class SettingController extends Controller
{
    protected $settingModel;
    protected $pageModel;
    protected $voteModel;
    protected $categoryModal;
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(Setting $settingModel, Page $pageModel, Vote $voteModel, Category $categoryModal)
    {
        $this->settingModel = $settingModel;
        $this->pageModel = $pageModel;
        $this->voteModel = $voteModel;
        $this->categoryModal = $categoryModal;
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index(Request $request)
    {
        $this->authorize('index', $this->settingModel);
        $settings = $this->settingModel->getSettingsData();
        return view('admin.templates.setting.index')->with([
            'settings' => $settings
        ]);
    }
    public function fixedpage(Request $request)
    {
        $this->authorize('index', $this->settingModel);
        $pages = $this->pageModel->getPages($request);
        return view('admin.templates.setting.fixedpage')->with([
            'pages' => $pages
        ]);
    }
    public function vote(Request $request)
    {
        $this->authorize('index', $this->settingModel);
        $votes = $this->voteModel->getVotes($request);
        return view('admin.templates.setting.vote')->with([
            'votes' => $votes
        ]);
    }
    public function widget(Request $request)
    {
        $this->authorize('index', $this->settingModel);
        $settings = $this->settingModel->getSettingsData();
        return view('admin.templates.setting.widget')->with([
            'settings' => $settings
        ]);
    }
    public function stylesheet(Request $request)
    {
        $this->authorize('index', $this->settingModel);
        $settings = $this->settingModel->getSettingsData();
        return view('admin.templates.setting.stylesheet')->with([
            'settings' => $settings
        ]);
    }
    public function sidebar(Request $request)
    {
        $this->authorize('index', $this->settingModel);
        $settings = $this->settingModel->getSettingsData();
        $parentCategories = $this->categoryModal->getParentCategories();
        return view('admin.templates.setting.sidebar')->with([
            'settings' => $settings,
            'parentCategories' => $parentCategories
        ]);
    }
    public function footer(Request $request)
    {
        $this->authorize('index', $this->settingModel);
        $settings = $this->settingModel->getSettingsData();
        $parentCategories = $this->categoryModal->getParentCategories();
        return view('admin.templates.setting.footer')->with([
            'settings' => $settings
        ]);
    }
    public function update(Request $request)
    {
        $this->authorize('index', $this->settingModel);
        if($request->has('redirect') && !empty($request->redirect)){
            $redirect = $request->redirect;
        }else{
            $redirect = route('admin.setting.index');
        }
        $settingsData = $request->except(['_token','redirect']);
        foreach ($settingsData as $key => $value) {
            if(is_array($value)){
                $value = json_encode($value);
            }
            $this->settingModel->updateOrCreate(
                ['key' => $key],
                [
                    'display_name' => $key,
                    'value' => $value
                ]
            );
        }
        return redirect($redirect)->with('success', trans('messages.success.update', ['Module' => trans('module.entries.setting')]));
    }
}
