<?php
namespace App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\URL;
use Illuminate\Http\Request;
use App\Models\User;
use App\Models\FileManage;
use App\Models\Post;
use App\Models\Media;
use DB;
use Helper;
use Auth;
use Response;
use ZipArchive;
require_once('../vendor/autoload.php');
class FileManageController extends Controller
{
    protected $fileModel;
    protected $postModel;
    protected $userModel;
    protected $mediaModel;
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(FileManage $fileModel, Post $postModel, User $userModel, Media $mediaModel)
    {
        $this->fileModel = $fileModel;
        $this->postModel = $postModel;
        $this->userModel = $userModel;
        $this->mediaModel = $mediaModel;
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index(Request $id)
    {
        $files = $this->fileModel->getFiles($id); 
        $user = $this->userModel->getUserById($id['id']);
        $medias = $this->mediaModel->getMediasByUserId($id);
        // echo $media;
        // die();
        // echo $user['name'];
        // echo $id['id'];
        return view('admin.templates.filemanage.index')->with([
            'files' => $files,
            'id'    => $id,
            'user'  => $user,
            'medias' => $medias
        ]);
        
    }

    public function indexFilebrowsing(Request $Request , $id)
    {
        $filebrowsing = $this->fileModel->getFilebrowsingById($id);
        $user_id = $filebrowsing->user_id;               
        $user = $this->userModel->getUserById($user_id);
        $parser = new \Smalot\PdfParser\Parser(); 
        // call this in your controller
        //$path = 'public/media/'.$user_id.'/';
        $pdf    = $parser->parseFile(__DIR__.'/filedemo.pdf');
        //$details  = $pdf->getDetails();
        //$text = $pdf->getText();
        $pages  = $pdf->getPages();
        //dd($text);
        // echo $filebrowsing;die();
        return view('admin.templates.filemanage.filebrowsing.index')->with([
            'filebrowsing'  => $filebrowsing,
            'id'            => $id,
            'user'          => $user,
            'pages'         => $pages
        ]);
    }

    public function destroy($id)
    {
        $files = $this->fileModel->findOrFail($id);
        $this->fileModel->destroy($id);
        return redirect()->back()->with('success', trans('messages.success.delete', ['Module' => trans('module.entry.filemanage')]));
    }
    public function changeAll(Request $request){
        if($request->has('redirect')){
            $redirect = $request->redirect;
        }else{
            $redirect = route('admin.filemanage.index');
        }
        if(!$request->has('_action') || empty($request->_action)){
            return redirect($redirect)->with('error', trans('messages.error.update', ['Module' => trans('module.entries.filemanage')]));
        }
        if($request->has('post_ids') && !empty($request->post_ids)){
            $post_ids = $request->post_ids;
            if($request->_action == 'delete'){
                $post_ids = explode(",", $post_ids);
                $images = array();
                $files = array();
                foreach ($post_ids as $post_id) {

                    if (substr($post_id,-1) == 'i') {
                        $post_id = str_replace("_i","",$post_id);
                        array_push($images, $post_id);
                    } else {
                        array_push($files, $post_id);
                    }
                }                
                // print_r($images);
                // print_r($files);
                // die;
                $this->mediaModel->destroy($images);
                $this->fileModel->destroy($files);
                return redirect($redirect)->with('success', trans('messages.success.delete', ['Module' => trans('module.entries.filemanage')]));
            }
            if($request->_action == 'download'){
                //print_r($post_ids);
                //die;
                $post_ids = explode(",", $post_ids);
                $images = array();
                $files = array();
                foreach ($post_ids as $post_id) {

                    if (substr($post_id,-1) == 'i') {
                        $post_id = str_replace("_i","",$post_id);
                        array_push($images, $post_id);
                    } else {
                        array_push($files, $post_id);
                    }
                }   
                $zip = new ZipArchive();
                $zip_name = time().".zip"; // Zip name
                $zip->open($zip_name,  ZipArchive::CREATE);
                foreach ($files as  $file) {                    
                    $filebrowsing = $this->fileModel->getFilebrowsingById($file);
                    $original_path = $filebrowsing->original_path;   
                    $name = $filebrowsing->name;  
                    $url = asset(Storage::url($original_path));
                    $zip->addFromString($name,file_get_contents($url));  
                }
                foreach ($images as  $image) {                    
                    $filebrowsing = $this->mediaModel->getMediaById($image);
                    $original_path = $filebrowsing->original_path;   
                    $name = $filebrowsing->name;  
                    $url = asset(Storage::url($original_path));
                    $zip->addFromString($name,file_get_contents($url)); 
                }
                $zip->close();
                $file_path = URL::to('').'/';  
                header('Content-Type: application/zip');
                header('Content-disposition: attachment; filename='.$zip_name);
                readfile($file_path.$zip_name);  
                // $dir = dirname(__FILE__);
                // if ($handle = opendir($dir)) {
                //     while (false !== ($zip_name = readdir($handle))) {
                //         if ((time()-filectime($dir.'/'.$zip_name)) < 20) {  // 86400 = 60*60*24
                //           if (preg_match('/\.txt$/i', $zip_name)) {
                //             unlink($dir.'/'.$zip_name);
                //           }
                //         }
                //     }
                //   }            
            }
                
        }

    }
    public function download(Request $request){
        print_r($request->id);
        die;
        if($request->has('post_ids') && !empty($request->post_ids)){
            $post_ids = $request->post_ids;
            if($request->_action == 'download'){
                //print_r($post_ids);
                //die;
                $post_ids = explode(",", $post_ids);
                $images = array();
                $files = array();
                foreach ($post_ids as $post_id) {

                    if (substr($post_id,-1) == 'i') {
                        $post_id = str_replace("_i","",$post_id);
                        array_push($images, $post_id);
                    } else {
                        array_push($files, $post_id);
                    }
                }
                foreach ($files as  $file) {                    
                    $filebrowsing = $this->fileModel->getFilebrowsingById($file);
                    $original_path = $filebrowsing->original_path;   
                    $name = $filebrowsing->name;  
                    $url = asset(Storage::url($original_path));
                    $zip->addFromString($name,file_get_contents($url));  
                }
                foreach ($images as  $image) {                    
                    $filebrowsing = $this->mediaModel->getMediaById($image);
                    $original_path = $filebrowsing->original_path;   
                    $name = $filebrowsing->name;  
                    $url = asset(Storage::url($original_path));
                    $zip->addFromString($name,file_get_contents($url)); 
                }
                $zip->close();
                $file_path = URL::to('').'/';  
                header('Content-Type: application/zip');
                header('Content-disposition: attachment; filename='.$zip_name);
                readfile($file_path.$zip_name);  
            }
                
        }

    }
}