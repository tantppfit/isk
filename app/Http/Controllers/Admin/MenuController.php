<?php

namespace App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use App\Models\Menu;

class MenuController extends Controller
{
    protected $menuModel;
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct( Menu $menuModel )
    {
        $this->menuModel = $menuModel;
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index(Request $request)
    {
        $this->authorize('index', $this->menuModel);
        $location = ($request->has('location')) ? $request->location : '';
        $menus = $this->menuModel->getMenus($location);
        $menuLocations = config('adiva.menu_locations');
        return view('admin.templates.menu.index')->with([
            'menus' => $menus,
            'menuLocations' => $menuLocations
        ]);
    }
    public function create(Request $request)
    {
        $this->authorize('create', $this->menuModel);
        $menuLocations = config('adiva.menu_locations');
        return view('admin.templates.menu.create')->with([
            'menuLocations' => $menuLocations
        ]);
    }
    public function store(Request $request)
    {
        $this->authorize('store', $this->menuModel);
        $this->validate($request, [
          'name' => 'required',
          'url' => 'required',
          'location' => 'required'
        ],[],[
            'name' =>  trans('table_fields.menus.name'),
            'url' =>  trans('table_fields.menus.url'),
            'location' =>  trans('table_fields.menus.location')
        ]);
        $menuData = $request->except('_token');
        if($request->has('show') && in_array($request->show, [0,1])){
            $menuData['show'] = $request->show;
        }else{
            $menuData['show'] = 0;
        }
        $menu = $this->menuModel->create($menuData);
        return redirect()->route('admin.menu.index')->with('success', trans('messages.success.create', ['Module' => trans('module.entry.menu')]));
    }
    public function edit(Request $request, $id)
    {
        $menu = $this->menuModel->findOrFail($id);
        $this->authorize('edit', $menu);
        $menuLocations = config('adiva.menu_locations');
        return view('admin.templates.menu.edit')->with([
            'menu' => $menu,
            'menuLocations' => $menuLocations
        ]);
    }
    public function update(Request $request, $id)
    {
        $menu = $this->menuModel->findOrFail($id);
        $this->authorize('update', $menu);
        $this->validate($request, [
          'name' => 'required',
          'url' => 'required',
          'location' => 'required'
        ],[],[
            'name' =>  trans('table_fields.menus.name'),
            'url' =>  trans('table_fields.menus.url'),
            'location' =>  trans('table_fields.menus.location')
        ]);
        $menuData = $request->except('_token');
        if($request->has('show') && in_array($request->show, [0,1])){
            $menuData['show'] = $request->show;
        }else{
            $menuData['show'] = 0;
        }
        $menu->fill($menuData)->save();
        return redirect()->route('admin.menu.index')->with('success', trans('messages.success.update', ['Module' => trans('module.entry.menu')]));
    }
    public function updateshow(Request $request, $id)
    {
        $menu = $this->menuModel->findOrFail($id);
        $this->authorize('update', $menu);
        $postData = [];
        if($request->has('show') && in_array($request->show, [0,1])){
            $postData['show'] = $request->show;
        }else{
            $postData['show'] = 0;
        }
        $menu->fill($postData)->save();
        if($request->ajax()){
            return response()->json([
                'status' => true
            ]);
        }
        return redirect()->route('admin.menu.index')->with('success', trans('messages.success.update', ['Module' => trans('module.entry.menu')]));
    }
    public function destroy($id)
    {
        $menu = $this->menuModel->findOrFail($id);
        $this->authorize('destroy', $menu);
        $this->menuModel->destroy($id);
        return redirect()->route('admin.menu.index')->with('success', trans('messages.success.delete', ['Module' => trans('module.entry.menu')]));
    }

}
