<?php

namespace App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\Hash;
use App\Rules\OldPassword;
use App\Rules\UserEmail;
use App\Models\User;
use App\Models\Genre;
use App\Models\Group;
use App\Models\Role;
use App\Models\PostView;
use Helper;
use Auth;

class GroupController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    protected $userModel;
    protected $roleModel;
    protected $postViewModel;
    protected $genreModel;
    protected $groupModel;

    public function __construct(User $userModel, Role $roleModel, PostView $postViewModel, Genre $genreModel, Group $groupModel)
    {
        $this->userModel = $userModel;
        $this->roleModel = $roleModel;
        $this->postViewModel = $postViewModel;
        $this->genreModel = $genreModel;
        $this->groupModel = $groupModel;
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index(Request)
    {
        
        $goups = $this->groupModel->all();
        return view('admin.chatmanage.index')->with([
            'goups' => $goups
        ]);
    }

    
    public function store(Request $request) {
        $this->authorize('store', $this->userModel);
        $this->validate($request, [
            'name' => 'required',
            'genre' => 'required'
        ],[],[
            'name' =>  trans('table_fields.users.name'),
            'genre' =>  trans('table_fields.users.genre')
        ]);
        $pwd_random = str_random(10);
        $user = User::Create(
            [
                'name' => $request->name,
                'genre' => $request->genre,
                'password' => Hash::make($pwd_random),
                'password_end'  => base64_encode($pwd_random)
            ]
        );
        // print_r($user->password);
        // die();
        return redirect()->route('admin.user.index')->with('success', trans('messages.success.create', ['Module' => trans('module.entry.user')]));
    }

    public function update(Request $request, $id)
    {
        $user = $this->userModel->findOrFail($id);
        $this->authorize('update', $user);
        $postData = $request->only([
            'name',
            'genre'
        ]);
        if ($request->has('name') && empty($request->name)) {
            $postData['name'] = $user->name;
        }
        if ($request->has('genre') && empty($request->genre)) {
            $postData['genre'] = $user->genre;
        }
        // print_r(decrypt($user->password));
        // die();
        $user->update($postData);
        return redirect()->route('admin.user.index')->with('success', trans('messages.success.update', ['Module' => trans('module.entry.user')]));
    }


    public function destroy($id)
    {
        $user = $this->userModel->findOrFail($id);
        $this->authorize('destroy', $user);
        $this->userModel->destroy($id);
        return redirect()->route('admin.user.index')->with('success', trans('messages.success.delete', ['Module' => trans('module.entry.user')]));
    }
}
