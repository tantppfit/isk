<?php

namespace App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use App\Models\Post;
use App\Models\PostView;
use App\Models\Tag;
use App\Models\Category;
use App\Models\Media;
use App\Models\User;
use App\Models\Vote;
use Helper;
use Auth;
use Carbon\Carbon;

class PostController extends Controller
{
    protected $postModel;
    protected $postViewModel;
    protected $tagModel;
    protected $categoryModel;
    protected $mediaModel;
    protected $voteModel;
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct( Post $postModel, PostView $postViewModel, Tag $tagModel, Category $categoryModel, Media $mediaModel, User $userModel, Vote $voteModel )
    {
        $this->postModel = $postModel;
        $this->postViewModel = $postViewModel;
        $this->tagModel = $tagModel;
        $this->categoryModel = $categoryModel;
        $this->mediaModel = $mediaModel;
        $this->userModel = $userModel;
        $this->voteModel = $voteModel;
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index(Request $request)
    {
        $this->authorize('index', $this->postModel);
        $posts = $this->postModel->getPosts($request);
        $users = $this->userModel->all();
        return view('admin.templates.post.index')->with([
            'posts' => $posts,
            'users' => $users
        ]);
    }
    public function indexBySite(Request $request)
    {
        $this->authorize('indexBySite', $this->postModel);
        $parentCategories = $this->categoryModel->getParentCategories();
        $tags = $this->tagModel->all();
        $users = $this->userModel->all();
        if(Auth::user()->role->slug == 'admin'){
            $siteId = Auth::user()->site_id;
            $posts = $this->postModel->getPostsBySite($request, $siteId);
        }else{
            $userId = Auth::user()->id;
            $posts = $this->postModel->getPostsByUser($request, $userId);
        }
        return view('admin.templates.site.post.index')->with([
            'posts' => $posts,
            'parentCategories' => $parentCategories,
            'tags' => $tags,
            'users' => $users
        ]);
    }

    public function create(Request $request)
    {
        $user = Auth::user();
        $post = $this->postModel->create([
            'user_id' => $user->id,
            'site_id' => $user->site_id
        ]);
        $slug = self::incrementSlug($post->id);
        $post->slug = $slug;
        $post->save();
        return redirect()->route('admin.post.edit', $post);
    }

    public function getViewLog(Request $request, $id)
    {
        $post = $this->postModel->findOrFail($id);
        $postStatistic = $this->postViewModel->getViewLogByPost($post);
        return response()->json([
            'status' => true,
            'data' => $postStatistic
        ]);
    }

    public function preedit(Request $request, $id)
    {
        $post = $this->postModel->findOrFail($id);
        return view('admin.templates.post.preedit')->with([
            'post' => $post
        ]);
    }

    public function edit(Request $request, $id)
    {
        $post = $this->postModel->findOrFail($id);
        $this->authorize('edit', $post);
        $parentCategories = $this->categoryModel->getParentCategories();
        $tags = $this->tagModel->all();
        $votes = $this->voteModel->all();
        $voteCountData = $post->getVoteCountData();
        return view('admin.templates.post.edit')->with([
            'post' => $post,
            'parentCategories' => $parentCategories,
            'tags' => $tags,
            'votes' => $votes,
            'voteCountData' => $voteCountData
        ]);
    }

    public function preUpdate(Request $request, $id)
    {
        $postData = $request->only(['title', 'content']);
        $post = $this->postModel->findOrFail($id);
        $post->update($postData);
        return redirect()->route('admin.post.preedit', $post)->with('success', trans('messages.success.create', ['Module' => trans('module.entry.post')]));
    }

    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'title' => 'required',
            'content' => 'required',
            'slug' => 'required',
        ],[],[
            'title' =>  trans('table_fields.posts.title'),
            'content' =>  trans('table_fields.posts.content'),
            'slug' =>  trans('table_fields.posts.slug')
        ]);
        $post = $this->postModel->findOrFail($id);
        if(!$request->has('vote_status')){
            $request->merge(['vote_status'=>false]);
        }
        $attributes = $request->except('_token');
        if(isset($attributes['created_at'])){
            $attributes['created_at'] = Carbon::parse($attributes['created_at']);
        }
        if(isset($attributes['vote_expire_time']) && !empty($attributes['vote_expire_time'])){
            $attributes['vote_expire_time'] = Carbon::parse($attributes['vote_expire_time']);
        }
        if($post->vote_id != $attributes['vote_id']){
            $attributes['vote_count'] = json_encode(['opt_1'=>0,'opt_2'=>0,'opt_3'=>0,'opt_4'=>0,'opt_5'=>0]);
        }
        if($attributes['slug'] != $post->slug){
            $slug = self::incrementSlug(Helper::toSlug($attributes['slug']));
            $attributes['slug'] = $slug;
        }
        $catPostData = ($request->has('categories')) ? $request->categories : [];
        $post->categories()->sync($catPostData);
        $tagPostData = [];
        $tags = ($request->has('tags')) ? $request->tags : [];
        foreach ($tags as $key => $tag) {
            $tagPostData[$tag] = ['tag_order' => $key+1];
        }
        $post->tags()->sync($tagPostData);
        $post->fill($attributes)->save();
        return redirect()->route('admin.post.edit', $post)->with('success', trans('messages.success.create', ['Module' => trans('module.entry.post')]));
    }

    public function uploadFeatureImage(Request $request, $id)
    {
        $this->validate($request, [
            'file' => 'image|required|mimes:jpeg,png,jpg'
        ],[],[
            'file' => trans('table_fields.medias.file')
        ]);
        $imageUploaded = Helper::uploadMedia($request->file);
        $attributes = [
            'name' => $imageUploaded['file_name'],
            'info' => $imageUploaded['info'],
            'post_id' => $id,
            'is_cover' => true
        ];
        foreach ($imageUploaded['size_paths'] as $key => $path) {
            $attributes[$key . '_path'] = $path;
        }
        $media = $this->mediaModel->create($attributes);
        $post = $this->postModel->find($id);
        $post->cover_image = $media->id;
        $post->save();
        $url = env('APP_URL') . Helper::getMediaUrl($media, 'ratio4x3');
        return response()->json([
            'uploaded' => true,
            'url' => $url,
            'id' => $media->id,
            'action_delete' => route('admin.media.destroy', $media)
        ]);
    }

    public function uploadSliderImages(Request $request, $id)
    {
        $this->validate($request, [
            'files.*' => 'image|required|mimes:jpeg,png,jpg'
        ],[],[
            'files' =>  trans('table_fields.medias.files'),
        ]);
        $paths = Helper::uploadSlideImages($request->file('files'), $id);
        $post = $this->postModel->find($id);
        if(is_null(json_decode($post->slide_images))){
            $post->slide_images = json_encode($paths);
        }else{
            $post->slide_images = json_encode( array_merge( json_decode($post->slide_images, true), $paths ) );
        }
        $post->save();
        $images = [];
        foreach($paths as $key => $path){
            $images[] = [
                'path' => $path,
                'url'   => Helper::getImageUrl($path)
            ];
        }
        return response()->json([
            'status' => true,
            'images' => $images
        ]);
    }
    public function removeSliderImage(Request $request, $id)
    {
        $this->validate($request, [
            'path' => 'required'
        ],[],[
            'path' =>  trans('table_fields.post_sliders.slider_image_path')
        ]);
        Helper::deleteImage($request->path);
        $post = $this->postModel->find($id);
        
        if(!is_null(json_decode($post->slide_images, true))){
            $images = json_decode($post->slide_images, true);
            if (($key = array_search($request->path, $images)) !== false) {
                unset($images[$key]);
            }
            $post->slide_images = json_encode($images);
            $post->save();
        }
        return response()->json([
            'status' => true
        ]);
    }

    public function updateFeatureImage(Request $request, $id)
    {
        $this->validate($request, [
            'media_id' => 'required'
        ],[],[
            'media_id' =>  trans('table_fields.medias.id')
        ]);
        $post = $this->postModel->find($id);
        $post->cover_image = $request->media_id;
        $post->save();
        return response()->json([
            'status' => true
        ]);
    }

    public function updateFeatureStyle(Request $request, $id)
    {
        $this->validate($request, [
            'cover_image_style' => 'required'
        ],[],[
            'cover_image_style' =>  trans('table_fields.posts.cover_image_style')
        ]);
        $post = $this->postModel->find($id);
        $post->cover_image_style = $request->cover_image_style;
        $post->save();
        return response()->json([
            'status' => true
        ]);
    }

    public function addTags(Request $request)
    {
        $this->validate($request, [
            'name' => 'required'
        ],[],[
            'name' =>  trans('table_fields.tags.name')
        ]);
        $tagNames = preg_split('/[,、]/', $request->name);
        $tagNames = array_filter($tagNames, function($value) { return !is_null($value) && $value !== ''; });
        $tags = $this->tagModel->createTags($tagNames);
        return response()->json([
            'status' => true,
            'tags' => $tags->toArray()
        ]);
    }

    public function addPreviewer(Request $request, $id)
    {
        $this->validate($request, [
            'email' => 'required|email'
        ],[],[
            'email' =>  trans('table_fields.users.email')
        ]);
        $user = $this->userModel->getUserByEmail($request->email);
        if($user){
            $post = $this->postModel->findOrFail($id);
            if($post->user_id !== $user->id){
                $post->previewers()->syncWithoutDetaching([$user->id]);
            }            
            if(Auth::user()->role->slug == 'admin'){
                $message = trans('messages.success.added_previewer_admin');
            }else{
                $message = trans('messages.success.added_previewer_user');
            }       
            //$message = trans('messages.success.added_previeweradmin');
        }else{
            $message = trans('messages.error.email_does_not_exist');
        }
        return response()->json([
            'status' => true,
            'message' => $message
        ]);
    }

    public function changeAll(Request $request){
        if($request->has('redirect')){
            $redirect = $request->redirect;
        }else{
            $redirect = route('admin.post.indexBySite');
        }
        if(!$request->has('_action') || empty($request->_action)){
            return redirect($redirect)->with('error', trans('messages.error.update', ['Module' => trans('module.entries.post')]));
        }
        if($request->has('post_ids') && !empty($request->post_ids)){
            $post_ids = explode(",", $request->post_ids);
            if($request->_action == 'update_categories'){
                if($request->has('category_id') && !empty($request->category_id)){
                    $tmpCats = [$request->category_id];
                    foreach ($post_ids as $post_id) {
                        $post = $this->postModel->find($post_id);
                        if($post){
                            $newCatData = [];
                            $postCatIds = $post->categories->pluck('id')->toArray();
                            if($request->has('cat_action') && $request->cat_action == 'remove'){
                                $postCatIds = array_diff($postCatIds, $tmpCats);
                            }else{
                                $postCatIds = array_merge($postCatIds, $tmpCats);
                            }
                            $post->categories()->sync($postCatIds);
                        }
                    }
                }
            }
            if($request->_action == 'update_tags'){
                if($request->has('tag_id') && !empty($request->tag_id)){
                    $tmpTags = [$request->tag_id];
                }else{
                    $tmpTags = [];
                }
                if($request->has('name') && !empty($request->name)){
                    $tagNames = preg_split('/[,、]/', $request->name);
                    $tagNames = array_filter($tagNames, function($value) { return !is_null($value) && $value !== ''; });
                    $newTags = $this->tagModel->createTags($tagNames);
                    $tmpTags = array_merge($tmpTags, $newTags->pluck('id')->toArray());
                }
                
                foreach ($post_ids as $post_id) {
                    $post = $this->postModel->find($post_id);
                    if($post){
                        $newTagData = [];
                        $postTagIds = $post->tags->pluck('id')->toArray();
                        if($request->has('tag_action') && $request->tag_action == 'remove'){
                            $postTagIds = array_diff($postTagIds, $tmpTags);
                        }else{
                            $postTagIds = array_merge($postTagIds, $tmpTags);
                        }                        
                        foreach ($postTagIds as $key => $tag) {
                            $newTagData[$tag] = ['tag_order' => $key+1];
                        }
                        $post->tags()->sync($newTagData);
                    }
                }
            }
            if($request->_action == 'update_status'){
                if($request->has('post_status')){
                    foreach ($post_ids as $post_id) {
                        $post = $this->postModel->find($post_id);
                        if($post){
                            $post->post_status = $request->post_status;
                            $post->save();
                        }
                    }
                }
            }
            if($request->_action == 'update_author'){
                if($request->has('user_id') && !empty($request->user_id)){
                    foreach ($post_ids as $post_id) {
                        $post = $this->postModel->find($post_id);
                        if($post){
                            $post->user_id = $request->user_id;
                            $post->save();
                        }
                    }
                }
            }
            if($request->_action == 'update_created_at'){
                if($request->has('created_at') && !empty($request->created_at)){
                    foreach ($post_ids as $post_id) {
                        $post = $this->postModel->find($post_id);
                        if($post){
                            $post->created_at = Carbon::parse($request->created_at);
                            $post->save();
                        }
                    }
                }
            }
            if($request->_action == 'delete'){
                $this->postModel->destroy($post_ids);
            }
        }
        return redirect($redirect)->with('success', trans('messages.success.update', ['Module' => trans('module.entries.post')]));

    }

    public function destroy($id)
    {
        $post = $this->postModel->findOrFail($id);
        $this->authorize('destroy', $post);
        $this->postModel->destroy($id);
        // return redirect()->route('admin.post.indexBySite')->with('success', trans('messages.success.delete', ['Module' => trans('module.entry.post')]));
        return redirect()->back()->with('success', trans('messages.success.delete', ['Module' => trans('module.entry.post')]));
    }

    //slug functions
    public function incrementSlug($slug) {
        $original = $slug;
        $count = 2;
        while ($this->postModel->whereSlug($slug)->exists()) {
            $slug = "{$original}-" . $count++;
        }
        return $slug;

    }
}
