<?php

namespace App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;

use App\Models\Media;
use App\Models\Post;
use Helper;

class MediaController extends Controller
{
    protected $mediaModel;
    protected $postModel;
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(Media $mediaModel, Post $postModel)
    {
        $this->mediaModel = $mediaModel;
        $this->postModel = $postModel;
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index(Request $request)
    {
        $this->authorize('index', Media::class);
        $medias = $this->mediaModel->getMedias($request);
        return view('admin.templates.media.index')->with([
            'medias' => $medias
        ]);
        return view('admin.templates.media.index');
    }

    public function store(Request $request)
    {
        $this->authorize('store', Media::class);
        $this->validate($request, [
            'file' => 'image|required|mimes:jpeg,png,jpg'
        ],[],[
            'file' => trans('table_fields.medias.file')
        ]);
        $imageUploaded = Helper::uploadMedia($request->file);
        $attributes = [
            'name' => $imageUploaded['file_name'],
            'info' => $imageUploaded['info']
        ];
        foreach ($imageUploaded['size_paths'] as $key => $path) {
            $attributes[$key . '_path'] = $path;
        }
        $this->mediaModel->create($attributes);
        return redirect()->route('admin.media.index')->with('success', trans('messages.success.create', ['Module' => trans('module.entry.media')]));
    }

    public function ckeditorPostUploadImage(Request $request, $id)
    {
        $this->validate($request, [
            'upload' => 'image|required|mimes:jpeg,png,jpg'
        ],[],[
            'upload' => trans('table_fields.medias.file')
        ]);
        $imageUploaded = Helper::uploadMedia($request->upload);
        $attributes = [
            'name' => $imageUploaded['file_name'],
            'info' => $imageUploaded['info'],
            'post_id' => $id
        ];
        foreach ($imageUploaded['size_paths'] as $key => $path) {
            $attributes[$key . '_path'] = $path;
        }
        $media = $this->mediaModel->create($attributes);
        /*$post = $this->postModel->find($id);
        if(!$post->cover_image){
            $post->cover_image = $media->id;
            $post->save();
        }*/
        $url = Helper::getMediaUrl($media, 'original');
        return response()->json([
            'uploaded' => true,
            'url' => $url
        ]);
    }

    public function modalGetMedias(Request $request)
    {
        $medias = $this->mediaModel->orderBy('id', 'desc')->get();
        return response()->json([
            'status' => true,
            'html' => view('admin.templates.media.parts.content-modal-library')->with([
                'medias' => $medias
            ])->render()
        ]);
    }
    public function modalUploadMedia(Request $request)
    {
        $this->validate($request, [
            'file' => 'image|required|mimes:jpeg,png,jpg'
        ],[],[
            'file' => trans('table_fields.medias.file')
        ]);
        $imageUploaded = Helper::uploadMedia($request->file);
        $attributes = [
            'name' => $imageUploaded['file_name'],
            'info' => $imageUploaded['info']
        ];
        foreach ($imageUploaded['size_paths'] as $key => $path) {
            $attributes[$key . '_path'] = $path;
        }
        $media = $this->mediaModel->create($attributes);
        return response()->json([
            'status' => true,
            'html' => view('admin.templates.media.parts.item-media-modal')->with([
                'media' => $media
            ])->render()
        ]);
    }

    public function destroy(Request $request, $id)
    {
        $media = $this->mediaModel->findOrFail($id);
        $this->authorize('destroy', $media);
        Helper::deleteMedia($media);
        $this->mediaModel->destroy($id);
        if($request->ajax()){
            return response()->json([
                'status' => true
            ]);
        }
        // return redirect()->route('admin.media.index')->with('success', trans('messages.success.delete', ['Module' => trans('module.entry.media')]));
        return redirect()->back()->with('success', trans('messages.success.delete', ['Module' => trans('module.entry.media')]));
    }

}
