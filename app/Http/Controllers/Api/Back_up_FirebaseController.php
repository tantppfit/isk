<?php

namespace App\Http\Controllers\Api;
use App\Http\Controllers\Controller;
use Kreait\Firebase;
use Kreait\Firebase\Factory;
use Kreait\Firebase\ServiceAccount;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Crypt;
use App\Rules\OldPassword;
use App\Rules\UserEmail;
use App\Models\User;
use App\Models\Genre;
use App\Models\Role;
use App\Models\PostView;
use App\Models\ChatManage;
use App\Models\Media;
use Helper;
use Auth;

class FirebaseController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    protected $chatmanageModel;
    protected $userModel;
    protected $roleModel;
    protected $postViewModel;
    protected $genreModel;
    protected $mediaModel;
    /**
     * Create a new controller instance.
     *
     * @return void
     */

    public function __construct(User $userModel, Role $roleModel, PostView $postViewModel, Genre $genreModel, ChatManage $chatmanageModel, Media $mediaModel)
    {
        $this->chatmanageModel  = $chatmanageModel;
        $this->userModel        = $userModel;
        $this->roleModel        = $roleModel;
        $this->postViewModel    = $postViewModel;
        $this->genreModel       = $genreModel;
        $this->mediaModel       = $mediaModel;
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index(Request $request)
    {
        $this->authorize('index', $this->userModel);
        $users = $this->userModel->getUsers($request);
        $genres = $this->genreModel->all();
        // echo $users;die();
        //$users  = user::orderBy('created_at', 'desc')->paginate(5);
        return view('admin.chatmanage.index')->with([
            'users' => $users,
            'genres' => $genres
        ]);
    }

    public function firebase(Request $request)
    {
        $serviceAccount = ServiceAccount::fromJsonFile(__DIR__.'/isk-chat-firebase-adminsdk-ci29k-6457f7bed0.json');
        $firebase = (new Factory)
            ->withServiceAccount($serviceAccount)
            ->withDatabaseUri('https://isk-chat.firebaseio.com/')
            ->create();
        $database = $firebase->getDatabase();
        // $chatData = array('created_at' => $request->get('created_at'),
        //                   'file_url' => $request->get('file_url'));
        $chatData = $request->all();
        $data_parse = json_encode($chatData);
        //$data1 = json_decode($chatData);  
        $data = json_decode($data_parse); 
        // echo $data->_id;
        // echo $data->avatar;
        // echo $data->created_at;
        // echo $data->file_url;
        // echo $data->image_url;
        // echo $data->name;
        // die;
        if (isset($_POST['file_url']) == null) {
            $file_url = '';
        }
        if (isset($_POST['image_url']) == null) {
            $image_url = '';
        }
        if (isset($_POST['text']) == null) {
            $text = '';
        }
        if ($data->file_url != null) {
            // $this->validate($data, [
            // 'file_url' => 'file|required|mimes:csv,pdf,docx,xlsx,pptx'
            // ],[],[]);
            $imageUploaded = Helper::uploadAvatar($data->file_url, $data->_id);
            $attributes = [
                'name' => $imageUploaded['name'],
                'user_id' => $data->_id
            ];
            foreach ($imageUploaded['size_paths'] as $key => $path) {
                $attributes[$key . '_path'] = $path;
            }
            $media = $this->mediaModel->create($attributes);
            $file_url = Helper::getMediaUrl($media, 'original');
            return response()->json([
                'uploaded' => true,
                'file_url' => $file_url
            ]);
        }
        //echo $this->$request->get('image_url'));
        
        // if(isset($request['_id']) == TRUE){ 
        //     $_id = $request['_id'];
        // } else {
        //     $_id = '1';
        // }    
        if ($data->image_url != null) {
            // $this->validate($data, [
            // 'image_url' => 'image|required|mimes:jpeg,png,jpg'
            // ],[],[]);
            $imageUploaded = Helper::uploadAvatar($data->image_url, $data->_id);
            $attributes = [
                'name' => $imageUploaded['name'],
                'user_id' => $data->_id
            ];
            foreach ($imageUploaded['size_paths'] as $key => $path) {
                $attributes[$key . '_path'] = $path;
            }
            $media = $this->mediaModel->create($attributes);
            $image_url = Helper::getMediaUrl($media, 'original');
            return response()->json([
                'uploaded' => true,
                'image_url' => $image_url
            ]);
        }
            
        $dataPush = ([  
                        'created_at'    => $data->created_at, 
                        'file_url'      => $file_url,
                        'image_url'     => $image_url, 
                        'text'          => $text, 
                        'user'          => 
                                            [   
                                                '_id'      => $data->_id,
                                                'avatar'    => $data->avatar,
                                                'name'      => $data->name
                                            ]
                    ]);
        //echo $chatData;
        $user = Auth::user(); 
        $_id = $data->_id;
        $newPost = $database
        ->getReference('Messagers/'.$_id)
        ->push($dataPush);
        $success['token'] =  true; 
        //$user_id['id'] = $user->id;
        return response()->json(['success' => $success]); 
        // } 
        // else{ 
        //     return response()->json(['error'=>'Unauthorised'], 401); 
        // } 
        $getFirstChat = $database
            ->getReference('Messagers/'.$_id)
            ->getSnapshot();
            // ->orderByChild('content')
            // // limits the result to the first 10 children (in this case: the 10 shortest persons)
            // // values for 'height')
            // ->limitToFirst(10)
            // ->getSnapshot();
        //echo '123';
        //print_r($getFirstChat->getvalue());        /
        //$messages = $getFirstChat->getvalue();
        //print_r(implode($messages, 'Array'));die;
        //$messages = array('return' => true, 'message' => $messages);
        //header('Content-type: text/javascript');
        //echo json_encode($Data);die;
        //print_r($messages);die;
        // $this->authorize('index', $this->userModel);
        // $users = $this->userModel->getUsers($request);
        // $genres = $this->genreModel->all();
        // return view('admin.chatmanage.firebase')->with([
        //     'messages'  => $messages,
        //     'users'     => $users,
        //     'id'        => $id,
        //     'genres'    => $genres
        // ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
