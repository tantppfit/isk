<?php

namespace App\Http\Controllers\Admin;
namespace App\Http\Controllers\Api;
use App\Http\Controllers\Controller;
use Kreait\Firebase;
use Kreait\Firebase\Factory;
use Kreait\Firebase\ServiceAccount;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Log;
use App\Rules\OldPassword;
use App\Rules\UserEmail;
use App\Models\User;
use App\Models\Genre;
use App\Models\Role;
use App\Models\ChatManage;
use App\Models\Media;
use Helper;
use Auth;
use Mail;

class ImageFirebaseController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    protected $chatmanageModel;
    protected $userModel;
    protected $roleModel;
    //protected $postViewModel;
    protected $genreModel;
    protected $mediaModel;
    /**
     * Create a new controller instance.
     *
     * @return void
     */

    public function __construct(User $userModel, Role $roleModel, Genre $genreModel, ChatManage $chatmanageModel, Media $mediaModel)
    {
        $this->chatmanageModel  = $chatmanageModel;
        $this->userModel        = $userModel;
        $this->roleModel        = $roleModel;
        //$this->postViewModel    = $postViewModel;
        $this->genreModel       = $genreModel;
        $this->mediaModel       = $mediaModel;
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index(Request $request)
    {
        $this->authorize('index', $this->userModel);
        $users = $this->userModel->getUsers($request);
        $genres = $this->genreModel->all();
        // echo $users;die();
        //$users  = user::orderBy('created_at', 'desc')->paginate(5);
        return view('admin.chatmanage.index')->with([
            'users' => $users,
            'genres' => $genres
        ]);
    }

    public function imagefirebase(Request $request)
    {
        $chatData = $request->all();
        $jData = json_encode($chatData);
        $data = json_decode($jData);
        //$_id = $data->_id;
        $_id = $request->_id; 
        //Log::debug($request);
        //$created_at = $data->created_at;
        //$created_at = str_replace("T"," ",$data->created_at);
        $created_at = date("Y-m-d H:i:s",time());

        // $this->validate($request, [
        // 'image_url' => 'required|image|mimes:png,image/heif,image/heic,image/heif-sequence,image/heic-sequence,jpeg,jpg,bmp,gif,svg|max:51200',
        // ]);
        //print_r($request->image_url);
        //die;
            //Log::debug('Hello'.$request->image_url);
        if ($request->hasFile('image_url')) {
            $image = $request->file('image_url'); 
            //Log::debug($image);                   
            $imageName = $image->getClientOriginalName();
            //$imageFormat = $image->getClientOriginalExtension(); 
            $newName = time().'_'.$imageName;
            $destinationPath = 'media/'.$_id.'/img/'.$newName;
            $movePath = 'storage/media/'.$_id.'/img';
            $image->move($movePath, $newName.'.jpg');
            $attributes = [
                'name'          => $imageName.'.jpg',
                'original_path' => $destinationPath.'.jpg',
                'thumbnail_path' => null,
                'user_id'       => $_id,
                'created_at'    => $created_at,
                'updated_at'    => null
            ];
            $this->mediaModel->create($attributes);
            $url = asset(Storage::url($destinationPath));
            //Log::debug('Hello'.$request->image_url);
            //print_r($request->image_url);
            //die();
            //$this->save();
            //return back()->with('success','Image Upload successfully');
        }
        $to_name = "Admin";
        $to_email = 'comp.doke@gmail.com';
        $data = array('name' => $_id, 
                      'body' => 'Test send mail when user send file');
        Mail::send('emails.mail', $data, function($message) use ($to_name, $to_email) {
        $message->to($to_email, $to_name)
        ->subject('Test send mail');
        $message->from('gisol.ltd@gmail.com','TPP-FIT');
        });
        $serviceAccount = ServiceAccount::fromJsonFile(__DIR__.'/isk-chat-firebase-adminsdk-ci29k-6457f7bed0.json');
        $firebase = (new Factory)
            ->withServiceAccount($serviceAccount)
            ->withDatabaseUri('https://isk-chat.firebaseio.com/')
            ->create();
        $database = $firebase->getDatabase();

            
        $dataPush = ([  
                        'created_at'    => $created_at, 
                        'image'         => $url.'.jpg',
                        'user'          => 
                                            [   
                                                '_id'      => $_id,
                                                // 'avatar'    => $data->avatar,
                                                'name'      => $request->name
                                            ]
                    ]);
        //echo $chatData;
        $user = Auth::user(); 
        $_id = $request->_id;
        $newPost = $database
        ->getReference('Messagers/'.$_id)
        ->push($dataPush);
        $success['token'] =  true; 
        //$user_id['id'] = $user->id;
        return response()->json(['success' => $success]); 
        // } 
        // else{ 
        //     return response()->json(['error'=>'Unauthorised'], 401); 
        // } 
        $getFirstChat = $database
            ->getReference('Messagers/'.$_id)
            ->getSnapshot();
            // ->orderByChild('content')
            // // limits the result to the first 10 children (in this case: the 10 shortest persons)
            // // values for 'height')
            // ->limitToFirst(10)
            // ->getSnapshot();
        //echo '123';
        //print_r($getFirstChat->getvalue());        /
        //$messages = $getFirstChat->getvalue();
        //print_r(implode($messages, 'Array'));die;
        //$messages = array('return' => true, 'message' => $messages);
        //header('Content-type: text/javascript');
        //echo json_encode($Data);die;
        //print_r($messages);die;
        // $this->authorize('index', $this->userModel);
        // $users = $this->userModel->getUsers($request);
        // $genres = $this->genreModel->all();
        // return view('admin.chatmanage.firebase')->with([
        //     'messages'  => $messages,
        //     'users'     => $users,
        //     'id'        => $id,
        //     'genres'    => $genres
        // ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
