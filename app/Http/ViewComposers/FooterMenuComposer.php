<?php

namespace App\Http\ViewComposers;

use App\Models\Menu;
use Illuminate\View\View;

class FooterMenuComposer
{
    /**
     * The user repository implementation.
     *
     */
    protected $menuModel;

    /**
     * Create a new profile composer.
     *
     * @param MenuService $menuService
     */
    public function __construct(Menu $menuModel)
    {
        // Dependencies automatically resolved by service container...
        $this->menuModel = $menuModel;
    }

    /**
     * Bind data to the view.
     *
     * @param  View $view
     * @return void
     */
    public function compose(View $view)
    {
        $footerMenus1 = $this->menuModel->getMenus('footer_menu_1', 1);
        $footerMenus2 = $this->menuModel->getMenus('footer_menu_2', 1);
        $footerMenus3 = $this->menuModel->getMenus('footer_menu_3', 1);
        $view->with([
            'footerMenus1' => $footerMenus1,
            'footerMenus2' => $footerMenus2,
            'footerMenus3' => $footerMenus3
        ]);
    }

}