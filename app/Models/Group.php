<?php

namespace App\Models;

use Laravel\Passport\HasApiTokens;
use App\Adiva\Entrust\Traits\EntrustUserTrait;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Foundation\Auth\User as Authenticatable;
use App\Notifications\ResetPassword as ResetPasswordNotification;
use Carbon\Carbon;
use Kyslik\ColumnSortable\Sortable;

class Group extends Model
{
    use Notifiable;
    use EntrustUserTrait;
    use HasApiTokens;
    use Sortable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $table = 'group';
    protected $fillable = [
        'name', 'listid', 'updated_at'
    ];
    public $sortable = ['created_at','updated_at'];
    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    
    public function getGroup($request){
        $query = $this->query();
        $query->orderBy('id','desc');
        return $query;
    }

    public function getUserById($id){
        $query = $this->query();
        $query->where('id', $id);
        // echo $query->first();
        return $query->first();
    }
    public function getUserByEmail($email){
        $query = $this->query();
        $query->where('email', $email);
        return $query->first();
    }
    
    public function getUsersByView($request){
        $query = $this->query();
        if($request->has('id') && !empty($request->id)){
            $query->where('id', $request->id);
        }
        if($request->has('order') && in_array($request->order, ['asc', 'desc'])){
            $query->withCount('lastMonthTotalViews')->orderBy('last_month_total_views_count', $request->order);
        }else{
            $query->withCount('lastMonthTotalViews')->orderBy('last_month_total_views_count', 'desc');
        }
        return $query->paginate(20);
    }

    
    public function getUsers($request) {
        $query = $this->query();
        $query->where('role_id', '=', 2)
              ->orderBy('created_at','desc');
        // $query->join('genre', 'genre.id', '=', 'users.genre')
        //       ->select('genre.name')
        //       ->get();  
        if($request->has('username') && !empty($request->username)){
          $query->where('name', 'like', '%' . $request->username . '%');
        }
        if ($request->has('genre') && !empty($request->genre)) {
            $query->where('genre', $request->genre);
        }
        if ($request->has('date') && !is_null($request->date)) {
            if($request->date == 'create') {
               if($request->has('from') && !empty($request->from)){
                    $query->whereDate('created_at', '>=', Carbon::parse($request->from));
                }
                if($request->has('to') && !empty($request->to)){
                    $query->whereDate('created_at', '<=', Carbon::parse($request->to));
                } 
            }
            if ($request->date == 'update') {
                if($request->has('from') && !empty($request->from)){
                    $query->whereDate('updated_at', '>=', Carbon::parse($request->from));
                }
                if($request->has('to') && !empty($request->to)){
                    $query->whereDate('updated_at', '<=', Carbon::parse($request->to));
                }
            }
        }
        return $query->paginate();
    }

    // public function getUsersBySearch($request) {
    //     $query = $this->query();
    //     $query->where('name', 'like', '%' . $request->search . '%')
    //           ->get();
    // }

    public function sendPasswordResetNotification($token)
    {
        $this->notify(new ResetPasswordNotification($token));
    }

    public function AauthAcessToken(){
        return $this->hasMany('\App\OauthAccessToken');
    }
}
