<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Carbon\Carbon;

class FileManage extends Model
{
	protected $table = 'files';
	protected $fillable = [
		'name', 'original_path', 'user_id', 'created_at'
	];
	public function post()
  {
    return $this->belongsTo(Post::class);
  }
  public function users($request)
  {
    return $this->belongsTo(users::class);
  }
  public function getFiles($request){
    $query = $this->query();
    $query->orderBy('id','desc');
    $query->where('user_id','=', $request->id);
    if($request->has('filename') && !empty($request->filename)){
      $query->where('name', 'like', '%' . $request->filename . '%');
    }
    if($request->has('from') && !empty($request->from)){
      $query->whereDate('created_at', '>=', Carbon::parse($request->from));
    }
    if($request->has('to') && !empty($request->to)){
      $query->whereDate('created_at', '<=', Carbon::parse($request->to));
    }
  return $query->paginate(40);
  }
  public function getFilesbyId($id){
    $query = $this->query();
    $query->orderBy('id','desc');
    $query->where('user_id','=', $id);
    return $query->paginate(40);
  }
  public function getFilebrowsingById($id)
  {
    $query = $this->query();
    $query->where('id', $id);
    return $query->first();
  }
  public function getFilebrowsingByUrl($url)
  {
    $query = $this->query();
    $query->where('original_path', $url);
    return $query->first();
  }
}