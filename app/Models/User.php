<?php

namespace App\Models;

use Laravel\Passport\HasApiTokens;
use App\Adiva\Entrust\Traits\EntrustUserTrait;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Foundation\Auth\User as Authenticatable;
use App\Notifications\ResetPassword as ResetPasswordNotification;
use Carbon\Carbon;
use Kyslik\ColumnSortable\Sortable;

class User extends Authenticatable
{
    use Notifiable;
    use EntrustUserTrait;
    use HasApiTokens;
    use Sortable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'password_end', 'avatar', 'writer_profile_html', 'facebook_id', 'twitter_id', 'receive_notification', 'genre', 'role_id', 'site_id', 'is_official_writer', 'active'
    ];
    public $sortable = ['id','name','created_at','updated_at'];
    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function site()
    {
        return $this->belongsTo(Site::class);
    }

    public function role()
    {
        return $this->belongsTo(Role::class);
    }

    public function previewPosts()
    {
        return $this->belongsToMany(Post::class);
    }
    public function lastMonthTotalViews()
    {
        return $this->hasMany(PostView::class)->whereDate('updated_at', '>', Carbon::now()->subDays(30));
    }
    public function genre()
    {
        return $this->hasOne(Genre::class);
    }

    public function getUserById($id){
        $query = $this->query();
        $query->where('id', $id);
        // echo $query->first();
        return $query->first();
    }

    public function getUserByGenreId($id){
        $query = $this->query();
        $query->where('genre', $id);
         // echo $query->get();
         // die;
        return $query->get();
    }
    public function getUserByEmail($email){
        $query = $this->query();
        $query->where('email', $email);
        return $query->first();
    }
    
    public function getUsersByView($request){
        $query = $this->query();
        if($request->has('id') && !empty($request->id)){
            $query->where('id', $request->id);
        }
        if($request->has('order') && in_array($request->order, ['asc', 'desc'])){
            $query->withCount('lastMonthTotalViews')->orderBy('last_month_total_views_count', $request->order);
        }else{
            $query->withCount('lastMonthTotalViews')->orderBy('last_month_total_views_count', 'desc');
        }
        return $query->paginate(20);
    }

    public function getOfficialWriters()
    {
        $query = $this->query();
        $query->where('is_official_writer', 1);
        return $query->paginate();
    }

    public function getUsers($request) {
        $query = $this->query();
        $query->where('role_id', '=', 2);
        if($request->has('username') && !empty($request->username)){
          $query->where('name', 'like', '%' . $request->username . '%');
        }
        if ($request->has('genre') && !empty($request->genre)) {
            $query->where('genre', $request->genre);
        }
        return $query->sortable()->paginate();
    }

    public function sendPasswordResetNotification($token)
    {
        $this->notify(new ResetPasswordNotification($token));
    }

    public function AauthAcessToken(){
        return $this->hasMany('\App\OauthAccessToken');
    }
}
