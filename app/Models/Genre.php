<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Genre extends Model
{
    protected $table = 'genre';
	protected $fillable = [
		'id', 'name'
	];

	public function users()
	{
		return $this->belongsToMany(User::class);
	}
	public function getGenre($request){
        $query = $this->query();
        $query->orderBy('id','desc');
        return $query;
    }
}
