<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Vote extends Model
{
    protected $fillable = [
        'name', 'opt_1', 'opt_2', 'opt_3', 'opt_4', 'opt_5', 'content'
    ];
    public function posts()
    {
        return $this->belongsToMany(Post::class);
    }
    public function getVotes($request, $number=15){
        $query = $this->query();
        $query->orderBy('id','desc');
        return $query->paginate($number);
    }
}