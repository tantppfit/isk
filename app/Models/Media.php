<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Media extends Model
{
	protected $table = 'medias';
	protected $fillable = [
		'name', 'original_path', 'thumbnail_path', 'user_id', 'created_at'
	];
	public function post()
  {
    return $this->belongsTo(Post::class);
  }
  public function getMedias($request){
    $query = $this->query();
    $query->orderBy('id','desc');
    if($request->has('s') && !empty($request->s)){
      $query->where('name', 'like', '%' . $request->s . '%');
    }
    if($request->has('pt') && !empty($request->pt)){
      $query->join('posts', 'posts.id', '=', 'medias.user_id')
            ->select('posts.id', 'posts.title', 'name', 'info', 'original_path', 'xlarge_path', 'large_path', 'medium_path', 'small_path', 'thumbnail_path', 'user_id', 'ratio3x2_path', 'ratio4x3_path', 'is_cover', 'user_id')
            ->where('title', 'like', '%' . $request->pt . '%');
    }
    return $query->paginate(20);
  }
  public function getMediasByUserId($request) {
    $query = $this->query();
    $query->where('user_id','=', $request->id);
    return $query->paginate(40);
  }
  public function getMediaById($id)
  {
    $query = $this->query();
    $query->where('id', $id);
    return $query->first();
  }
}