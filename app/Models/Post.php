<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Carbon\Carbon;
use Helper;

class Post extends Model
{
    protected $fillable = [
        'title', 'slug', 'content', 'cover_image', 'cover_image_style', 'cover_via_text', 'cover_via_href', 'post_status', 'user_id', 'site_id', 'html_title', 'html_meta_description', 'html_meta_keyword', 'canonical_url', 'ogp_title', 'ogp_description', 'ogp_image_url', 'excerpt', 'slide_images', 'vote_id', 'vote_count', 'vote_status', 'vote_expire_time', 'created_at', 'updated_at'
    ];
    public function user()
    {
        return $this->belongsTo(User::class);
    }
    public function previewers()
    {
        return $this->belongsToMany(User::class);
    }
    public function categories()
    {
        return $this->belongsToMany(Category::class);
    }
    public function tags()
    {
        return $this->belongsToMany(Tag::class, 'tag_post')->orderBy('tag_order', 'asc');
    }
    public function featureImage()
    {
        return $this->belongsTo(Media::class, 'cover_image');
    }
    public function medias()
    {
        return $this->hasMany(Media::class);
    }
    public function site()
    {
        return $this->belongsTo(Site::class);
    }
    public function views()
    {
        return $this->hasMany(PostView::class);
    }

    public function lastMonthTotalViews()
    {
        return $this->hasMany(PostView::class)->whereDate('updated_at', '>', Carbon::now()->subDays(30));
    }
    public function vote()
    {
        return $this->belongsTo(Vote::class);
    }
    public function sliders()
    {
        return $this->hasMany(PostSlider::class);
    }
    
    public function getPosts($request, $post_status='', $number=15){
        $query = $this->query();
        $query->orderBy('created_at','desc');
        $query->where('created_at', '<=', Carbon::now()->toDateTimeString());
        // $query->whereDate('created_at', '<=', Carbon::now()->toDateTimeString());
        // if($request->has('s')){
        //     $query->where('title','like', '%'.$request->s.'%');
        // }
        if($request->has('s') && !empty($request->s)){
            $query->where(function($query1) use ($request) {
                $query1->where('title', 'like', '%'. $request->s. '%')
                    ->orWhere('content','like', '%'.$request->s.'%');
            });
        }
        if(!empty($post_status)){
            $query->where('post_status', $post_status);
        }
        if($request->has('user_id') && !empty($request->user_id)){
            $query->where('user_id', $request->user_id);
        }
        return $query->paginate($number);
    }

    public function getPostBySlug($slug){
        $query = $this->query();
        $query->where('post_status', 1);
        $query->where('slug', $slug);
        return $query->first();
    }

    public function getPostsBySite(Request $request, $site_id){
        $query = $this->query();
        $query->orderBy('created_at','desc');
        $query->where('site_id', $site_id);
        if($request->has('user') && !empty($request->user)){
            $query->where('user_id', $request->user);
        }
        if($request->has('post_status') && !is_null($request->post_status)){
            $query->where('post_status', $request->post_status);
        }
        if($request->has('from') && !empty($request->from)){
            $query->whereDate('created_at', '>=', Carbon::parse($request->from));
        }
        if($request->has('to') && !empty($request->to)){
            $query->whereDate('created_at', '<=', Carbon::parse($request->to));
        }
        if($request->has('s') && !empty($request->s)){
            $query->where(function($query1) use ($request) {
                $query1->where('title', 'like', '%'. $request->s. '%')
                    ->orWhere('content','like', '%'.$request->s.'%');
            });
        }
        return $query->paginate(20);
    }

    public function getPostsByUser(Request $request, $user_id){
        $query = $this->query();
        $query->orderBy('created_at','desc');
        $query->where('user_id', $user_id);
        if($request->has('post_status') && !is_null($request->post_status)){
            $query->where('post_status', $request->post_status);
        }
        if($request->has('from') && !empty($request->from)){
            $query->whereDate('created_at', '>=', Carbon::parse($request->from));
        }
        if($request->has('to') && !empty($request->to)){
            $query->whereDate('created_at', '<=', Carbon::parse($request->to));
        }
        if($request->has('s') && !empty($request->s)){
            $query->where(function($query1) use ($request) {
                $query1->where('title', 'like', '%'. $request->s. '%')
                    ->orWhere('content','like', '%'.$request->s.'%');
            });
        }
        $query->orWhereHas('previewers', function( $query ) use ( $request, $user_id ){
            $query->where('user_id', $user_id );
            if($request->has('post_status') && !is_null($request->post_status)){
                $query->where('post_status', $request->post_status);
            }
            if($request->has('from') && !empty($request->from)){
                $query->whereDate('created_at', '>=', Carbon::parse($request->from));
            }
            if($request->has('to') && !empty($request->to)){
                $query->whereDate('created_at', '<=', Carbon::parse($request->to));
            }
            if($request->has('s') && !empty($request->s)){
                $query->where(function($query1) use ($request) {
                    $query1->where('title', 'like', '%'. $request->s. '%')
                        ->orWhere('content','like', '%'.$request->s.'%');
                });
            }
        });
        return $query->paginate();
    }

    public function getPostByTag($tag_id, $post_status=''){
        $query = $this->query();
        $query->orderBy('created_at','desc');
        $query->whereDate('created_at', '<=', Carbon::now()->toDateTimeString());
        $query->whereHas('tags', function($query) use ($tag_id)
        {
            $query->where('tag_id', $tag_id);
        });
        if(!empty($post_status)){
            $query->where('post_status', $post_status);
        }
        return $query->paginate(20);
    }

    public function getPostByCategory($cat_id, $post_status=''){
        $query = $this->query();
        $query->orderBy('created_at','desc');
        $query->whereDate('created_at', '<=', Carbon::now()->toDateTimeString());
        $query->whereHas('categories', function($query) use ($cat_id)
        {
            $query->where('category_id', $cat_id);
        });
        if(!empty($post_status)){
            $query->where('post_status', $post_status);
        }
        return $query->paginate(20);
    }
    
    public function getPostsByView($request, $user_id=null){
        $query = $this->query();
        if(!is_null($user_id)){
            $query->where('user_id', $user_id);
        }
        if($request->has('id') && !empty($request->id)){
            $query->where('id', $request->id);
        }
        if($request->has('order') && in_array($request->order, ['asc', 'desc'])){
            $query->withCount('lastMonthTotalViews')->orderBy('last_month_total_views_count', $request->order);
        }else{
            $query->withCount('lastMonthTotalViews')->orderBy('last_month_total_views_count', 'desc');
        }
        
        return $query->paginate(20);
    }

    public function getSliderPosts($post_status='', $number=5){
        $query = $this->query();
        $query->orderBy('created_at','desc');
        $query->where('created_at', '<=', Carbon::now()->toDateTimeString());
        if(!empty($post_status)){
            $query->where('post_status', $post_status);
        }
        return $query->limit($number)->get();
    }
    
    public function getTopPosts($number=5){
        $query = $this->query();
        $query->where('post_status', 1);
        $query->whereDate('created_at', '<=', Carbon::now()->toDateTimeString());
        $query->withCount('views')->orderBy('views_count', 'desc');
        return $query->limit($number)->get();
    }


    public function getLatestPosts($post_status='', $number=5){
        $query = $this->query();
        $query->orderBy('created_at','desc');
        $query->whereDate('created_at', '<=', Carbon::now()->toDateTimeString());
        if(!empty($post_status)){
            $query->where('post_status', $post_status);
        }
        return $query->limit($number)->get();
    }

    public function getRelatedPost($post, $number=4){
        $query = $this->query();
        $query->orderBy('id', 'desc');
        $query->whereDate('created_at', '<=', Carbon::now()->toDateTimeString());
        $query->whereHas('tags', function ($query) use ($post) {
            return $query->whereIn('tag_id', $post->tags->pluck('id')); 
        });
        $query->where('id', '!=', $post->id);
        $query->where('post_status', 1);
        return $query->limit($number)->get();
    }
    public function getVoteCountData(){
        if($this->vote_count){
            return json_decode($this->vote_count, true);
        }else{
            return ['opt_1'=>0,'opt_2'=>0,'opt_3'=>0,'opt_4'=>0,'opt_5'=>0];
        }
    }
    public function getLoadMorePost(Request $request){
        $query = $this->query();
        $query->orderBy('created_at', 'desc');
        $query->whereDate('created_at', '<=', Carbon::now()->toDateTimeString());
        $query->where('post_status', 1);
        if($request->has('list_post_id') && !empty($request->list_post_id)){
            $query->whereNotIn('id', $request->list_post_id);
        }
        if($request->has('list_tag_id') && !empty($request->list_tag_id)){
            $query->whereHas('tags', function ($query) use ($request) {
                return $query->whereIn('tag_id', $request->list_tag_id); 
            });
        }
        return $query->first();
    }
}