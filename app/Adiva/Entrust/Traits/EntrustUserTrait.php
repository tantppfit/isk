<?php

namespace App\Adiva\Entrust\Traits;
trait EntrustUserTrait {

	public function ability($route) {
		return $this->can($route);
	}

	/**
	 * Check if user has a permission by its name.
	 *
	 * @param string|array $permission Permission string or array of permissions.
	 * @param bool         $requireAll All permissions in the array are required.
	 *
	 * @return bool
	 */
	public function can($route, $arguments = null) {
		return in_array($route, $this->getCounfigRoutes());
	}

	public function getCounfigRoutes() {
		return config('adiva.adiva_roles.' . $this->role->slug);
	}
}
