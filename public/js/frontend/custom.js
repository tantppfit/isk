jQuery('document').ready(function($){
	var MAX_HEIGHT_CONTENT_TAB = 2000;
	function splitContent(content){
		if($(content).height() > MAX_HEIGHT_CONTENT_TAB){
			var tabCount = 1;
			var children = $(content).children();
			var tmp_height = 0;
			var tabContents = $('<div class="tab-contents"></div>');
			var tabLinks = $('<div class="tab-links"></div>');
			var tabContent = $('<div class="tab-content active" data-tab="'+ tabCount +'"></div>');
			var tabLink = $('<a href="#" class="active" data-tab="'+ tabCount +'">'+ tabCount +'</a>');
      for (var i = 0; i < children.length; i++) {
      	var el = $(children).eq(i);
      	if(tmp_height + el.height() <= MAX_HEIGHT_CONTENT_TAB){
      		tmp_height = tmp_height + el.height();
      		$(tabContent).append(el);
      		if(i == children.length - 1){
      			$(tabContents).append(tabContent);
      			$(tabLinks).append(tabLink);
      		}
      	}else{
      		$(tabContents).append(tabContent);
      		$(tabLinks).append(tabLink);
      		tabCount++;
      		tmp_height = 0;
      		tabContent = $('<div class="tab-content" data-tab="'+ tabCount +'"></div>');
      		tabLink = $('<a href="#" data-tab="'+ tabCount +'"">'+ tabCount +'</a>');
      		$(tabContent).append(el);
      		if(i == children.length - 1){
      			$(tabContents).append(tabContent);
      			$(tabLinks).append(tabLink);
      		}
      	}
      }
			$(content).html(tabContents);
			$(content).append(tabLinks);
		}
	}
	if($('.article-content.post-content').length){
		if($('.article-content.post-content').height() > MAX_HEIGHT_CONTENT_TAB){
			//splitContent($('.article-content.post-content'));
		}		
	}

	$('body').delegate('.tab-links a', 'click', function(e){
		e.preventDefault();
		$(this).addClass('active').siblings().removeClass('active');
		var postContent = $(this).parents('.post-content');
		var tabIndex = $(this).data('tab');
		var tabContent = $(postContent).find('.tab-content[data-tab='+tabIndex+']');
		$(tabContent).show().siblings().hide();
		$('html, body').animate({
      scrollTop: $(tabContent).offset().top
    }, 500);
	});
	//post slider
	/*$('.post-slide-images .flexslider').flexslider({
    animation: "slide",
    controlNav: "thumbnails",
    slideshow: false,
    slideshowSpeed: 3000,
    pausePlay: true,
    pauseText: 'Pause',
    playText: 'Play',
    start: function(slider) {
    	$('.post-slide-images .flex-pauseplay .flex-pause').trigger('click');
    	$('.post-slide-images .flex-pauseplay').prepend('<a href="#" class="control-prev"><i class="fas fa-step-backward"></i>Prev</a>');
			var controlWrap = $('<div class="control-wrap"></div>');
			$('.post-slide-images .flexslider').append(controlWrap);
			$('.post-slide-images .flex-control-thumbs').appendTo(controlWrap);
			$('.post-slide-images .flex-pauseplay').appendTo(controlWrap);
    	$('.post-slide-images .flex-control-thumbs').find('li').each(function(e){
    		var _this = $(this);
    		var img = $(_this).children('img');
    		var src = $(img).attr('src');
    		console.log($(_this).index());
    		$(_this).attr('style', 'background-image: url("'+ src +'");');
    	});
			
    },
  });*/
  function getWidthCarousel(carousel){
  	//var width = $('.post-slide-images #carousel').width() / 5;
    var width = ($(carousel).width() - 20 ) / 5;
  	return width;
  }
  if($('.post-slide-images').length > 0){
    $('.post-slide-images').each(function( e ) {
      var _this = $(this);
      var sliderId = $(this).data('slider-id');
      $('#carousel-'+sliderId).flexslider({
        animation: "slide",
        controlNav: false,
        slideshow: false,
        itemMargin: 5,
        itemWidth: getWidthCarousel('#carousel-'+sliderId),
        asNavFor: '#slider-'+sliderId
      });

      $('#slider-'+sliderId).flexslider({
        animation: "slide",
        /*pausePlay: true,
        pauseText: 'Pause',
        playText: 'Play',*/
        controlNav: false,
        slideshow: true,
        slideshowSpeed: 3000,
        sync: "#carousel-"+sliderId,
        start: function(slider) {
        	$('.post-slide-images #slider-'+sliderId+' .flex-pauseplay .flex-pause').trigger('click');
        }
      });
    });
  }

  $('.post-slide-images .wrap-control .prev-slide').click( function(e){
  	e.preventDefault();
    var postSlider = $(this).parents('.post-slide-images');
  	$(postSlider).find('.slider .flex-prev').trigger('click');
  });

  $('.post-slide-images .wrap-control .pauseplay-slide').click( function(e){
  	e.preventDefault();
    var postSlider = $(this).parents('.post-slide-images');
  	$(this).find('i').toggleClass('fa-play').toggleClass('fa-pause');
  	$(postSlider).find('.slider .flex-pauseplay a').trigger('click');
  });
  // main slider
  $('.post-main-flexslider').flexslider({
    animation: "fade",
    slideshow: false,
    controlNav: "thumbnails",
    startAt: ($('#slide-focus').index() > 0) ? $('#slide-focus').index() : 0,
    start: function(slider){
      $(slider).find('.slides').wrapAll('<div class="wrap-slides"></div>');
      $(slider).find('.flex-direction-nav').appendTo('.wrap-slides');
    }
  });
  //clickToCopy
  function clickToCopy($input) {
    $input.select();
    document.execCommand("copy");
  }
  $('body').delegate('.btn-copy-text', 'click', function(e){
    e.preventDefault();
    var inputId = $(this).data('input');
    var input = $('#'+inputId);
    clickToCopy(input);
  });
  //post vote
  $('body').delegate('#form-vote', 'change', function(e){
    $(this).trigger('submit');
  });
  $('body').delegate('#form-vote', 'submit', function(e){
    e.preventDefault();
    var _this = $(this);
    var voteSection = $(this).parents('.vote-section');
    var radioId = $(this).find('input[name="vote_opt"]:checked').attr('id');
    $(this).find('.message').hide();
    $(this).addClass('submitting');
    var action = $(this).attr('action');
    var postData = $(this).serializeArray();
    $.ajax({
      url: action,
      method: "POST",
      data: postData,
      dataType: 'JSON',
      success: function(data)
      {
        if(data.status){
          $(voteSection).html(data.html);
          if(data.isVote){
            var input = $(voteSection).find('#'+radioId);
            $(input).parents('.vote-select').siblings().each(function(e){
              $(this).find('input').prop('disabled', true);
              $(this).find('label').addClass('disabled');
            });
            $(input).prop("checked", true).addClass('checked');
            $(voteSection).find('.message').show();
          }
        }
      },
      error: function(data){
        alert('Error');
        $(_this).removeClass('submitting');
      }
    });
  });
  $('body').delegate('#form-vote .vote-select input', 'click', function(e){
    e.preventDefault();
    var input = $(this);
    var _thisForm = $(this).parents('#form-vote');
    if($(input).hasClass('checked')) {
      var voteOpt = $(input).val();
      $(_thisForm).find('.un-vote').val(voteOpt);
      $(input).prop('checked', false);
    }
    $(_thisForm).trigger('submit');
  });

  //single load more post
  var lastScrollTop = 0;
  $(window).on('scroll', function () {
    var st = $(document).scrollTop();
    if (st > lastScrollTop){
      var winHeight = $(window).height(),
        docHeight = $(document).height(),
        docScroll = $(document).scrollTop() + winHeight,
        docPer = docHeight * 80 / 100;
      if ($('#form-load-more').length !== 0 && docScroll >= docPer) {
        var loading = $('body').data('ajax');
        if (!loading) {
          $('#form-load-more').trigger('submit');
          
        }
      }
    }
    lastScrollTop = st;
  });
  $('#form-load-more').submit(function(e){
    e.preventDefault();
    var _thisForm = $(this);
    var action = $(this).attr('action');
    var postData = $(this).serializeArray();
    $.ajax({
      url: action,
      method: "POST",
      data: postData,
      dataType: 'JSON',
      beforeSend: function () {
        $('body').data('ajax', 1);
      },
      error: function () {
        $('body').data('ajax', 0);
      },
      success: function (data) {
        if(data.status){
          $('.main-article-details-page-content').append(data.html);
          $(_thisForm).append('<input type="hidden" name="list_post_id[]" value="'+ data.post_id +'" />');
          if($('#article-content-' + data.post_id).height() > MAX_HEIGHT_CONTENT_TAB){
            setTimeout(function(){
              //splitContent($('#article-content-' + data.post_id));
            }, 2000);
          } 
          //splitContent($('#article-content-' + data.post_id));
        }
        $('body').data('ajax', 0);
      }
    });
  });
});