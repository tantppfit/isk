$(function(){
	$('.instagram-posts-scrolls').infiniteslide({
		speed: 50,
		pauseonhover: false
	});
});

$(document).ready(function() {

	$('#page-top').click(function() {
		$('html, body').animate({scrollTop: 0}, 500);
	});

	$(document).on('click', 'a[href^="#"]', function (event) {
		var bodyHeight = $('body').height();
		var target = $(this.getAttribute('href'));
		console.log(bodyHeight);

		if (target.length) {
			event.preventDefault();
			$('html, body').stop().animate({
				scrollTop: target.offset().top - bodyHeight
			}, 800);
		}
	});
});

function changeHambugerBar() {
	document.getElementById("menu-bar").classList.toggle("change")
}