<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| Api Routes
|--------------------------------------------------------------------------
|
| Here is where you can register Api routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "Api" middleware group. Enjoy building your Api!
|
*/

Route::middleware('auth:Api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::post('login', 'Api\UserController@login');
Route::post('logout', 'Api\UserController@logout');
Route::post('chatfirebase', 'Api\ChatFirebaseController@chatfirebase');
Route::post('filefirebase', 'Api\FileFirebaseController@filefirebase');
Route::post('imagefirebase', 'Api\ImageFirebaseController@imagefirebase');
Route::post('register', 'Api\UserController@register');
Route::group(['middleware' => 'auth:Api'], function(){
Route::post('details', 'Api\UserController@details');
});