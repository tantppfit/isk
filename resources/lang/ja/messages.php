<?php

return [
	'success' => [
		'create' => '「:Module」 を更新しました。',
		'update' => '「:Module」 編集しました。',
		'delete' => '「:Module」 を削除しました。',
		'added_previewer_admin' => '「管理者」を追加しました。',
		'added_previewer_user' => '「記事作成者」を追加しました。'
	],
	'error' => [
		'create' => '「:Module」 更新できません。',
		'update' => '「:Module」 編集できません。',
		'delete' => '「:Module」 削除できません。',
		'old_password' => 'パスワードが間違いました。',
		'email_does_not_exist' => 'メールアドレスは存在しません。',
		'email_exist' => '電子メールアドレスはすでに存在しています',
	]
];