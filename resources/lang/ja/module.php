<?php
return [
	'entry' => [
		'filemanage' => 'file manage',
		'post' => '投稿',
		'tag' => 'tag',
		'category' => 'category',
		'menu_tag' => 'menu tag',
		'menu' => 'menu',
		'user' => 'ユーザー',
		'inquiry' => 'inquiry',
		'support_email' => 'support email',
		'media' => 'メディア',
		'setting' => 'setting',
		'page' => 'page',
		'vote' => 'vote',
	],
	'entries' => [
		'post' => 'posts',
		'filemanage' => 'ファイル',
		'tag' => 'tags',
		'category' => 'categories',
		'menu_tag' => 'menu tags',
		'menu' => 'menu',
		'user' => 'users',
		'inquiry' => 'inquiries',
		'support_email' => 'support emails',
		'media' => 'medias',
		'setting' => 'settings',
		'page' => 'pages',
		'vote' => 'votes',
	]
];