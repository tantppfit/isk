@extends('admin.layouts.app')

@section('content')
    @include('admin.parts.left-menu')

<div class="admin-edit">
    <section class="main">
        <div class="content-table-list" style="height: calc(100vh - 150px);"> 
            <div class="floatLeft" style="height: 100%; overflow: auto;">
                <table>
                    <thead>
                    <tr style="height: 30px;"> 
                            <!-- <form action="{{ route('admin.chatmanage.index') }}" method="GET" autocomplete="off"> -->
                                <th style="text-align: left;color: red;width: 20%">
                                    <div class="search-page-input">
                                        <input type="text" class="search-input-search-page fas fa-search" placeholder="" name="search" id="search-user" style="height: 35px;width:65%;border-radius:5px;" data-action="{{route('admin.chatmanage.searchdetail')}}">
                                        <!-- <button class="btn" name="search-user"><i class="fas fa-search" aria-hidden="true" style="margin-left: 5%;font-size:20px"></i></button> -->
                                    </div>
                                </th>
                            <!-- </form> -->
                        </tr>         
                    </thead>
                    <tbody id="user-table">
                    @if(count($searchresult))
                    @foreach($searchresult as $user)    
                    @if($user->id != '10001')                
                        @if ($user->id == $id)
                            <tr style="height: 30px;background-color:#B0C4DE"> 
                                <td><a class="user-chat" href="{{ route('admin.chatmanage.chatfirebase',$user->id) }}">
                                    <div class="float-left"><img id="changeimg" src="/storage/avatar/default1.png" style="border-radius:25px;height: auto;width: 45px; object-fit: cover;"></div>
                                    <div class="float-left text-left" style="margin-left:3%;">{{ $user->name }}
                                        <p style="color: black;">{{ substr($user->updated_at, 0, -9)}}</p>
                                    </div>
                                    <!-- <input style="float: right;margin-top: 8%;transform: scale(2);" class="file-checkbox" type="checkbox" data-file-id="{{ $user->id }}" name=""> --></a>
                                </td> 
                            </tr>
                        @else
                            <tr style="height: 30px;"> 
                                <td><a class="user-chat" href="{{ route('admin.chatmanage.chatfirebase',$user->id) }}">
                                    <div class="float-left"><img id="changeimg" src="/storage/avatar/default1.png" style="border-radius:25px;height: auto;width: 45px; object-fit: cover;"></div>
                                    <div class="float-left text-left" style="margin-left:3%;">{{ $user->name }}
                                        <p style="color: black;">{{ substr($user->updated_at, 0, -9)}}</p>
                                    </div>
                                    <!-- <input style="float: right;margin-top: 8%;transform: scale(2);" class="file-checkbox" type="checkbox" data-file-id="{{ $user->id }}" name=""> --></a>
                                </td> 
                            </tr>
                        @endif
                    @endif
                    @endforeach
                    @else
                    @foreach($users as $user)
                    @if($user->id != '10001')
                        @if ($user->id == $id)
                            <tr style="height: 30px;background-color:#B0C4DE"> 
                                <td><a class="user-chat" href="{{ route('admin.chatmanage.chatfirebase',$user->id) }}">
                                    <div class="float-left"><img id="changeimg" src="/storage/avatar/default1.png" style="border-radius:25px;height: auto;width: 45px; object-fit: cover;"></div>
                                    <div class="float-left text-left" style="margin-left:3%;">
                                        {{ $user->name }}
                                        <p style="color: black;">{{ substr($user->updated_at, 0, -9)}}</p>
                                    </div>
                                    <!-- <input style="float: right;margin-top: 8%;transform: scale(2);" class="file-checkbox" type="checkbox" data-file-id="{{ $user->id }}" name=""> --></a>
                                </td> 
                            </tr>
                        @else
                            <tr style="height: 30px;"> 
                                <td><a class="user-chat" href="{{ route('admin.chatmanage.chatfirebase',$user->id) }}">
                                    <div class="float-left"><img id="changeimg" src="/storage/avatar/default1.png" style="border-radius:25px;height: auto;width: 45px; object-fit: cover;"></div>
                                    <div class="float-left text-left" style="margin-left:3%;">
                                        {{ $user->name }}
                                        <p style="color: black;">{{ substr($user->updated_at, 0, -9)}}</p>
                                    </div>
                                    <!-- <input style="float: right;margin-top: 8%;transform: scale(2);" class="file-checkbox" type="checkbox" data-file-id="{{ $user->id }}" name=""> --></a>
                                </td> 
                            </tr>
                        @endif
                    @endif
                    @endforeach
                    @endif 
                    @if(count($genres))
                        @foreach($genres as $genre)
                        <tr style="height: 30px;"> 
                            <td><a class="user-chat" style="color: #007bff;">
                                <div class="float-left"><img id="changeimg" src="/storage/avatar/default2.png" style="border-radius:25px;height: auto;width: 45px; object-fit: cover;"></div>
                                <div class="float-left text-left" style="margin-left:3%;">
                                    {{ $genre->name }}
                                    <p style="color: black;">{{ substr($genre->updated_at, 0, -9)}}</p>
                                </div>                              
                                <!-- <input style="float: right;margin-top: 8%;transform: scale(2);" class="file-checkbox" type="checkbox" data-file-id="{{ $genre->id }}" name=""> --></a>
                            </td>
                        </tr>
                        @endforeach
                    @endif
                    @if(count($groups))
                    @foreach($groups as $group)
                    <tr style="height: 30px;"> 
                        <td><a class="user-chat" style="color: #007bff;">
                            <div class="float-left"><img id="changeimg" src="/storage/avatar/default2.png" style="border-radius:25px;height: auto;width: 45px; object-fit: cover;"></div>
                            <div class="float-left text-left" style="margin-left:3%;">
                                {{ $group->name }}
                                <p style="color: black;">{{ substr($group->updated_at, 0, -9)}}</p>
                            </div>
                            <!-- <input style="float: right;margin-top: 8%;transform: scale(2);" class="file-checkbox" type="checkbox" data-file-id="{{ $group->listid }}" name=""> --></a>
                        </td>
                    </tr>
                    @endforeach
                    @endif
                    </tbody>
                    <tr>
                    </tr>
                </table>
            </div>

            <div class="floatRight">
                <table>
                    <tr>
                        <th>
                            <div id="demo" style="position: relative; height: calc(100vh - 210px); display: block; overflow-y:auto;">
                                <table>
                                @if($messages)
                                @foreach($messages as $message)
                                    <tr>
                                    @if($message['user']['_id'] == $id)
                                        <td style="background-color: #fff;text-align: left;">
                                            <img id="changeimg" src="/storage/avatar/default.png" style="border-radius:25px;height: auto;width: 45px; object-fit: cover;">
                                            {{$message['user']['name']}}<br>

                                            @if(!empty($message['text']) && $message['text'] != 'a')
                                                {{ $message['text'] }}
                                                <p style="margin-left: 0%;">
                                                {{ $message['created_at'] }}</p>
                                            @elseif(!empty($message['image']))
                                                <img id="changeimg" src="{{ $message['image']}}" style="height: auto;width: 20%; object-fit: cover;">
                                                <p style="margin-left: 0%;">
                                                {{ $message['created_at'] }}</p>
                                            @elseif(!empty($message['file_url']))
                                                @if($files)
                                                @foreach($files as $file)
                                                    @if (basename($file['original_path']) == basename($message['file_url']))
                                                    <!-- <div style="cursor: pointer;" ondblclick="location.href='{{ route('admin.filebrowsing.index',$file) }}'"> -->
                                                        <div style="cursor: pointer;">
                    @php ($format = $file -> name)
                    @php ($format = strrchr( $format, '.'))
                    @if ($format == '.xlsx' || $format == '.xls')   
                        <img src="/image/icon/excel.png" style="height: auto;width: 8%; object-fit: cover;"/>
                    @elseif ($format == '.docx' || $format == '.doc')
                        <img src="/image/icon/word.png" style="height: auto;width: 8%; object-fit: cover;"/>
                    @elseif ($format == '.pptx' || $format == '.ppt')
                        <img src="/image/icon/powerpoint.png" style="height: auto;width: 8%; object-fit: cover;"/>
                    @elseif ($format == '.pdf')
                        <img src="/image/icon/pdf.png" style="height: auto;width: 8%; object-fit: cover;"/>
                    @elseif ($format == '.csv')
                        <img src="/image/icon/csv.png" style="height: auto;width: 8%; object-fit: cover;"/>
                    @elseif ($format == '.txt')
                        <img src="/image/icon/txt.png" style="height: auto;width: 8%; object-fit: cover;"/>
                    @elseif ($format == '.sql')
                        <img src="/image/icon/sql.png" style="height: auto;width: 8%; object-fit: cover;"/>
                    @else
                        <img src="/image/icon/filenormal.png" style="height: auto;width: 8%; object-fit: cover;"/>
                    @endif
                                                            <br>
                                                            {{ $file -> name}}
                                                        </div>
                                                        <p style="margin-left: 0%;">
                                                        {{ $message['created_at'] }}</p>
                                                    @endif
                                                @endforeach
                                                @endif
                                            @endif  
                                        </td>
                                        @else
                                        <td style="background-color: #fff;text-align: right;">
                                           
                                            <img id="changeimg" src="/storage/avatar/default.png" style="border-radius:25px;height: auto;width: 45px; object-fit: cover;">
                                            {{$message['user']['name']}}<br>

                                            @if(!empty($message['text']) && $message['text'] != 'a')
                                                {{ $message['text'] }}
                                                <p style="margin-right: 0%;">
                                                {{ $message['created_at'] }}</p>
                                            @elseif(!empty($message['image']))
                                                <img id="changeimg" src="{{ $message['image']}}" style="height: auto;width: 20%; object-fit: cover;">
                                                <p style="margin-right: 0%;">
                                                {{ $message['created_at'] }}</p>
                                            @elseif(!empty($message['file_url']))
                                                @if($files)
                                                @foreach($files as $file)
                                                    @if (basename($file['original_path']) == basename($message['file_url']))
                                                    <!-- <div style="cursor: pointer;" ondblclick="location.href='{{ route('admin.filebrowsing.index',$file) }}'"> -->
                                                        <div style="cursor: pointer;">
                    @php ($format = $file -> name)
                    @php ($format = strrchr( $format, '.'))
                    @if ($format == '.xlsx' || $format == '.xls')   
                        <img src="/image/icon/excel.png" style="height: auto;width: 8%; object-fit: cover;"/>
                    @elseif ($format == '.docx' || $format == '.doc')
                        <img src="/image/icon/word.png" style="height: auto;width: 8%; object-fit: cover;"/>
                    @elseif ($format == '.pptx' || $format == '.ppt')
                        <img src="/image/icon/powerpoint.png" style="height: auto;width: 8%; object-fit: cover;"/>
                    @elseif ($format == '.pdf')
                        <img src="/image/icon/pdf.png" style="height: auto;width: 8%; object-fit: cover;"/>
                    @elseif ($format == '.csv')
                        <img src="/image/icon/csv.png" style="height: auto;width: 8%; object-fit: cover;"/>
                    @elseif ($format == '.txt')
                        <img src="/image/icon/txt.png" style="height: auto;width: 8%; object-fit: cover;"/>
                    @elseif ($format == '.sql')
                        <img src="/image/icon/sql.png" style="height: auto;width: 8%; object-fit: cover;"/>
                    @else
                        <img src="/image/icon/filenormal.png" style="height: auto;width: 8%; object-fit: cover;"/>
                    @endif
                                                            <br>
                                                            {{ $file -> name}}
                                                        </div>
                                                        <p style="margin-right: 0%;">
                                                        {{ $message['created_at'] }}</p>
                                                    @endif
                                                @endforeach
                                                @endif          
                                            @endif  
                                        </td>
                                        @endif  
                                    </tr>
                                @endforeach
                                @endif  
                                </table>                                       
                            </div>
                        </th> 
                    </tr> 
                </table>
            </div>
            <div style="display: flex; width: 100%; flex-flow: wrap; justify-content: space-between; margin-top: 15px;bottom: 15px; position: absolute;">
                <div style="float: left;">
                    <form action="{{ route('admin.chatmanage.index') }}" method="GET" autocomplete="off">
                        <button class="btn" style="width:100px;background:#F2F2F2 ;color: #D34725;margin: 30px 0 0 15px;border:1px solid #D34725">戻る
                        </button>
                    </form>    
                </div>
                <div class="w-75">
                    <form action="{{ route('admin.chatmanage.chatfb',$id) }}" method="POST" autocomplete="off" enctype="multipart/form-data" style="width: 100%;">
                        <input type="hidden" name="post_ids" value="" />
                        <input type="hidden" name="redirect" value="{{ url()->full() }}" />
                        <div style="text-align: center;">
                            <output style="padding-left:17%;" id="list" class="row"></output>
                            <input type="file" hidden style="margin-right: 39%;" id="files-img" name="file" value="" multiple="multiple" accept="*"/><br>
                            <label for="files-img">
                                <img style="font-size:35px;margin-left:10%;" src="/image/icon/file-alt-regular-1.png" width="35px" height="46px"/>
                            </label>
                            <input type="hidden" id="userid" name="userid" value="{{$user->id}}" />   
                            <input type="text" class="form-control" style="display:inline-block; height: 45px; width:60%;margin-left:2%;" id="inputtext" value="" class="input-text" name="text" accept="image/x-png,image/jpeg" multiple="multiple"/>
                            <button type="submit" class="btn" id="form-chat-text" style="background: #F2F2F2; width:100px; color:#D34725;height:45px;border-radius:6px;font-size: 14px;border: 1px solid #D34725;">送信
                            </button>
                            {{csrf_field()}}
                        </div>
                    </form>
                </div>
            </div>
        </div>         
    </section>
</div>
<script type="text/javascript" language="Javascript">
// var myDiv = document.getElementById("demo");
//     myDiv.scrollTop = myDiv.scrollHeight - myDiv.clientHeight;
window.onload=function () {
     var objDiv = document.getElementById("demo");
     objDiv.scrollTop = objDiv.scrollHeight;
}
</script>
@endsection