@extends('admin.layouts.app')
@section('content')
@include('admin.parts.left-menu')

<div class="admin-edit">
	<section class="main">
		<div class="content-table-list" style="height: calc(100vh - 150px);">
			
			<div class="floatLeft" style="height: 100%; overflow: auto;">
				<table>
					<thead>
						<tr style="height: 30px;"> 
							<!-- <form action="{{ route('admin.chatmanage.index') }}" method="GET" autocomplete="off"> -->
								<th style="text-align: left;color: red;width: 20%">
									<div class="search-page-input">
										<input type="text" class="search-input-search-page fas fa-search" placeholder="" name="search" id="search-user" style="height: 35px;width:65%;border-radius:5px;" data-action="{{route('admin.chatmanage.search')}}">
										<!-- <button class="btn" name="search-user"><i class="fas fa-search" aria-hidden="true" style="margin-left: 5%;font-size:20px"></i></button> -->
									</div>
								</th>
							<!-- </form> -->
						</tr>
					</thead>
					<tbody id="user-table">
						@if(count($searchresult))
						@foreach($searchresult as $user)
						@if($user->id != '10001')
						<tr style="height: 30px;"> 
							<td><a class="user-chat" href="{{ route('admin.chatmanage.chatfirebase',$user->id) }}">
								<div class="float-left"><img id="changeimg" src="/storage/avatar/default1.png" style="border-radius:25px;height: auto;width: 45px; object-fit: cover;"></div>
								<div class="float-left text-left" style="margin-left:3%;">
									{{ $user->name }}
									<p style="color: black;">{{ substr($user->updated_at, 0, -9)}}</p>
								</div>
								<input style="float: right;margin: 6% 3%;transform: scale(2);" class="file-checkbox" type="checkbox" data-file-id="{{ $user->id }}" name=""></a>
							</td>
						</tr>
						@endif
						@endforeach
						@endif
						@if(count($genres))
						@foreach($genres as $genre)
						<tr style="height: 30px;"> 
							<td><a class="user-chat" style="color: #007bff;">
								<div class="float-left"><img id="changeimg" src="/storage/avatar/default2.png" style="border-radius:25px;height: auto;width: 45px; object-fit: cover;"></div>
								<div class="float-left text-left" style="margin-left:3%;">
									{{ $genre->name }}
									<p style="color: black;">{{ substr($genre->updated_at, 0, -9)}}</p>
								</div>								
								<input style="float: right;margin: 6% 3%;transform: scale(2);" class="file-checkbox" type="checkbox" data-file-id="{{ $genre->id }}" name=""></a>
							</td>
						</tr>
						@endforeach
						@endif
						@if(count($groups))
						@foreach($groups as $group)
						<tr style="height: 30px;"> 
							<td><a class="user-chat" style="color: #007bff;">
								<div class="float-left"><img id="changeimg" src="/storage/avatar/default2.png" style="border-radius:25px;height: auto;width: 45px; object-fit: cover;"></div>
								<div class="float-left text-left" style="margin-left:3%;">
									{{ $group->name }}
									<p style="color: black;">{{ substr($group->updated_at, 0, -9)}}</p>
								</div>
								<input style="float: right;margin: 6% 3%;transform: scale(2);" class="file-checkbox" type="checkbox" data-file-id="{{ $group->listid }}" name=""></a>
							</td>
						</tr>
						@endforeach
						@endif
					</tbody>
					<tr>
					</tr>
				</table>
			</div>

			<div class="floatRight">
			</div>
		</div>
		<div style="display: flex; width: 100%; flex-flow: wrap; justify-content: space-between; margin-top: 15px; position: absolute;
    bottom: 15px;">
			<div style="float: left;">
				<form action="{{ route('admin.user.index') }}" method="GET" autocomplete="off">
					<button class="btn" style="width:100px;background:#F2F2F2 ;color: #D34725;margin: 30px 15px 0 15px; border:1px solid #D34725;">戻る
					</button>
				</form>  
			</div>
			<div class="w-75">
				<form action="{{ route('admin.chatmanage.chatfbmulti') }}" method="POST" autocomplete="off" enctype="multipart/form-data" style="width: 100%;">
						<input type="hidden" name="post_ids" value="" />
						<input type="hidden" name="redirect" value="{{ url()->full() }}" />
                        <div style="text-align: center;">
                            <output style="padding-left:17%;" id="list" class="row"></output>
                            <input type="file" hidden style="margin-right: 39%;" id="files-img" name="file" value="" multiple="multiple" accept="*"/><br>
                            <label for="files-img">
                                <img style="font-size:35px;margin-left:10%;" src="/image/icon/file-alt-regular-1.png" width="35px" height="46px"/>
                            </label>
                            <input type="hidden" id="userid" name="userid" value="{{$user->id}}" />   
                            <input type="text" class="form-control" style="display:inline-block; height: 45px; width:60%;margin-left:2%;" id="inputtext" value="" class="input-text" name="text" accept="image/x-png,image/jpeg" multiple="multiple"/>
                            <button type="submit" class="btn" id="form-chat-text" style="background: #F2F2F2; width:100px; color:#D34725;height:45px;border-radius:6px;font-size: 14px;border: 1px solid #D34725;">送信
                            </button>
                            {{csrf_field()}}
                        </div>
                    </form>
			</div>    
		</div>   
	</section>
</div>
<!-- <script type="text/javascript" language="Javascript">
           
</script> -->
@endsection
