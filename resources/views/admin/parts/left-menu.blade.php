<!---------- admin-edit side-bar ---------->
<!-- <nav class="side-bar navbar collapse navbar-expand-md" id="sidebar-main">
	<ul class="side-bar-menu">
		<li class="side-bar-items d-none">
			<a href="#" class="nav-link navbar-toggler navbar-hide-menu" data-toggle="collapse" data-target="#sidebar-main"
			aria-controls="sidebar-main" aria-expanded="false" aria-label="Toggle navigation">
				<span style="visibility: hidden;">hide menu</span>
				<span style="position: absolute; right: 0;"><i class="fas fa-times"></i></span>
			</a>
		</li>

		<li class="side-bar-items avatar-admin text-center {{ request()->is('admin/user/edit/'.Auth::user()->id) ? 'active' : ''}}">
			<a href="{{ route('admin.user.edit', Auth::user()->id) }}" class="side-bar-links">
				<div class="avatar-admin-img">
					<img src="{{ !empty(Auth::user()->avatar) ? Helper::getImageUrl(Auth::user()->avatar) : asset('image/no-img.png') }}">
				</div>
				<p>{{ Auth::user()->email }}</p>
				<p>{{ __('自分の信報を編集する') }}</p>
			</a>
		</li>

		<div class="editer-menu-screen">
			@can('create',App\Models\Post::class)
			<a href="{{ route('admin.post.create') }}" class="side-bar-links">
				<li class="side-bar-items {{ request()->is('admin/post/edit/*') ? 'active' : '' }}">
					<i class="fas fa-pencil-alt"></i>
					{{ __('投稿する') }}
				</li>
			</a>
			@endcan
			@can('indexBySite',App\Models\Post::class)
			<a href="{{ route('admin.post.indexBySite') }}" class="side-bar-links">
				<li class="side-bar-items {{ request()->is('admin/site/post*') ? 'active' : ''}}">
					<i class="far fa-copy"></i>
					{{ __('コンテンツー覧') }}
				</li>
			</a>
			@endcan
			@can('edit', Auth::user()->site)
			<a href="{{ route('admin.site.edit', Auth::user()->site_id) }}" class="side-bar-links">
				<li class="side-bar-items {{ request()->is('admin/site/edit*') ? 'active' : ''}}">
					<i class="fas fa-cogs"></i>
					{{ __('サイト設定') }}
				</li>
			</a>
			@endcan
			@can('index',App\Models\User::class)
			<a href="{{ route('admin.user.index') }}" class="side-bar-links">
				<li class="side-bar-items {{ request()->is('admin/user*') && !request()->is('admin/user/edit/'.Auth::user()->id) ? 'active' : ''}}">
					<i class="fas fa-user"></i>
					{{ __('管理者設定') }}
				</li>
			</a>
			@endcan
			@can('index',App\Models\Category::class)
			<a href="{{ route('admin.category.index') }}" class="side-bar-links">
				<li class="side-bar-items {{ request()->is('admin/category*') ? 'active' : ''}}">
					<i class="fas fa-th"></i>
					{{ __('カテゴリー管理') }}
				</li>
			</a>
			@endcan
			@can('index',App\Models\Tag::class)
			<a href="{{ route('admin.tag.index') }}" class="side-bar-links">
				<li class="side-bar-items {{ request()->is('admin/tag*') ? 'active' : ''}}">
					<i class="fas fa-tag"></i>
					{{ __('タグ管理') }}
				</li>
			</a>
			@endcan
			<a href="{{ route('admin.statistic.index') }}" class="side-bar-links">
				<li class="side-bar-items {{ request()->is('admin/statistic*') ? 'active' : ''}}">
					<i class="fas fa-chart-bar"></i>
					{{ __('メディア貢献度') }}
				</li>
			</a>
			@can('index',App\Models\Media::class)
			<a href="{{ route('admin.media.index') }}" class="side-bar-links">
				<li class="side-bar-items {{ request()->is('admin/media*') ? 'active' : ''}}">
					<i class="far fa-image"></i>
					{{ __('メディアライブラリ') }}
				</li>
			</a>
			@endcan
		</div>

		@if(Auth::user()->role->slug == 'admin')
			<div class="media-manager-screen-menu">
				@can('index',App\Models\Setting::class)
				<a href="{{ route('admin.setting.index') }}" class="side-bar-links">
					<li class="side-bar-items {{ request()->is('admin/setting*') || request()->is('admin/page*') || request()->is('admin/vote*') ? 'active' : ''}}">
						<i class="fas fa-cogs"></i>
						{{ __('メディア設定') }}
					</li>
				</a>
				@endcan
				@can('index',App\Models\Post::class)
					{{--<a href="{{ route('admin.post.index') }}" class="side-bar-links">
						<li class="side-bar-items {{ request()->is('admin/post') ? 'active' : ''}}">
							<i class="far fa-copy"></i>
							{{ __('コンテンツ管理') }}
						</li>
					</a> --}}
				@endcan
				@can('index',App\Models\User::class)
					{{-- <a href="{{ route('admin.user.indexofficialwriter') }}" class="side-bar-links">
						<li class="side-bar-items {{ request()->is('admin/official-writer*') ? 'active' : ''}}">
							<i class="far fa-flag"></i>
							{{ __('公式ライター管理') }}
						</li>
					</a> --}}
				@endcan
				@can('index',App\Models\MenuTag::class)
				<a href="{{ route('admin.menu_tag.index') }}" class="side-bar-links">
					<li class="side-bar-items {{ request()->is('admin/menu-tag*') || request()->is('admin/menu') || request()->is('admin/menu/*') ? 'active' : ''}}">
						<i class="fas fa-tag"></i>
						{{ __('メニュータグ管理') }}
					</li>
				</a>
				@endcan
			</div>
		@endif
		<a href="#" class="side-bar-links" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
			<li class="side-bar-items">
				<i class="fas fa-sign-out-alt"></i>
				{{ __('ログアウト') }}
			</li>
		</a>
		<a href="{{ route('frontend.home.index') }}" target="_blank" class="side-bar-links">
			<li class="side-bar-items">
				<i class="fas fa-arrow-right"></i>
				{{ __('メディアを開く') }}
			</li>
		</a>
	</ul>
</nav> -->
<!-----x----- admin-edit side-bar -----x----->