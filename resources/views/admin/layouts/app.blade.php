<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<!-- CSRF Token -->
  <meta name="csrf-token" content="{{ csrf_token() }}">
	
	<title>{{ config('app.name', 'Laravel') }}</title>
	@if(isset($settings['general_favicon']) && !empty($settings['general_favicon']))
  	<link rel="icon" href="{{ Helper::getMediaUrlById($settings['general_favicon'], 'thumbnail') }}" />
  @endif

  <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
  <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/jquery-ui-timepicker-addon/1.6.3/jquery-ui-timepicker-addon.min.css">

	<!-- bootstrap -->
	
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
	@yield('header_css')
	<link rel="stylesheet" type="text/css" href="{{ asset('css/admin/style.css') }}">
	<link rel="stylesheet" type="text/css" href="{{ asset('css/admin/custom.css') }}">
</head>
<body>
	<!---------- header ---------->

	<header class="admin-header">
		<div class="float-left logo">
			<a href="{{ route('admin.manager.index') }}">
				<!-- <img src="{{ asset('image/logo-white.png') }}"> -->
				@if((new \Jenssegers\Agent\Agent())->isMobile() && isset($settings['general_logo_mobile']) && !empty($settings['general_logo_mobile']))
					<img src="{{ $settings['general_logo_mobile'] }}">
				@elseif(isset($settings['general_logo']) && !empty($settings['general_logo']))
					<img src="{{ $settings['general_logo'] }}">
				@else
					<img src="{{ asset('image/logo_admin2.png') }}">
				@endif
			</a>
		</div>
		<div class="float-right">
			<label class="login-name" style="position: relative; top: 5px; font-size: 12px;">ユーザーID：{{ Auth::user()->email }}</label>
			<label class="logout-txt"><a href="#" onclick="event.preventDefault(); document.getElementById('logout-form').submit();"><i style="margin: 0 1px 0 0;" class="fas fa-sign-out-alt"></i>ログアウト</a></label>

			<button class="navbar-toggler navbar-show-menu" type="button" id="admin-menu-btn" onclick="changeHambugerBar()" data-toggle="collapse" data-target="#sidebar-main"
				aria-controls="sidebar-main" aria-expanded="false" aria-label="Toggle navigation">
				<!-- <span><i class="fas fa-bars" style="color: #fff; font-size: 10px;"></i></span> -->
				<div id="bar1" class="bar"></div>
				<div id="bar2" class="bar"></div>
				<div id="bar3" class="bar"></div>
				<small style="color: #fff; display: block; font-size: 10px; margin: 5px 0 0 -5px;">menu</small>
			</button>

			<form id="logout-form" action="{{ route('logout') }}" method="POST" class="hidden" style="display: none;">
        @csrf
      </form>
		</div>
	</header>

	<!-----x----- header -----x----->

	<div class="admin-edit">
		@yield('content')
	</div>

	<!-- script, bootstrap, ... -->


	<script src="https://kit.fontawesome.com/3973fa3cec.js" crossorigin="anonymous"></script>
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>  

	<!-- <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script> -->
	<script src="https://code.jquery.com/jquery-3.4.1.min.js" integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo=" crossorigin="anonymous"></script>
	<script src="https://code.jquery.com/jquery-1.12.4.js"></script>
	<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-ui-timepicker-addon/1.6.3/jquery-ui-timepicker-addon.min.js"></script>

	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>

	<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
	<script async="" src="https://www.instagram.com/embed.js"></script>
	@yield('footer_js')
	<script src="{{ asset('js/admin/custom.js') }}"></script>

	<script>
		function changeHambugerBar() {
			document.getElementById("admin-menu-btn").classList.toggle("change")
		}
	</script>
</body>
</html>