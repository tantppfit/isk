@if(isset($slider))
	<div class="slider-item slider-{{ $slider->id }}" data-action-delete-slider="{{ route('admin.slider.destroy', $slider) }}" data-action-delete-image="{{ route('admin.slider.removeImage', $slider) }}">
		<div class="slider-images">
			@if(is_array(json_decode($slider->slide_images, true)))
			<?php //dd($slider->slide_images); ?>
				@foreach(json_decode($slider->slide_images, true) as $key => $slide_image)
					<div class="slider-image">
						<a href="#" class="remove" data-media-id="{{ $key }}"><i class="fa fa-times"></i></a>
						<div style="background-image: url('{{ $slide_image }}'); "></div>
					</div>
				@endforeach
			@endif
		</div>
		<div>
			<span>コード:</span>
			<input type="text" class="form-control" value="{{ '[slider id='.$slider->id.']' }}" readonly="">
		</div>
		<div>
			<label for="add-slide-files" data-slider-id="{{ $slider->id }}" class="btn btn-no-radius add-slide-files">画像を追加</label>
			<a href="#" class="delete-slider" data-action="" data-id="{{ $slider->id}}"><i class="fas fa-trash-alt"></i></a>
		</div>
	</div>
@endif