@extends('admin.layouts.app')

@section('content')
	@include('admin.parts.left-menu')
	<!---------- admin-edit main section ---------->
	<section class="main">
		<div class="post-edit d-flex" style="justify-content: space-between;">
			<div>
				<h3 class="font-weight-bold">投稿・編集</h3>
				<p class="font-weight-bold">記事の作成 ・ 編集をします。</p>
				@include('admin.parts.alert')
			</div>
			<div>
				<img src="{{ asset('image/icon/question-ic-01.png') }}" style="width: 25px; height: 25px; position: relative; top: 2rem;left: 12px">
			</div>
		</div>
		@include('admin.parts.alert')
		<form action="{{ route('admin.post.preupdate', $post) }}" method="post">
			{{ csrf_field() }}
			<div class="sub-title d-flex" style="margin-top: 55px;"><img src="{{ asset('image/ic-img-01.png') }}" style="width:27px;height:27px;color:#989898;margin-top:9px;">
				<h3 class="w-100"><input type="text" placeholder="ここをクリックしてコンテンツタイトルを入力" name="title" class="form-control" value="{{ $post->title }}"></h3>
			</div>
			<div class="d-flex">
				<i class="fas fa-pencil-alt" style="color: #989898;width:37px;margin-left:6px; float: left;"></i>
				<div style="float: right; width: calc(100% - 43px);">
					<textarea id="editor" name="content" style="width: 100%;">{!! $post->content !!}</textarea>
				</div>
			</div>
			<div style="border: .3px solid;color:#989898;margin-top:192px; margin-left: 40px;opacity:.3;width: 56%"></div>
			<div class="button d-flex" style="margin-top:33px;">
				<a href="{{ route('admin.post.edit', $post) }}" class="form-control btn-no-radius admin-login-btn" style="font-size: 12px; text-align: center;">キャンセル</a>
				<button class="form-control btn-no-radius admin-login-btn" style="margin-left:10px">決定</button>
			</div>
		</form>
		<div>
			<label style="opacity:.3; float:right;font-style:italic;">0 chars</label>
		</div>
	</section>
	<!-----x----- admin-edit main section -----x----->
	<style> 
	.ck-editor__editable { 
	    min-height: 400px; 
	} 
	</style>
@endsection
@section('footer_js')
	<!-- <script src="https://cdn.ckeditor.com/ckeditor5/15.0.0/inline/ckeditor.js"></script> -->
	<script src="https://cdn.ckeditor.com/ckeditor5/15.0.0/classic/ckeditor.js"></script>
	<script>
      var _post_id = '{{{ $post->id }}}';
      var _post_upload_image_action = '{{{ route("admin.media.ckeditorpostuploadimage", $post) }}}';
      var _token = '{{{ csrf_token() }}}';
  </script>
	<script src="{{ asset('js/admin/content-editor.js') }}"></script>
@endsection