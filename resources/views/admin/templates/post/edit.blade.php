@extends('admin.layouts.app')

@section('content')
	@include('admin.parts.left-menu')
	<!-- admin-edit main section -->
	<section class="main">
		<div class="post-edit">
			<h3>投稿・編集</h3>
			<p>記事の作成 ・ 編集をします。</p>
			@include('admin.parts.alert')
		</div>

		<div class="admin-post-news">
			<form class="post-news" action="{{ route('admin.post.update', $post) }}" method="post">
				{{ csrf_field() }}
				<!-- content edit -->

				<div class="form-group d-flex" style="background: #f8f8f8; margin-top:65px; padding: 1rem 0;">
					<div class="label-admin-edit col-lg-2 col-md-3 col-6">
						<label style="margin-right: 10px;">コンテンツタイプ</label>
					</div>
					<div class="col-lg-10 col-md-9 col-6">
						<label>通常コンテンツ</label>
					</div>
				</div>

				<div class="form-group row" style="margin-top: 2rem;">
					<div class="label-admin-edit col-lg-2 col-md-3 col-12">
						<label>URL</label>
					</div>
					<div class="col-md-9 col-12">
						<input type="text" class="form-control" name="" value="{{ ($post->slug) ? route('frontend.post.show', $post->slug) : '' }}" disabled="" style="font-size: 12px;">
					</div>
				</div>

				<div class="form-group row" style="margin-top: 2.5rem;">
					<div class="label-admin-edit col-lg-2 col-md-3 col-12">
						<label>パーマリンク</label>
					</div>
					<div class="col-md-9 col-12">
						<span>{{ route('frontend.home.index') }}/ </span><input type="text" class="form-control d-inline w-auto" name="slug" value="{{ old('slug', $post->slug) }}" style="font-size: 12px;" />
					</div>
				</div>

				<div class="form-group row" style="margin-top: 2.5rem;">
					<div class="label-admin-edit col-lg-2 col-md-3 col-12">
						<label>タイトル</label>
					</div>
					<div class="col-md-9 col-12">
						<textarea class="form-control" rows="3" placeholder="" name="title" style="font-size: 12px;">{{ old('title', $post->title) }}</textarea>
					</div>
				</div>

				<div class="form-group row" style="margin-top: 3rem; background: #f8f8f8; padding: 1rem 0;">
					<div class="label-admin-edit col-lg-2 col-md-3 col-12">
						<label>カバー画像</label>
					</div>
					<div class="col-md-9 col-12">
						<div>
							<select class="cover-image-style" name="cover_image_style">
								<option value="default">画像の横幅を表示幅いっぱいに合わせる(縦幅は表示枠に合わせてカット)</option>
								<option value="img-max-height" {{ ($post->cover_image_style && $post->cover_image_style == 'img-max-height') ? 'selected' : '' }}>画像の縦幅を表示幅いっぱいに合わせる(横幅は表示枠に合わせてカット)</option>
						  </select>
						  <div class="float-right">
				  			<label for="feature-file" style="cursor: pointer;">
						  		<img src="{{ asset('image/ic-img-01.png') }}" style="width:27px;height:27px;color:#989898;margin-top:9px;">
						  	</label>						  	
						  </div>
						</div>

						<div class="cover-img-radio-choice row" style="margin-top: 1rem; margin-left: .1rem;">
							@if(isset($post->medias) && count($post->medias))
								@foreach($post->medias as $media)
									@if($media->is_cover)
										<div class="cover-img-radio" style="display: grid; margin-right: 1rem !important;">
											<input id="cover-img-{{ $media->id }}" type="radio" name="cover_image" value="{{ $media->id }}" {{ $post->cover_image == $media->id ? 'checked' : '' }}>
											<label for="cover-img-{{ $media->id }}">
												<a href="#" class="delete-cover" data-action="{{ route('admin.media.destroy', $media) }}"><i class="fa fa-times"></i></a>
												<img src="{{ Helper::getMediaUrl($media, 'ratio4x3') }}" style="width: 200px; height: 150px;">
											</label>
										</div>
									@endif
								@endforeach
							@endif
						</div>

						<div>
							<div style="margin-top: 1rem;">
								<img src="{{ asset('image/icon/ic-text-a.png') }}" style="float: left; padding: 10px 10px 10px 0;">
								<input type="text" class="form-control" name="cover_via_text" value="{{ old('cover_via_text', $post->cover_via_text) }}" placeholder="引用元（省略可）" style="width: 25%; font-size: 12px;">
							</div>

							<div style="margin-top: 1rem;">
								<img src="{{ asset('image/icon/ic-url.png') }}" style="float: left; padding: 10px 10px 10px 0;">
								<input type="text" class="form-control" name="cover_via_href" value="{{ old('cover_via_href', $post->cover_via_href) }}" placeholder="引引用元リンクURL（省略可）" style="width: 45%; font-size: 12px;">
							</div>
						</div>
					</div>
				</div>

				<!-- x content edit x -->

				<!-- preview -->

				<div class="responsive-demo col-12">
					<label class="font-weight-bold">プレビュー</label>
				
					<div class="d-flex overflow-auto iframe-preview">
						
						<div style="width: 680px; height: 350px; margin-right: .5rem;">
							<iframe id="iframe-preview-desktop" style="width: 1280px !important; height: 675px !important; border: 0; transform: scale(0.517); transform-origin: 0 0;" src="{{ route('frontend.post.preview', $post) }}"></iframe>
						</div>

						<div style="width: 320px; height: 230px;">
							<iframe id="iframe-preview-mobile" style="width: 480px!important; height: 346px !important; border: 0; transform: scale(0.66); transform-origin: 0 0;" src="{{ route('frontend.post.preview', $post) }}"></iframe>
						</div>
						
					</div>

					<p style="color: #c43638;"><i class="far fa-eye" style="color: #c43638; width: 20px; margin-top: 2rem;"></i><a href="{{ route('frontend.post.preview', $post) }}" target="_blank" style="color: #c43638;">別ウィンドウでプレビュー</a></p>

					<p style="color: #c43638;"><i class="fas fa-caret-right" style="color: #c43638; width: 20px;"></i><a href="#wrap-preview" style="color: #c43638;" data-toggle="collapse" href="#wrap-preview" role="button" aria-expanded="false" aria-controls="wrap-preview">プレビューを他の人と共有</a></p>
					<div id="wrap-preview" class="collapse" style="background: #f8f8f8; padding: 2rem;">
						<div class="form-group">
				      <label for="invite-email" class="font-weight-bold">メールアドレス</label>
				      <input type="text" class="form-control" id="invite-email" placeholder="メールアドレス">
				      <p class="message"></p>
				    </div>
				    <button id="submit-email-preview" class="btn btn-no-radius" style="background: #111111; color: #ffffff; min-width: 100px;" data-action="{{ route('admin.post.addpreviewer', $post) }}">共有</button>
					</div>

					<!-- x preview x -->

					<a href="#wrap-editor" class="btn btn-no-radius mb-4" style="background: #111111; color: #ffffff;font-size: 12px; text-align: left; margin-top: 2rem;" data-toggle="collapse" href="#collapseExample" role="button" aria-expanded="false" aria-controls="wrap-editor"><i class="fas fa-pencil-alt" style="margin-right: 10px; color: #ffffff;"></i>タイトル・カバー画像・本文を編集</a>

					<div id="wrap-editor" class="post-content collapse">
						<div class="row mt-4 mb-4">
							<div style="float: right; width: 100%;">
								<textarea id="editor" name="content" style="width: 100%;">{!! old('content', $post->content) !!}</textarea>
							</div>
						</div>
					</div>

					<!-- 12/02/2020 -->
					<!-- <p style="color: #c43638; margin-top: .5rem;"><i class="fas fa-caret-right" style="color: #c43638; width: 20px;"></i>記事の外部配信に際する注意点</p> -->
					<!-- x 12/02/2020 x -->

					<!-- <div>
						<label class="font-weight-bold">スライド画像</label>
						<div class="slide-images" data-action="{{ route('admin.post.removeslideimage', $post) }}">
							@if(is_array(json_decode($post->slide_images, true)))
								@foreach(json_decode($post->slide_images, true) as $slide_image)
									<div class="slide-image">
										<a href="#" class="remove" data-path="{{ $slide_image }}"><i class="fa fa-times"></i></a>
										<img src="{{ Helper::getImageUrl($slide_image) }}" />
									</div>
								@endforeach
							@endif
						</div>
						<label for="slide-files" class="btn btn-no-radius" style="background: #111111; color: #ffffff; margin-top: 2rem;">その他の画像</label>
					</div> -->
					<div class="block-sliders">
						<label class="font-weight-bold">スライド画像</label>
						<div class="sliders">
							@if($post->sliders && count($post->sliders))
								@foreach($post->sliders as $slider)
									@include('admin.templates.post.parts.slider_item', ['slider' => $slider])
								@endforeach
							@endif
						</div>
						<label for="new-slide" class="btn btn-no-radius" style="background: #111111; color: #ffffff; margin-top: 2rem; font-size: 12px;">スライドを追加</label>
					</div>
				</div>

				<div class="form-group row" style="margin-top: 2rem; background: #f8f8f8; padding-top: 1rem; padding-bottom: 1rem;">
					<div class="label-admin-edit col-lg-2 col-md-3 col-12">
						<label>カテゴリー</label>
					</div>
					<div class="col-md-9 col-12">
						<div class="post-categories" style="max-height: 200px; overflow: auto;">
							@if(isset($parentCategories) && count($parentCategories))
								@foreach($parentCategories as $parentCategory)
									<div>
										<label class="category-items">
											<input type="checkbox" name="categories[]" value="{{ $parentCategory->id }}" {{ $post->categories->contains('id',$parentCategory->id) ? 'checked' : '' }}>
											<span> {{ $parentCategory->name }}</span>
										</label>
									</div>
									@if($parentCategory->children && count($parentCategory->children))
					  				@foreach($parentCategory->children as $categoryChild)
											<div>&nbsp;
												<label class="category-items">
													<input type="checkbox" name="categories[]" value="{{ $categoryChild->id }}" {{ $post->categories->contains('id',$categoryChild->id) ? 'checked' : '' }}>
													<span> -- {{ $categoryChild->name }}</span>
												</label>
											</div>
					  				@endforeach
					  			@endif
								@endforeach
							@endif
						</div>
					</div>
				</div>

				<div class="form-group row" style="margin-top: 2rem; background: #f8f8f8; padding-top: 1rem;">
					<div class="label-admin-edit col-lg-2 col-md-3 col-12">
						<label>タグ</label>
					</div>
					<div class="col-md-9 col-12">
						<div class="tags post-tags">
							<div class="tag-menu row">
								@if(count($post->tags))
									@foreach($post->tags as $tag)
										<div class="tag-items remove-tag" data-tag-id="{{ $tag->id }}">
											<input type="hidden" name="tags[]" value="{{ $tag->id }}">
											<div><i class="fas fa-minus"></i></div>
											<label>{{ $tag->name }}</label>
										</div>
									@endforeach
								@endif
							</div>
						</div>

						<div class="row" style="margin-top: 1rem; margin-left: 0;">
							<input type="text" class="form-control input-tags" placeholder="タグを入力" style="width: 300px; border: 1px solid #111111; color: #888888; font-size: 12px;">
							<button class="btn-add-tags" style="margin-left: 10px; padding: 0 .5rem;" data-action="{{ route('admin.post.addtags') }}"><i class="fas fa-plus" style="color: #111111; width: 25px;"></i>追加</button>
						</div>

						<div class="row" style="margin: 1rem 0; background: #ffffff; padding-top: 1rem; width: 70%;">
							<div>
								<i class="fas fa-info-circle" style="color: #888888; width: 15px; margin-left: 10px;"></i>
							</div>

							<div>
								<ul style="color: #888888;">
									<li>設定できるタグは10個までドラッグ</li>
									<li>ドラッグでタグ順を変更できます</li>
									<li>「_（アンダースコア）」は空白として表示されます</li>
									<li>カンマ区切りで複数のタグを同時に追加できます</li>
								</ul>
							</div>
						</div>

						<div class="tags not-post-tags dash-border-box">
							<p style="color: #cccccc; margin: 0 .5rem;">プリセットから追加</p>
							<div class="tag-menu row pl-2">
								@if(isset($tags) && count($tags))
									@foreach($tags as $tag)
										@if($post->tags->contains('id',$tag->id))
											<div class="tag-items add-tag selected" data-tag-id="{{ $tag->id }}" data-tag-name="{{ $tag->name }}">
												<div><i class="fas fa-plus"></i></div>
												<label>{{ $tag->name }}</label>
											</div>
										@else
											<div class="tag-items add-tag" data-tag-id="{{ $tag->id }}" data-tag-name="{{ $tag->name }}">
												<div><i class="fas fa-plus"></i></div>
												<label>{{ $tag->name }}</label>
											</div>
										@endif
									@endforeach
								@endif
							</div>
						</div>

						<p style="color: #c43638; margin-top: 1rem;"><i class="fas fa-caret-right" style="color: #c43638; width: 15px;"></i>非表示タグ</p>
					</div>
				</div>

				<div class="form-group row" style="margin-top: 2.5rem;">
					<div class="label-admin-edit col-lg-2 col-md-3 col-12">
						<label>アンケート</label>
					</div>
					
					<div class="col-md-9 col-12">
						<select style="font-size: 12px;" class="form-control" name="vote_id">
							<option value="">アンケートを選択</option>
							@if(isset($votes) && count($votes))
								@foreach($votes as $vote)
									<option value="{{ $vote->id }}" {{ ($vote->id == $post->vote_id) ? 'selected' : '' }}>{{ $vote->name }}</option>
								@endforeach
							@endif
						</select>
						
						@if($post->vote)
							@php
								$voteTotal = array_sum($voteCountData);
							@endphp
							@if(!empty($post->vote->opt_1))
								<div class="row mt-3">
									<label class="mr-2">オプション1:</label>
									<p>{{ $post->vote->opt_1 }} ({{ ($voteTotal > 0) ? number_format(($voteCountData['opt_1']/$voteTotal)*100, 2) : '0' }}%/{{ $voteCountData['opt_1'] }}人)</p>
								</div>
							@endif
							@if(!empty($post->vote->opt_2))
								<div class="row mt-3">
									<label class="mr-2">オプション2:</label>
									<p>{{ $post->vote->opt_2 }} ({{ ($voteTotal > 0) ? number_format(($voteCountData['opt_2']/$voteTotal)*100, 2) : '0' }}%/{{ $voteCountData['opt_2'] }}人)</p>
								</div>
							@endif
							@if(!empty($post->vote->opt_3))
								<div class="row mt-3">
									<label class="mr-2">オプション3:</label>
									<p>{{ $post->vote->opt_3 }} ({{ ($voteTotal > 0) ? number_format(($voteCountData['opt_3']/$voteTotal)*100, 2) : '0' }}%/{{ $voteCountData['opt_3'] }}人)</p>
								</div>
							@endif
							@if(!empty($post->vote->opt_4))
								<div class="row mt-3">
									<label class="mr-2">オプション4:</label>
									<p>{{ $post->vote->opt_4 }} ({{ ($voteTotal > 0) ? number_format(($voteCountData['opt_4']/$voteTotal)*100, 2) : '0' }}%/{{ $voteCountData['opt_4'] }}人)</p>
								</div>
							@endif
							@if(!empty($post->vote->opt_5))
								<div class="row mt-3">
									<label class="mr-2">オプション5:</label>
									<p>{{ $post->vote->opt_5 }} ({{ ($voteTotal > 0) ? number_format(($voteCountData['opt_5']/$voteTotal)*100, 2) : '0' }}%/{{ $voteCountData['opt_5'] }}人)</p>
								</div>
							@endif
						@endif

						<!-- <div class="row mt-3">
							<label class="mr-2">Time: </label>
							<input type="number" class="mr-2" id="date-vote">
							<label for="date-vote">Days</label>
						</div>

						<div class="mt-3">
							<input type="checkbox" id="user-check" name="vote_status" value="1" {{ $post->vote_status ? 'checked' : '' }}>
							<label class="font-weight-bold" for="user-check">User vote</label>
						</div> -->
					</div>
				</div>

				<div class="row mt-3">
					<div class="label-admin-edit col-lg-2 col-md-3 col-12">
						<label>表示期間</label>
					</div>
					<div class="col-md-9 col-12">
						<!-- <input type="text" class="mr-2 d-inline-block form-control mw-100 datepicker" id="date-vote" name="vote_expire_time" value="{{ ($post->vote_expire_time) ? date('m/d/Y', strtotime($post->vote_expire_time)) : '' }}" placeholder="年−月−日" style="width: 175px; font-size: 12px;"> -->
						<input type="text" class="mr-2 d-inline-block form-control mw-100 datepicker" id="date-vote" name="vote_expire_time" value="{{ ($post->vote_expire_time) ? date('Y/m/d', strtotime($post->vote_expire_time)) : '' }}" placeholder="年−月−日" style="width: 175px; font-size: 12px;">
					</div>
				</div>

				<div class="row mt-3">
					<div class="label-admin-edit col-lg-2 col-md-3 col-12">
					</div>
					<div class="col-md-9 col-12">
						<input type="checkbox" id="user-check" name="vote_status" value="1" {{ $post->vote_status ? 'checked' : '' }}>
						<label class="font-weight-bold" for="user-check">表示</label>
					</div>
				</div>

				<div class="form-group row" style="margin-top: 3.5rem;">
					<div class="label-admin-edit col-lg-2 col-md-3 col-12">
						<label>アルバム</label>
					</div>
					<div class="col-md-9 col-12">
						<label>本文エディタの<!-- <i class="fas fa-camera-retro" style="color: #111111; width: 15px; margin: 0 5px;"></i> --><img src="{{ asset('image/camera-ic-01.png') }}" style="width: 23px;">
						から、アルバム(画像集)を作成できます</label>

						<div class="col-12" style="margin-top: 1rem;">
							<div class="row">
								<i class="fas fa-info-circle" style="color: #888888; width: 15px; margin: 5px 10px;"></i>
								<p style="width: 65%">アルバムは、コンテンツとは独立したページ群として表示され、本文にはアルバムページへのリンクプロックが挿入されます</p>
							</div>
						</div>
					</div>
				</div>

				<div class="form-group row" style="margin-top: 3rem; background: #f8f8f8; padding: 1rem 0 2rem 0;">
					<div class="label-admin-edit col-lg-2 col-md-3 col-12">
						<label>公開日時</label>
					</div>
					<div class="col-md-9 col-12">
						<!-- <input type="text" class="form-control datetimepicker" name="created_at" value="{{ $post->created_at->format('m/d/Y H:i:s') }}" style="font-size: 12px;"> -->
						<!-- <input type="text" class="form-control datetimepicker" name="created_at" value="{{ $post->created_at->format('Y/m/d H:i:s') }}" style="font-size: 12px;"> -->
						<input type="text" class="form-control datetimepicker-nos" name="created_at" value="{{ $post->created_at->format('Y/m/d H:i') }}" style="font-size: 12px;">

						<div class="col-12" style="margin: 1rem 0; ">
							<div class="row" style="padding: 1rem 0; background: #ffffff; width: 70%;">
								<i class="fas fa-info-circle" style="color: #888888; width: 15px; margin: 5px 10px;"></i>
								<p style="width: 90%;">公開日時を未来に設定すると、予約投稿として扱われます。設定された時刻にな<br>るまで、<span style="color: #c43638;">メディア</span>には表示されません。</p>

								<p style="width: 90%; margin-left: 35px;">（※実際に表示が開始されるのは、公開日時として設定された時刻の数分後にな<br>
								ります）</p>
							</div>
						</div>
					</div>
				</div>

				<div class="form-group row" style="margin-top: 3.5rem;">
					<div class="label-admin-edit col-lg-2 col-md-3 col-12">
						<label>作成者</label>
					</div>
					<div class="col-md-9 col-12">
						<label>{{ $post->user->name }} (u{{ $post->user->id }})</label>
					</div>
				</div>

				<div class="form-group row" style="margin-top: 3.5rem; background: #f8f8f8; padding: 1rem 0;">
					<div class="label-admin-edit col-lg-2 col-md-3 col-12">
						<label>状態</label>
					</div>
					<div class="col-md-9 col-12 row">
						<div style="margin-right: 1rem;">
							@if(Auth::user()->role->slug == 'admin')
								<input type="radio" name="post_status" value="1" {{ $post->post_status ? 'checked' : '' }} >
							@else
								<input type="radio" name="post_status" value="1" {{ $post->post_status ? 'checked' : '' }} disabled >
							@endif
							<label>公開</label>
						</div>

						<div>
							<input type="radio" name="post_status" value="0" {{ !$post->post_status ? 'checked' : '' }} >
							<label>下書き</label>
						</div>
					</div>
				</div>

				<!-- 12/02/2020 -->
				<!-- <div style="margin-top: 1rem;">
					<p class="font-weight-bold">CEO関連設定</p>

					<div class="dash-border-box">
						<p style="color: #c43638; margin: 1rem;"><i class="fas fa-caret-right" style="color: #c43638; width: 15px;"></i>記事別のSEO最適化設定</p>
					</div>
				</div> -->
				<!-- x 12/02/2020 x -->

				<div style="margin-top: 1rem;">
					<p class="font-weight-bold">外部サービス用RSS設定</p>

					<div class="dash-border-box">
						<p style="color: #c43638; margin: 1rem;"><a href="#wrap-seo" style="color: #c43638;" data-toggle="collapse" role="button" aria-expanded="false" aria-controls="wrap-seo"><i class="fas fa-caret-right" style="color: #c43638; width: 15px;"></i>外部サービス用RSSのメディア別設定</a></p>
						<div id="wrap-seo" class="collapse">
							<div class="form-group row" style="margin-top: 2.5rem;">
								<div class="col-lg-2 col-md-3 col-12 text-left text-md-right">
									<label class="font-weight-bold">タイトル</label>
								</div>
								<div class="col-lg-10 col-md-9 col-12">
									<input type="text" class="form-control" name="ogp_title" value="{{ old('ogp_title', $post->ogp_title) }}" />
									<p><em>タイトルには最大95文字を推奨します。</em></p>
								</div>
							</div>
							<div class="form-group row" style="margin-top: 2.5rem;">
								<div class="col-lg-2 col-md-3 col-12 text-left text-md-right">
									<label class="font-weight-bold">説明</label>
								</div>
								<div class="col-lg-10 col-md-9 col-12">
									<textarea name="ogp_description" rows="3" class="form-control">{{ old('ogp_description', $post->ogp_description) }}</textarea>
									<p><em>説明には最大200文字を推奨します。</em></p>
								</div>
							</div>
							<div class="form-group row" style="margin-top: 2.5rem;">
								<div class="col-lg-2 col-md-3 col-12 text-left text-md-right">
									<label class="font-weight-bold">画像</label>
								</div>
								<div class="col-lg-10 col-md-9 col-12">
									@if(!empty($post->ogp_image_url))
										<div style="margin-top: 16px;">
											<img src="{{ $post->ogp_image_url }}" style="max-height: 200px;max-width: 350px;" data-image-input-preview="ogp_image_url">
										</div>
									@endif
									<div style="margin-top: 5px;">
										<input name="ogp_image_url" class="form-control" value="{{ $post->ogp_image_url }}">
									</div>
									<div style="margin-top: 10px;">
										<button type="button" class="btn btn-no-radius open-library" data-input-name="ogp_image_url" data-input-type="url" data-toggle="modal" data-target="#library-modal" style="background: #111; color: #fff;"><i class="far fa-image" style="color: #fff;"></i>メディアライブラリから選択</button>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>

				<div style="margin-right: -15px; margin-left: -15px;">
					<div class="col-12 clearfix">
						<div class="float-left">
							<a href="{{ route('admin.post.destroy', $post) }}" class="btn-trash btn btn-no-radius mt-3" style=" width: 100px; font-size: 12px; line-height: 28px;">
								<i class="fas fa-trash-alt"></i>
								削除
							</a>
						</div>
						<div class="float-right">
							<a href="{{ route('admin.post.indexBySite') }}" class="btn btn-no-radius mt-3" style="font-size: 12px; width: 100px; line-height: 26px; color: #fff; background: #111111; margin-right: 10px;">
								<i class="fas fa-arrow-left" style="color: #fff"></i>
								キャンセル
							</a>
							<button class="btn-submit btn btn-no-radius mt-3" style=" width: 100px;">
								<i class="fas fa-check" style="color: #ffffff;"></i>
								保存
							</button>
						</div>
					</div>
				</div>
			</form>

			<form action="{{ route('admin.post.uploadfeatureimage', $post) }}" id="form-feature-image" method="post" enctype="multipart/form-data" class="d-none">
  			{{ csrf_field() }}
  			<input type="file" id="feature-file" class="hidden" name="file" accept="image/x-png,image/jpeg" style="display: none;" />
  		</form>
			<form action="{{ route('admin.slider.uploadSliderImages') }}" class="form-slide-images" method="post" enctype="multipart/form-data" class="d-none">
  			{{ csrf_field() }}
  			<input class="slider-id" type="hidden" name="slider_id" value="" />
  			<input type="file" id="add-slide-files" class="input-files" name="files[]" accept="image/x-png,image/jpeg" multiple="multiple" style="display: none;" />
  		</form>
			<form action="{{ route('admin.slider.uploadSliderImages') }}" class="form-slide-images" method="post" enctype="multipart/form-data" class="d-none">
  			{{ csrf_field() }}
  			<input type="hidden" name="post_id" value="{{ $post->id }}" />
  			<input type="file" id="new-slide" class="input-files" name="files[]" accept="image/x-png,image/jpeg" multiple="multiple" style="display: none;" />
  		</form>
		</div>
	</section>
	<!-- x admin-edit main section x -->
	<!-- Library modal -->
	@include('admin.templates.media.parts.modal-library')
	<!-- x Library modal x -->
	<style> 
		.add-tag.selected {
		opacity: 0.5;
		}
		.tags .tag-menu .tag-items {
		margin-right: 15px;
		}
		.ck-content.ck-editor__editable { 
	    min-height: 400px; 
		}
		.post-content h2 {
		font-size: 137.5%;
		font-weight: 700;
		margin-top: 2em;
	    padding: 7px 7px 4px;
	    border-top: 4px double #0050aa;
	    border-bottom: 4px double #0050aa;
		}
		.post-content h3 {
			font-size: 112.5%;
			font-weight: 700;
	    margin: 16px 0 8px 0;
	    border-left: 5px solid #0050aa;
	    line-height: 1.25;
	    padding: .15em 0 .1em 0.65em;
		}
		.post-content h4 {
		font-size: 16px;
	    font-weight: 600;
	    padding-left: 8px;
	    padding-bottom: 5px;
	    border-bottom: 1px dotted;
	    margin-bottom: 10px;
		}
		.post-content .summary {
			font-size: inherit;
		}
		.post-content p {
		  font-size: 14px;
		}
		.post-content i {
		  color: inherit;
		}
		/* .post-content .ck-content .image {
		  position: relative;
		  width: 100%;
		}
		.post-content .ck-content .image::after {
		  display: block;
		  content: '';
		  padding-bottom: 66.66%;
		}
		.post-content .ck-content .image img {
	    position: absolute;
	    left: 0;
	    top: 0;
	    width: 100%;
	    height: 100%;
      object-fit: cover;
  		object-position: center;
		} */
		.post-content iframe {
			width: 100%;
		}
		.break-page {
			font-size: 0;
	    width: 100%;
	    border-bottom: 1px solid #000;
	    position: relative;
	    margin: 40px 0;
		}
		.break-page:before {
			font-size: 15px;
	    content: '改ページ';
	    position: absolute;
	    background: #fff;
	    padding: 0 10px;
	    left: calc(50% - 55px);
	    top: -11px;
		}
		.instagram-media {
		  background: white;
		  max-width: 540px;
		  width: calc(100% - 2px);
		  border-radius: 3px;
		  border: 1px solid rgb(219, 219, 219);
		  box-shadow: none;
		  display: block;
		  margin: 0px 0px 12px;
		  padding: 0px;
		}
	</style>
@endsection
@section('footer_js')
	<!-- <script src="{{ asset('js/admin/ckeditor/ckeditor.min.js') }}"></script> -->
	<!-- <script src="{{ asset('js/admin/ckeditor/translations/ja.js') }}"></script> -->
	<script src="{{ asset('js/admin/ckeditor/ckeditor-build.js') }}"></script>
	<script>
      var _post_id = '{{{ $post->id }}}';
      var _post_upload_image_action = '{{{ route("admin.media.ckeditorpostuploadimage", $post) }}}';
      var _post_update_feature_image_action = '{{{ route("admin.post.updatefeatureimage", $post) }}}';
      var _post_update_feature_style_action = '{{{ route("admin.post.updatefeaturestyle", $post) }}}';
      var _token = '{{{ csrf_token() }}}';
  </script>
	<script src="{{ asset('js/admin/content-editor.js') }}"></script>
@endsection