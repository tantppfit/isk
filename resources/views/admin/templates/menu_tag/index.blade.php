@extends('admin.layouts.app')

@section('content')
	@include('admin.parts.left-menu')
	<!---------- admin-edit main section ---------->
	<section class="main">
		<div class="list-of-content-title">
			<h3>メニュータグ管理</h3>
			<h6>メディアの構成や特殊なタグ機能を設定します。</h6>
			@include('admin.parts.alert')
		</div>
		@include('admin.templates.menu_tag.parts.menu-tabs')
		<section id="tab-menu-content">
			<div class="nav-tab-1-title">
				<p>メディアの基本情報を設定します。</p>
			</div>
			<div class="menu-tag-management-table table-responsive-md" style="overflow: auto;">
				<table class="table">
					<tr>
					<th>表示順</th>
					<th>タグ名</th>
					<th>表示設定</th>
					<th></th>
					</tr>
					@if(count($menuTags))
						@foreach($menuTags as $menuTag)
							<tr>
								<td>{{ $loop->index+1 }}</td>
								<td>{{ $menuTag->tag->name }}</td>
								<td>
									<form action="{{ route('admin.menu_tag.update', $menuTag) }}" class="form-menu-tag" method="POST">
										{{ csrf_field() }}
										<p>
											<label>
												<input type="checkbox" class="checkbox" name="shown_in_header_menu" value="1" {{ ($menuTag->shown_in_header_menu) ? 'checked' : '' }}>
												<span>表示</span>
											</label>
										</p>
									</form>
								</td>
								<td>
									<a href="{{ route('admin.menu_tag.destroy', $menuTag) }}"><i class="fas fa-trash-alt" style="color: gray; font-size: 20px;" aria-hidden="true"></i></a>
								</td>
							</tr>
						@endforeach
					@endif
					<tr>
						<td colspan="6">
							<form action="{{ route('admin.menu_tag.store') }}" method="post">
								{{ csrf_field() }}
								<input type="text" class="form-control" placeholder="タグ名" name="name" style="display: inline-block; height: 35px; width:20%; font-size: 12px;">
								<button class="btn" style="background: #111111; width: 130px; color: #ffffff;height:32px;margin-bottom: 4px;border-radius: 0;font-size: 14px;"><i class="fas fa-plus" style="width: 25px; color: #ffffff;"></i>タグを追加
								</button>
							</form>
						</td>
					</tr>
				</table>	
			</div>
		</section>
		
	</section>
	<!---------- admin-edit main section ---------->
@endsection