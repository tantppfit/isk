@extends('admin.layouts.app')

@section('content')
	@include('admin.parts.left-menu')
	<!---------- main ---------->
	<section class="main">
		<div class="list-of-content-title">
			<h3>メディア設定</h3>
			@include('admin.parts.alert')
		</div>
		@include('admin.templates.setting.parts.setting-tabs')

		<form action="{{ route('admin.setting.update',['redirect' => route('admin.setting.footer')]) }}" method="POST" class="media-edit-form" style="margin: 1rem 0;">
			<section id="nav-tab-1">
				<div class="nav-tab-1-title">
					<p>フッターの外部ロゴ設定</p>
				</div>
				<div class="media-edit">
					<div class="form-group row" style="padding: 1rem 0 2rem 0;">
						<div class="col-lg-2 col-md-3 text-left text-md-right">
							<label>ロゴ1</label>
						</div>
						<div class="col-12 col-lg-10 col-md-9">
							<div class="img-wrap" style="width: 200px; height: 100px; background: url('{{ (array_key_exists('footer_banner_image_1', $settings)) ? $settings['footer_banner_image_1'] : '' }}'); margin: 1rem 0 .5rem 0;" data-image-input-preview="footer_banner_image_1">
							</div>
							<input type="text" class="form-control" placeholder="" name="footer_banner_image_1" value="{{ (array_key_exists('footer_banner_image_1', $settings)) ? $settings['footer_banner_image_1'] : '' }}" style="margin-bottom: .5rem; font-size: 12px;">
							<button type="button" class="btn btn-no-radius open-library" data-input-name="footer_banner_image_1" data-input-type="url" data-toggle="modal" data-target="#library-modal" style="background: #111; color: #fff;"><i class="far fa-image" style="color: #fff;"></i>メディアライブラリから選択</button>
						</div>
					</div>
					<div class="form-group row" style="padding: 1rem 0 2rem 0;">
						<div class="col-lg-2 col-md-3 text-left text-md-right">
							<label>URL1</label>
						</div>
						<div class="col-12 col-lg-10 col-md-9">
							<input type="text" class="form-control" name="footer_banner_link_1" value="{{ array_key_exists('footer_banner_link_1', $settings) ? $settings['footer_banner_link_1'] : '' }}" style="font-size: 12px;" />
						</div>
					</div>
					<div class="form-group row" style="padding: 1rem 0 2rem 0;">
						<div class="col-lg-2 col-md-3 text-left text-md-right">
							<label>ロゴ2</label>
						</div>
						<div class="col-12 col-lg-10 col-md-9">
							<div class="img-wrap" style="width: 200px; height: 100px; background: url('{{ (array_key_exists('footer_banner_image_2', $settings)) ? $settings['footer_banner_image_2'] : '' }}'); margin: 1rem 0 .5rem 0;" data-image-input-preview="footer_banner_image_2">
							</div>
							<input type="text" class="form-control" placeholder="" name="footer_banner_image_2" value="{{ (array_key_exists('footer_banner_image_2', $settings)) ? $settings['footer_banner_image_2'] : '' }}" style="margin-bottom: .5rem; font-size: 12px;">
							<button type="button" class="btn btn-no-radius open-library" data-input-name="footer_banner_image_2" data-input-type="url" data-toggle="modal" data-target="#library-modal" style="background: #111; color: #fff;"><i class="far fa-image" style="color: #fff;"></i>メディアライブラリから選択</button>
						</div>
					</div>
					<div class="form-group row" style="padding: 1rem 0 2rem 0;">
						<div class="col-lg-2 col-md-3 text-left text-md-right">
							<label>URL2</label>
						</div>
						<div class="col-12 col-lg-10 col-md-9">
							<input type="text" class="form-control" name="footer_banner_link_2" value="{{ array_key_exists('footer_banner_link_2', $settings) ? $settings['footer_banner_link_2'] : '' }}"  style="font-size: 12px;" />
						</div>
					</div>
				</div>
			</section>
			<section id="nav-tab-1">
				<div class="nav-tab-1-title">
					<p>フッターロゴの表示設定</p>
				</div>
				<div class="media-edit">
						<div class="form-group row" style="padding: 1rem 0 2rem 0;">
							<div class="col-lg-2 col-md-3 text-left text-md-right">
								<label>AideaStyleのロゴ</label>
							</div>
							<div class="col-12 col-lg-10 col-md-9">
								<div class="form-check-inline">
								  <label class="form-check-label">
								    <input type="hidden" name="footer_menu_appearance" value="hide" checked />
								    <input type="checkbox" class="form-check-input" name="footer_menu_appearance" value="show" {{ (array_key_exists('footer_menu_appearance', $settings) && $settings['footer_menu_appearance'] == 'show') ? 'checked' : '' }}>表示
								  </label>
								</div>
							</div>
						</div>
						<div class="form-group row" style="padding: 1rem 0 2rem 0;">
							<div class="col-lg-2 col-md-3 text-left text-md-right">
								<label>外部のロゴ</label>
							</div>
							<div class="col-12 col-lg-10 col-md-9">
								<div class="form-check-inline">
								  <label class="form-check-label">
								  	<input type="hidden" name="footer_banner_appearance" value="hide" checked />
								    <input type="checkbox" class="form-check-input" name="footer_banner_appearance" value="show" {{ (array_key_exists('footer_banner_appearance', $settings) && $settings['footer_banner_appearance'] == 'show') ? 'checked' : '' }}>表示
								  </label>
								</div>
							</div>
						</div>
				</div>
			</section>
			<button class="btn btn-no-radius" style="background: #111; color: #fff;">更新</button>
			{{ csrf_field() }}
		</form>
	</section>
	<!-----x----- main -----x----->
	<!-- Library modal -->
	@include('admin.templates.media.parts.modal-library')
	<!--x-- Library modal -->
@endsection