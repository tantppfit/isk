@extends('admin.layouts.app')

@section('content')
	@include('admin.parts.left-menu')
	<!---------- main ---------->
	<section class="main">
		<div class="list-of-content-title">
			<h3>メディア設定</h3>
			@include('admin.parts.alert')
		</div>
		@include('admin.templates.setting.parts.setting-tabs')
		<form action="{{ route('admin.setting.update',['redirect' => route('admin.setting.stylesheet')]) }}" method="POST" class="media-edit-form" style="margin: 1rem 0;">
			<section>
				<div>
					<p>サイドバー設定</p>
				</div>

				<div>
					{{-- <div class="form-group row" style="padding: 1rem 0 2rem 0;">
						<div class="col-lg-2 col-md-3">
							<label>Feature category</label>
						</div>
						<div class="col-12 col-lg-10 col-md-9">
							<select name="siderbar_feature_cat_id" class="form-control">
								<option value="">Select category</option>
								@if(isset($parentCategories) && count($parentCategories))
									@foreach($parentCategories as $parentCategory)
										<option value="{{ $parentCategory->id }}" {{ (array_key_exists('siderbar_feature_cat_id', $settings) && ($settings['siderbar_feature_cat_id'] == $parentCategory->id)) ? 'selected' : '' }}>{{ $parentCategory->name }}</option>
									@endforeach
								@endif
							</select>
						</div>
					</div> --}}
					<div class="form-group row" style="padding: 1rem 0 2rem 0;">
						<div class="col-lg-2 col-md-3 text-left text-md-right">
							<label>Feature</label>
						</div>
						<div class="col-12 col-lg-10 col-md-9">
							<input type="number" class="form-control" name="siderbar_feature_number" min="1" value="{{ array_key_exists('siderbar_feature_number', $settings) ? $settings['siderbar_feature_number'] : '1' }}" style="font-size: 12px;" />
							<div><p style="color: #888;">Limit item</p></div>
						</div>
					</div>
					{{-- <div class="form-group row" style="padding: 1rem 0 2rem 0;">
						<div class="col-lg-2 col-md-3 text-left text-md-right">
							<label>Series category</label>
						</div>
						<div class="col-12 col-lg-10 col-md-9">
							<select name="siderbar_series_cat_id" class="form-control">
								<option value="">Select category</option>
								@if(isset($parentCategories) && count($parentCategories))
									@foreach($parentCategories as $parentCategory)
										<option value="{{ $parentCategory->id }}" {{ (array_key_exists('siderbar_series_cat_id', $settings) && ($settings['siderbar_series_cat_id'] == $parentCategory->id)) ? 'selected' : '' }}>{{ $parentCategory->name }}</option>
									@endforeach
								@endif
							</select>
						</div>
					</div> --}}
					<div class="form-group row" style="padding: 1rem 0 2rem 0;">
						<div class="col-lg-2 col-md-3 text-left text-md-right">
							<label>Series</label>
						</div>
						<div class="col-12 col-lg-10 col-md-9">
							<input type="number" class="form-control" name="siderbar_series_number" min="1" value="{{ array_key_exists('siderbar_series_number', $settings) ? $settings['siderbar_series_number'] : '1' }}" style="font-size: 12px;" />
							<div><p style="color: #888;">Limit item</p></div>
						</div>
					</div>
					<div class="form-group row" style="padding: 1rem 0 2rem 0;">
						<div class="col-lg-2 col-md-3 text-left text-md-right">
							<label>Ranking</label>
						</div>
						<div class="col-12 col-lg-10 col-md-9">
							<input type="number" class="form-control" name="siderbar_ranking_number" min="1" value="{{ array_key_exists('siderbar_ranking_number', $settings) ? $settings['siderbar_ranking_number'] : '1' }}" style="font-size: 12px;" />
							<div><p style="color: #888;">Limit item</p></div>
						</div>
					</div>
				</div>
			</section>

			<!-- 12/02/2020 -->
			<!-- <section>
				<div>
					<p>Stylesheet customize</p>
				</div>
				<div>
					<div class="form-group row" style="padding: 1rem 0 2rem 0;">
						<div class="col-12">
							<textarea name="stylesheet" id="stylesheet_editor" class="form-control" rows="10" style="font-size: 12px;">{{ (array_key_exists('stylesheet', $settings)) ? $settings['stylesheet'] : '' }}</textarea>
						</div>
					</div>
					
				</div>
			</section> -->
			<!-- x 12/02/2020 x -->

			{{-- <section>
				<div>
					<p>Javascript customize for admin page</p>
				</div>
				<div>
					<div class="form-group row" style="padding: 1rem 0 2rem 0;">
						<div class="col-12">
							<textarea name="admin_javascript" id="admin_javascript_editor" class="form-control" rows="10" style="font-size: 12px;">{{ (array_key_exists('admin_javascript', $settings)) ? $settings['admin_javascript'] : '' }}</textarea>
						</div>
					</div>
					
				</div>
			</section> --}}
			<button class="btn btn-no-radius" style="background: #111; color: #fff;">更新</button>
			{{ csrf_field() }}
		</form>
	</section>
	<!-----x----- main -----x----->
@endsection
@section('header_css')
	<link rel="stylesheet" type="text/css" href="https://codemirror.net/lib/codemirror.css" />
	<link rel="stylesheet" type="text/css" href="https://codemirror.net/addon/hint/show-hint.css" />
@endsection
@section('footer_js')
	<script src="https://codemirror.net/lib/codemirror.js"></script>
	<script src="https://codemirror.net/mode/css/css.js"></script>
	<script src="https://codemirror.net/mode/javascript/javascript.js"></script>
	<script src="https://codemirror.net/addon/hint/show-hint.js"></script>
	<script src="https://codemirror.net/addon/hint/css-hint.js"></script>
	<script>
      var editor = CodeMirror.fromTextArea(document.getElementById("stylesheet_editor"), {
      	lineNumbers: true,
      	lineWrapping: true,
        extraKeys: {"Ctrl-Space": "autocomplete"}
      });
      var admin_js_editor = CodeMirror.fromTextArea(document.getElementById("admin_javascript_editor"), {
      	lineNumbers: true,
      	lineWrapping: true,
      	mode:  "javascript",
        extraKeys: {"Ctrl-Space": "autocomplete"}
      });
  </script>
@endsection