@extends('admin.layouts.app')

@section('content')
	@include('admin.parts.left-menu')
	<!---------- admin-edit main section ---------->
	<section class="main">
		<div class="post-edit">
			<h3>アンケート・編集</h3>
			@include('admin.parts.alert')
		</div>

		<div class="admin-post-news">
			<form class="post-news" action="{{ route('admin.vote.store') }}" method="post">
				{{ csrf_field() }}

				<div class="form-group row" style="margin-top: 2.5rem;">
					<div class="label-admin-edit col-lg-2 col-md-3 col-12">
						<label>タイトル</label>
					</div>
					<div class="col-md-9 col-12">
						<input type="text" class="form-control" name="name" value="{{ old('name') }}" required="">
					</div>
				</div>
				<div class="form-group row" style="margin-top: 2.5rem;">
					<div class="label-admin-edit col-lg-2 col-md-3 col-12">
						<label>内容</label>
					</div>
					<div class="col-md-9 col-12">
						<textarea class="form-control" name="content" rows="3" required="">{{ old('content') }}</textarea>
					</div>
				</div>

				<div class="form-group row" style="margin-top: 2.5rem;">
					<div class="label-admin-edit col-lg-2 col-md-3 col-12">
						<label>OPT 1</label>
					</div>
					<div class="col-md-9 col-12">
						<textarea class="form-control" name="opt_1" rows="3">{{ old('opt_1') }}</textarea>
					</div>
				</div>

				<div class="form-group row" style="margin-top: 2.5rem;">
					<div class="label-admin-edit col-lg-2 col-md-3 col-12">
						<label>OPT 2</label>
					</div>
					<div class="col-md-9 col-12">
						<textarea class="form-control" name="opt_2" rows="3">{{ old('opt_2') }}</textarea>
					</div>
				</div>

				<div class="form-group row" style="margin-top: 2.5rem;">
					<div class="label-admin-edit col-lg-2 col-md-3 col-12">
						<label>OPT 3</label>
					</div>
					<div class="col-md-9 col-12">
						<textarea class="form-control" name="opt_3" rows="3">{{ old('opt_3') }}</textarea>
					</div>
				</div>

				<div class="form-group row" style="margin-top: 2.5rem;">
					<div class="label-admin-edit col-lg-2 col-md-3 col-12">
						<label>OPT 4</label>
					</div>
					<div class="col-md-9 col-12">
						<textarea class="form-control" name="opt_4" rows="3">{{ old('opt_4') }}</textarea>
					</div>
				</div>

				<div class="form-group row" style="margin-top: 2.5rem;">
					<div class="label-admin-edit col-lg-2 col-md-3 col-12">
						<label>OPT 5</label>
					</div>
					<div class="col-md-9 col-12">
						<textarea class="form-control" name="opt_5" rows="3">{{ old('opt_5') }}</textarea>
					</div>
				</div>

				<div>
					<div class="col-11" style="margin-top: 1rem;">
						<div class="float-right">
							<a href="{{ route('admin.setting.vote') }}" class="btn btn-no-radius" style="font-size: 12px; width: 100px; line-height: 26px; color: #fff; background: #111111; margin-right: 10px;">
								<i class="fas fa-arrow-left" style="color: #fff"></i>
								キャンセル
							</a>
							<button class="btn-submit btn btn-no-radius" style=" width: 100px;">
								<i class="fas fa-check" style="color: #ffffff;"></i>
								保存
							</button>
						</div>
					</div>
				</div>
			</form>
		</div>
	</section>
	<!-----x----- admin-edit main section -----x----->
@endsection