@extends('admin.layouts.app')

@section('content')
	@include('admin.parts.left-menu')
	<!---------- admin-edit main section ---------->
	<section class="main">
		<div class="list-of-content-title">
			<h3>メディアライブラリ</h3>
			<p>メディアライブラリに登録されているファイルを管理します。</p>
			@include('admin.parts.alert')
		</div>

		<div class="col-12 dash-border-box" style="margin: 20px 0;">
			<form action="{{ route('admin.media.index') }}" method="get">
				<div class="row">
					<div style="margin: 1rem;margin-left: 10px; margin-top: 27px;">
						<i class="fas fa-search" style="font-size: 24px; color: #d8dce3;"></i>
					</div>
				
					<div style="margin: .4rem 0;">
						<input class="form-control" type="text" name="s" value="{{ request()->s }}" style="margin: 15px 0; font-size: 12px; width: 250px;" placeholder="Search image name...">
							<!-- <select style="margin: 15px 0 15px 15px;">
								<option>Image name</option>
								<option>Post title</option>
							</select> -->
						<input class="form-control" type="text" name="pt" value="{{ request()->pt }}" style="margin: 15px 0; font-size: 12px; width: 250px;" placeholder="Search page title...">
						<button class="btn btn-no-radius" style="background: #111111; color: #ffffff;width: 7rem;">検索</button>
					</div>
				</div>
			</form>
		</div>
		
		<p style="color: #999999;">#{{ (($medias->currentPage() - 1) * $medias->perPage()) + 1 }}...#{{ ($medias->currentPage() * $medias->perPage()) <= $medias->total() ? $medias->currentPage() * $medias->perPage() : $medias->total() }} / {{ $medias->total() }}</p>
		<div class="pagination-menu d-flex">{{ $medias->appends(request()->except('page'))->onEachSide(1)->links() }}</div>

		<div class="media-table-library table-responsive-md" style="overflow: auto;">
			<table class="table">
				<tr>
					<th></th>
					<th>ファイル情報</th>
					<th>公開日</th>
					<th></th>
				</tr>
				<tr>
				  	<td colspan="4">
							<form action="{{ route('admin.media.store') }}" method="post" enctype="multipart/form-data" onchange="this.submit()">
			  			{{ csrf_field() }}
			  			<input type="file" id="file" class="hidden" name="file" accept="image/x-png,image/jpeg" style="display: none;" />
			  			<label for="file" class="btn" style="background: #111111; width: 200px; color: #ffffff;height:40px;margin-bottom: 4px;border-radius: 0; font-size: 12px; line-height: 27px;"><i class="fas fa-plus" style="width: 25px; color: #ffffff;"></i>ファイルを追白加</label>
			  		</form>
				  	</td>
				</tr>
				@if(count($medias))
					@foreach($medias as $media)
						<tr>
							<td>
								<div class="media-ratio-library" style="background-image: url('{{ Helper::getMediaUrl($media, 'thumbnail') }}');"></div>
							<td>
								<div class="media-text-library">
									<p>{{ $media->name }}</p>
									<p>{{ $media->info }}</p>
									<p>[<a href="{{ URL::to('/') . Helper::getMediaUrl($media, 'original') }}" class="link-hold-to-copy" target="_blank"><span>右クリックかロングタップでウェプ用のJPEG画像 {{ ($media->info) ? '('.explode(' ', $media->info)[0].') ' : '' }}URLをコピー</span></a>]</p>
									<a style="color: #C43638; text-decoration: none;" data-toggle="collapse" href="#collapse-media-{{ $media->id }}" role="button" aria-expanded="false" aria-controls="collapse-media-{{ $media->id }}"><p style="color: #c43638;"><i class="fas fa-caret-right" style="color: #c43638; width: 20px;"></i>他のサイズ</p></a>
									<div class="collapse" id="collapse-media-{{ $media->id }}">
										<ul class="media-other-size">
											@if($media->ratio4x3_path)
												<li>[<a href="{{ URL::to('/') . Helper::getMediaUrl($media, 'ratio4x3') }}" data-toggle="tooltip" title="右クリックかロングタップでウェプ用のJPEG画像 URLをコピー" target="_blank" class="link-hold-to-copy">Ratio4x3</a>]</li>
											@endif
											@if($media->ratio3x2_path)
												<li>[<a href="{{ URL::to('/') . Helper::getMediaUrl($media, 'ratio3x2') }}" data-toggle="tooltip" title="右クリックかロングタップでウェプ用のJPEG画像 URLをコピー" target="_blank" class="link-hold-to-copy">Ratio3x2</a>]</li>
											@endif
											@if($media->xlarge_path)
												<li>[<a href="{{ URL::to('/') . Helper::getMediaUrl($media, 'xlarge') }}" data-toggle="tooltip" title="右クリックかロングタップでウェプ用のJPEG画像 URLをコピー" target="_blank" class="link-hold-to-copy">X-large</a>]</li>
											@endif
											@if($media->large_path)
												<li>[<a href="{{ URL::to('/') . Helper::getMediaUrl($media, 'large') }}" data-toggle="tooltip" title="右クリックかロングタップでウェプ用のJPEG画像 URLをコピー" target="_blank" class="link-hold-to-copy">Large</a>]</li>
											@endif
											@if($media->medium_path)
												<li>[<a href="{{ URL::to('/') . Helper::getMediaUrl($media, 'medium') }}" data-toggle="tooltip" title="右クリックかロングタップでウェプ用のJPEG画像 URLをコピー" target="_blank" class="link-hold-to-copy">Medium</a>]</li>
											@endif
											@if($media->small_path)
												<li>[<a href="{{ URL::to('/') . Helper::getMediaUrl($media, 'small') }}" data-toggle="tooltip" title="右クリックかロングタップでウェプ用のJPEG画像 URLをコピー" target="_blank" class="link-hold-to-copy">Small</a>]</li>
											@endif
											@if($media->thumbnail_path)
												<li>[<a href="{{ URL::to('/') . Helper::getMediaUrl($media, 'thumbnail') }}" data-toggle="tooltip" title="右クリックかロングタップでウェプ用のJPEG画像 URLをコピー" target="_blank" class="link-hold-to-copy">Thumbnail</a>]</li>
											@endif
										</ul>
									  
									</div>
									<p>
										References:
									</p>
								</div>
								@if($media->post)
									<a href="{{ route('admin.post.edit', $media->post) }}" target="_blank" style="text-decoration:none;"><i class="far fa-edit d-flex" style="color: #C43638; margin: 10px;margin-bottom: 0;"><p style="margin-left: 5px;">{{ $media->post->created_at }}{{ $media->post->title }}</p></i>
									</a>
								@endif
							</td>
							<td style="color: #111111;"><p>{{ $media->created_at }}</p></td>
							<td style="vertical-align: top;"><a href="{{ route('admin.media.destroy', $media) }}" style="color: #C43638;text-decoration: none;" ><p style="width: 50px;"><i class="far fa-trash-alt" style="color: #C43638;margin-right: 5px;"></i>削除</p></a></td>
						</tr>
					@endforeach
				@endif
			</table>
		</div>
		<p style="color: #999999;">#{{ (($medias->currentPage() - 1) * $medias->perPage()) + 1 }}...#{{ ($medias->currentPage() * $medias->perPage()) <= $medias->total() ? $medias->currentPage() * $medias->perPage() : $medias->total() }} / {{ $medias->total() }}</p>
		<div class="pagination-menu d-flex">{{ $medias->appends(request()->except('page'))->onEachSide(1)->links() }}</div>
	</section>
	<!---------- admin-edit main section ---------->
@endsection