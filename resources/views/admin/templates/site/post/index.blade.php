@extends('admin.layouts.app')

@section('content')
	@include('admin.parts.left-menu')
	<!-- admin-edit main section -->
	<section class="main">
		<div class="list-of-content-title">
			<h3>コンテンツ一覧</h3>
			<p>サイトのコンテンツを編集します。</p>
			@include('admin.parts.alert')
		</div>
		<a href="{{ route('admin.post.create') }}" ><button class="btn btn-no-radius" style="background: #111111; color: #ffffff; margin-top: 2rem;"><i class="fas fa-pencil-alt" style="width: 25px; color: #ffffff;"></i>新規投稿</button></a>

		<div class="col-12 dash-border-box" style="margin: 20px 0;">
			<div class="row">
				<div style="margin: 1rem; margin-left: 10px;">
					<i class="fas fa-search" style="font-size: 24px; color: #d8dce3;"></i>
				</div>

				<div style="margin: .4rem 0;">
					<p class="font-weight-bold">投稿の絞り込み</p>
					<form action="{{ route('admin.post.indexBySite')}}" method="GET" autocomplete="off">
						<div class="row">
							@if(Auth::user()->role->slug == 'admin')
								<div>
									<select style="margin-right: 35px;width: 250px; margin-bottom: 5px;" name="user">
										<option value="">全て作成者</option>
										@if(isset($users) && count($users))
											@foreach($users as $user)
												<option value="{{ $user->id }}" {{ (request()->user == $user->id) ? 'selected' : '' }}>{{ $user->name }}</option>
											@endforeach
										@endif
									</select>
								</div>
							@endif
								<div>
									<select style="width: 250px;" name="post_status">
										<option value="">全て状態</option>
										<option value="0" {{ (request()->post_status == '0') ? 'selected' : '' }}>下書き</option>
										<option value="1" {{ (request()->post_status == '1') ? 'selected' : '' }}>公開</option>
									</select>
								</div>
						</div>
						<div class="row" style="margin-left: 0px;margin-top: 8px;">
						    <div>
								<input type="text" class="form-control datepicker" name="from" style="width: 250px;margin-right: 10px; margin-bottom: 5px; font-size: 12px;" value="{{ request()->from }}" placeholder="年−月−日">
							</div>
							<p style="width: 25px; margin-top: 5px;">~</p>
							<div>
								<input type="text" class="form-control datepicker" name="to" style="width: 250px;margin-right: 10px; margin-bottom: 5px;font-size: 12px;" value="{{ request()->to }}" placeholder="年−月−日">
							</div>
						</div>
	
						<div style="margin-top: 10px; margin-bottom:10px;">
						    <div>
								<input type="text" class="form-control" name="s" value="{{ request()->s }}" placeholder="検索" style="font-size: 12px; width: calc(100% - 10px);">
							</div>
						</div>
	
						<button class="btn btn-no-radius" style="background: #111111; color: #ffffff;width: 7rem;"><i class="fas fa-search" style="color: #ffffff; width: 25px;"></i>検索</button>
					</form>
				</div>
			</div>
		</div>
		<p style="color: #999999;">#{{ (($posts->currentPage() - 1) * $posts->perPage()) + 1 }}...#{{ ($posts->currentPage() * $posts->perPage()) <= $posts->total() ? $posts->currentPage() * $posts->perPage() : $posts->total() }} / {{ $posts->total() }}</p>
		<div class="pagination-menu d-flex">{{ $posts->appends(request()->except('page'))->onEachSide(1)->links() }}</div>

		<div class="content-table-list table-responsive-md">
			<table class="table">
				<tr>
					<th><input type="checkbox" id="check-all" class="bulk-checkbox" ></th>
					<th>ID</th>
					<th>画像</th>
					<th>タイトル</th>
					<th>作成者</th>
					<th>公開日</th>
					<th>状態</th>
					<th></th>
				</tr>
				@if(count($posts))
					@foreach($posts as $post)
						<tr style="{{ (!$post->post_status) ? 'opacity: 0.5;' : ''  }}">
							<td>
								@if(Auth::user()->role->slug == 'admin' || Auth::user()->id == $post->user_id)
									<input type="checkbox" class="bulk-checkbox" data-post-id="{{ $post->id }}" data-post-title="{{ ($post->title) ? $post->title : '('.$post->id.')' }}" />
								@endif
							</td>
							<td style="color: #111111;"><p>{{ $post->id }}</p></td>
							<td>
								<div class="d-block" style="width: 80px; height: 80px; margin: 0 auto;">
								@if($post->featureImage)
									<img src="{{ Helper::getMediaUrl($post->featureImage, 'thumbnail') }}" style="width:78px;">
								@elseif(Helper::getDefaultCover($post))
									<img src="{{ Helper::getDefaultCover($post) }}" style="width:78px;">
								@endif
								</div>
							</td>
							<td><p style="color:#C43638;"><a style="color: #C43638;" href="{{ ($post->post_status) ? route('frontend.post.show', $post->slug) : route('frontend.post.preview', $post) }}" target="_blank">{{ $post->title }}</a></p></td>
							<td style="color: #C43638;"><p style="width: 120px;">{{ $post->user->name }}</p></td>
							<td style="color: #111111;"><p>{{ $post->created_at->format('Y/m/d H:i') }}</p></td>
							<td style="color: #111111;"><p style="width: 50px;">{{ $post->post_status ? '公開' : '下書き' }}</p></td>
							<td>
								<div class="d-flex" style="width: 85px;">
									@if(Auth::user()->role->slug == 'admin' || Auth::user()->id == $post->user_id)
									<a href="{{ route('admin.post.edit', $post->id) }}" style="display: inline-block; color: #C43638; margin-right: 25px;"><p style="margin-bottom: 50px;"><i class="far fa-edit" style="color: #C43638;margin-right: 5px;"></i>編集</p></a>
									<a href="{{ route('admin.post.destroy', $post) }}"><i class="fas fa-trash-alt" style="color: gray; font-size: 20px; vertical-align: middle;" aria-hidden="true"></i></a>
									@endif
								</div>
							</td>
						</tr>
					@endforeach
				@endif
			</table>
		</div>
		
		<p style="margin-top: 1rem;color: black;">投稿の一括操作</p>

		<div>
			<select id="setting-post">
				<option value="default">一括操作</option>
				@if(Auth::user()->role->slug == 'admin')
					<option value="setting-post-status">状態操作</option>
				@endif
				<option value="setting-post-category" >カテゴリー操作</option>
				<option value="setting-post-tag" >タグ操作</option>
				<option value="setting-post-delete">ゴミ箱へ移動</option>
				@if(Auth::user()->role->slug == 'admin')
					<option value="setting-post-author">作成者</option>
				@endif
				<option value="setting-post-release-date">公開日</option>
			</select>
			@if(Auth::user()->role->slug == 'admin')
				<div class="box-setting-post setting-post-status mt-3">
					<form action="{{ route('admin.post.changeAll') }}" method="POST">
						{{ csrf_field() }}
						<input type="hidden" name="_action" value="update_status">
						<select name="post_status" required="">
							<option value="1">公開</option>
							<option value="0">下書き</option>
						</select>
						<input type="hidden" name="post_ids" value="" />
						<input type="hidden" name="redirect" value="{{ url()->full() }}" />
						<button type="submit" class="btn mt-3 d-block btn-no-radius btn-dark">状態の一括操作の適用</button>
					</form>
				</div>
			@endif

			<div class="box-setting-post setting-post-category mt-3">
				<form action="{{ route('admin.post.changeAll') }}" method="POST">
					{{ csrf_field() }}
					<input type="hidden" name="_action" value="update_categories">
					<input type="hidden" name="post_ids" value="" />
					<input type="hidden" name="redirect" value="{{ url()->full() }}" />
					<select name="category_id" required="">
						<option value="">カテゴリー</option>
						@if(isset($parentCategories) && count($parentCategories))
							@foreach($parentCategories as $parentCategory)
								<option value="{{ $parentCategory->id }}">{{ $parentCategory->name }}</option>
								@if($parentCategory->children && count($parentCategory->children))
					  			@foreach($parentCategory->children as $categoryChild)
										<option value="{{ $categoryChild->id }}">-- {{ $categoryChild->name }}</option>
					  			@endforeach
					  		@endif
							@endforeach
						@endif
					</select>
					<select name="cat_action">
						<option value="add">追加する</option>
						<option value="remove">取り除く</option>
					</select>
					<button class="btn btn-no-radius btn-dark d-block mt-3">カテゴリーの一括操作の適用</button>
				</form>
			</div>
			<div class="box-setting-post setting-post-tag mt-3">
				<form action="{{ route('admin.post.changeAll') }}" method="POST">
					{{ csrf_field() }}
					<input type="hidden" name="_action" value="update_tags">
					<input type="hidden" name="post_ids" value="" />
					<input type="hidden" name="redirect" value="{{ url()->full() }}" />
					<div class="list-tag-setting-post mt-3">
						<select name="tag_id">
							<option value="">タグ</option>
							@if(isset($tags) && count($tags))
								@foreach($tags as $tag)
									<option value="{{ $tag->id }}">{{ $tag->name }}</option>
								@endforeach
							@endif
						</select>
					</div>

					<div class="mt-3">
						<input type="text" class="form-control" placeholder="タグを入力" name="name" style="width: 25%; margin-right: 10px;">
					</div>

					<p>カンマ区切りで複数のタグを同時に追加できます</p>

					<select name="tag_action">
						<option value="add">追加する</option>
						<option value="remove">取り除く</option>
					</select>
					<button class="btn btn-no-radius btn-dark d-block mt-3">タグの一括操作の適用</button>
				</form>
			</div>

			<div class="box-setting-post setting-post-delete mt-3">
				<form action="{{ route('admin.post.changeAll') }}" method="POST">
					{{ csrf_field() }}
					<input type="hidden" name="_action" value="delete">
					<input type="hidden" name="post_ids" value="" />
					<input type="hidden" name="redirect" value="{{ url()->full() }}" />
					<button type="button" class="btn btn-dark btn-no-radius" data-toggle="modal" data-target="#setting-post-del-modal">削除の一括操作の適用</button>
				
					<div class="modal fade" id="setting-post-del-modal" tabindex="-1" role="dialog" aria-hidden="true">
						<div class="modal-dialog" role="document">
							<div class="modal-content">
								<div class="modal-header">
									<h5 class="modal-title">List title post</h5>
								</div>

								<div class="modal-body">
									<div class="list-post">
									</div>
								</div>
								<div class="modal-footer">
									<button type="button" class="btn btn-secondary mr-3" data-dismiss="modal" style="width: 100px;">キャンセル</button>
									<button type="submit" class="btn btn-dark" style="width: 100px;">削除</button>
								</div>
							</div>
						</div>
					</div>
				</form>
			</div>
			@if(Auth::user()->role->slug == 'admin')
				<div class="box-setting-post setting-post-author mt-3">
					<form action="{{ route('admin.post.changeAll') }}" method="POST">
						{{ csrf_field() }}
						<input type="hidden" name="_action" value="update_author">
						<select name="user_id" required="">
							<option value="">作成者を選択</option>
							@if(isset($users) && count($users))
								@foreach($users as $user)
									<option value="{{ $user->id }}">{{ $user->name }}</option>
								@endforeach
							@endif
						</select>
						<input type="hidden" name="post_ids" value="" />
						<input type="hidden" name="redirect" value="{{ url()->full() }}" />
						<button type="submit" class="btn btn-dark btn-no-radius d-block mt-3">作成者の一括操作の適用</button>
					</form>
				</div>
			@endif
			<div class="box-setting-post setting-post-release-date mt-3">
				<form action="{{ route('admin.post.changeAll') }}" method="POST">
					{{ csrf_field() }}
					<input type="hidden" name="_action" value="update_created_at">
					<!-- <input type="text" class="form-control datetimepicker" name="created_at" style="width: 250px;margin-right: 10px; margin-bottom: 5px;" value="{{ date('m/d/Y H:i:s', time()) }}"> -->
					<!-- <input type="text" class="form-control datetimepicker" name="created_at" style="width: 250px;margin-right: 10px; margin-bottom: 5px;" value="{{ date('Y/m/d H:i:s', time()) }}"> -->
					<input type="text" class="form-control datetimepicker-nos" name="created_at" style="width: 250px;margin-right: 10px; margin-bottom: 5px;" value="{{ date('Y/m/d H:i', time()) }}">
					<input type="hidden" name="post_ids" value="" />
					<input type="hidden" name="redirect" value="{{ url()->full() }}" />
					<button type="submit" class="btn btn-dark btn-no-radius d-block mt-3">公開日の一括操作の適用</button>
				</form>
			</div>
		</div>

		<p style="color: #999999;">#{{ (($posts->currentPage() - 1) * $posts->perPage()) + 1 }}...#{{ ($posts->currentPage() * $posts->perPage()) <= $posts->total() ? $posts->currentPage() * $posts->perPage() : $posts->total() }} / {{ $posts->total() }}</p>
		<div class="pagination-menu d-flex">{{ $posts->appends(request()->except('page'))->onEachSide(1)->links() }}</div>
	</section>
	<!-- x admin-edit main section x -->
@endsection