@extends('admin.layouts.app')

@section('content')
	@include('admin.parts.left-menu')
	<!---------- main ---------->
	<section class="main">
		<div class="list-of-content-title">
			<h3>メディア貢献度</h3>
		</div>
		<nav class="content-tabs-management">
				<div class="nav nav-tabs" id="nav-tab" role="tablist">
  			<a class="nav-item nav-link" href="{{ route('admin.statistic.index') }}">投稿</a>
  			<a class="nav-item nav-link active" href="{{ route('admin.statistic.user') }}">ユーザーランキング</a>
				</div>
		</nav>
		<div style="margin-top: 10px;">
			<p style="margin-bottom: 0px;">直近30日間の、メディア上での貢献度情報を記事別に確認します。</p>
			<p>記事作成者は自分の執筆した記事の情報だけを確認できます。管理者はすべての記事の情報を確認できます。</p>
		</div>
		<div class="container">
	    	<div id="chartdiv" style="width: 100%; height: 300px;"></div>
    		<p id="chartTitle" style="text-align: center;">
    			@if(isset($userStatistic) && !is_null($userStatistic))
    				{{ $userStatistic->name }}
  				@endif
  			</p>
	 	</div>
		<div class="media-table-contribution table-responsive-md" style="overflow: auto;">
			<table class="table" style="font-size: 12px;">	  
				<tr>
				  	<th style="border-right: none; border-left: none;"><p style="margin-left: 10%;margin-bottom: 0;">User name</p></th>
				    <th style="text-align: right;border-left: none;border-right: none;margin-left: 5px;">30&nbsp;Day&nbsp;&nbsp;
				    	<div class="float-right" style="position: relative;">
				    		<a href="{{ route('admin.statistic.user', ['order' => 'asc']) }}"><i class="fas fa-sort-up" style="color: {{ (request()->order=='asc') ? '#C43638' : 'black'}}; position: absolute; top: 50%; left: -8px; transform: translateY(-32%); "></i></a>
				    		 &nbsp;
				    		<a href="{{ route('admin.statistic.user', ['order' => 'desc']) }}"><i class="fas fa-sort-down" style="color: {{ (request()->order!='asc') ? '#C43638' : 'black'}}; position: absolute; top: 35%; right: -8px; transform: translateY(-50%);"></i></a>
				    	</div>
				    </th>
				</tr>
				@if(isset($users) && count($users))
					@foreach( $users as $user )
						<tr class="user-view-statistic {{ $loop->first ? 'table-active' : '' }}" style="cursor: pointer;" data-action="{{ route('admin.user.get_view_log', $user) }}">
							<td>
								<div style="float: left;margin-right: 10px;">
									<img src="{{ !empty($user->avatar) ? Helper::getImageUrl($user->avatar) : asset('image/no-img.png') }}" style="width: 50px;">
								</div>
								<p><span>{{ $user->name }}</span>
								</p>
								<div style="clear: both; "></div>
							</td>
							<td style="text-align: right;"><p style="margin-right: 15px;">{{ ($totalView) ? number_format(($user->last_month_total_views_count/$totalView)*100, 2) : '' }}%</p></td>
						</tr>
					@endforeach
				@endif
			</table>
		</div>
		<p style="color: #999999;">#{{ (($users->currentPage() - 1) * $users->perPage()) + 1 }}...#{{ ($users->currentPage() * $users->perPage()) <= $users->total() ? $users->currentPage() * $users->perPage() : $users->total() }} / {{ $users->total() }}</p>
		<div class="pagination-menu d-flex">{{ $users->appends(['order' => request()->order])->onEachSide(1)->render() }}</div>
		<div class="col-12 dash-border-box" style="margin: 20px 0;">
			<div class="d-flex">
				<div style="margin: 1rem;margin-left: 10px; margin-top: 27px;">
					<i class="fas fa-search" style="font-size: 24px; color: #d8dce3;"></i>
				</div>

				<div style="margin: .4rem 0;">
					<div>
						<form action="{{ route('admin.statistic.user') }}" method="GET" class="d-flex">
							<input class="form-control" type="text" name="id" value="{{ request()->get('id') }}" placeholder="123456 - ユーザーID" style="margin: 15px 0; font-size: 12px;">
							<button class="btn btn-no-radius" style="background: #111111; color: #ffffff;width: 7rem; margin: 15px; "><i class="fas fa-search" style="color: #ffffff; width: 25px;"></i>検索</button>
						</form>
					</div>
					<div style="opacity: .5;">
						<i class="fas fa-info-circle" style="margin-right: 15px;margin-left: 5px;color: gray; float: left;"></i>
					</div>
					<div class="text-media-contribution">
						<p style="margin-bottom: 0px;">特定の己事のみに絞り込みできます。</p>
						<p>ブックマークによる定点観測情情報共有などにご利用ください</p>
					</div>
					
				</div>
			</div>
		</div>
	</section>
	<!-----x----- main -----x----->
@endsection
@section('footer_js')
	<script src="https://www.amcharts.com/lib/4/core.js"></script>
	<script src="https://www.amcharts.com/lib/4/charts.js"></script>
	<script src="https://www.amcharts.com/lib/4/lang/ja_JP.js"></script>
	<script src="https://www.amcharts.com/lib/4/themes/animated.js"></script>

	<!-- Chart code -->
	<script>
	am4core.ready(function() {

	// Themes begin
	am4core.useTheme(am4themes_animated);
	// Themes end

	var chart = am4core.create("chartdiv", am4charts.XYChart);
	chart.language.locale = am4lang_ja_JP;

	var data = [];
	var value = 0;
	var userStatistic = {!! (isset($userStatistic) && !is_null($userStatistic)) ? json_encode($userStatistic->toArray()) : json_encode([]) !!};
	if(typeof(userStatistic.view_log) != "undefined" && userStatistic.view_log !== null){
		data = userStatistic.view_log;
	}
	chart.data = data;

	// Create axes
	var dateAxis = chart.xAxes.push(new am4charts.DateAxis());
	dateAxis.renderer.minGridDistance = 150;

	var valueAxis = chart.yAxes.push(new am4charts.ValueAxis());
	valueAxis.min = 0;
	valueAxis.renderer.labels.template.adapter.add("text", function(text, target) {
	  return text.match(/\./) ? "" : text;
	});

	// Create series
	var series = chart.series.push(new am4charts.LineSeries());
	series.dataFields.valueY = "views";
	series.dataFields.dateX = "date";
	series.tooltipText = "{views} views"

	series.tooltip.pointerOrientation = "vertical";

	chart.cursor = new am4charts.XYCursor();
	chart.cursor.snapToSeries = series;
	chart.cursor.xAxis = dateAxis;

	chart.scrollbarX = new am4charts.XYChartScrollbar();
	chart.scrollbarX.series.push(series);

	}); // end am4core.ready()
	</script>
@endsection