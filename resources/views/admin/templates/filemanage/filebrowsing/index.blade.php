@extends('admin.layouts.app')

@section('content')
	@include('admin.parts.left-menu')
	<div class="bg" onclick="back()">
		
	</div>
	<div class="bg">
		<div class="file" onclick="">
			<a href=""><i class="fa fa-print"></i></a>
			<button class="btn" data-toggle="modal" data-target="#setting-post-download-modal" style="float: right;margin-right:0%;"><i class="fa fa-download" style="float: right;font-size: 35px;margin: 4% 1% 0 0;"></i>
			</button>
			<div class="setting-file-delete mt-3">
				<form action="{{ route('admin.filemanage.changeAll') }}" method="POST">
					{{ csrf_field() }}
					<input type="hidden" name="_action" value="download">
					<input type="hidden" name="post_ids" value="{{$id}}" />
					<input type="hidden" name="redirect" value="{{ url()->full() }}" />
					<!-- <button type="button" class="btn btn-dark btn-no-radius" data-toggle="modal" data-target="#setting-post-del-modal">DELETE</button> -->
					
					
					<div class="modal fade" id="setting-post-download-modal" tabindex="-1" role="dialog" aria-hidden="true">
						<div class="modal-dialog" role="document">
							<div class="modal-content">
								<div class="modal-header">
									<h5 class="modal-title ml-auto mr-auto">ファイルをダウンロードしますか？</h5>
								</div>

								<div class="modal-body">
									<div class="list-file text-center">
										{{$filebrowsing->name}}
									</div>
								</div>
								<div class="modal-footer">
									<button type="button" class="btn btn-secondary mr-3" data-dismiss="modal" style="width: 100px;">キャンセル</button>
									<button type="submit" class="btn btn-dark" style="width: 100px;">OK</button>
								</div>
							</div>
						</div>
					</div>
				</form>
			</div>
			<!-- <a href=""><i class="fa fa-trash-o"></i></a> -->
			<button class="btn" data-toggle="modal" data-target="#setting-post-del-modal" style="float: right;margin-top:1%;"><i class="fa fa-trash-o" style="float: right;font-size: 35px;margin: 30% 2% 0 0;"></i>
			</button>
			<div class="setting-file-delete mt-3">
				<form action="{{ route('admin.filemanage.changeAll') }}" method="POST">
					{{ csrf_field() }}
					<input type="hidden" name="_action" value="delete">
					<input type="hidden" name="post_ids" value="{{$id}}" />
					<input type="hidden" name="redirect" value="{{ url()->full() }}" />
					<!-- <button type="button" class="btn btn-dark btn-no-radius" data-toggle="modal" data-target="#setting-post-del-modal">DELETE</button> -->
					
					
					<div class="modal fade" id="setting-post-del-modal" tabindex="-1" role="dialog" aria-hidden="true">
						<div class="modal-dialog" role="document">
							<div class="modal-content">
								<div class="modal-header">
									<h5 class="modal-title ml-auto mr-auto">ファイルを削除しても宜しいですか？</h5>
								</div>

								<div class="modal-body">
									<div class="list-file text-center">
										{{$filebrowsing->name}}
									</div>
								</div>
								<div class="modal-footer">
									<button type="button" class="btn btn-secondary mr-3" data-dismiss="modal" style="width: 100px;">キャンセル</button>
									<button type="submit" class="btn btn-dark" style="width: 100px;">削除</button>
								</div>
							</div>
						</div>
					</div>
				</form>
			</div>			
		</div>
	</div>
	<div class="edit">
		<section class="main">			
			<div class="content-file">
				<div class="title">
					<p>「{{ $filebrowsing->name}}」</p>	
					<!-- @foreach ($pages as $page) {
					    {{$page->getText()}}
					}
					@endforeach -->
					<p>
					@php ($format = $filebrowsing->name)
					@php ($format = strrchr( $format, '.'))
					@if ($format == '.xlsx' || $format == '.xls')	
						<img src="/image/icon/excel.png" class="icon-for-file"/>
					@elseif ($format == '.docx' || $format == '.doc')
						<img src="/image/icon/word.png" class="icon-for-file"/>
					@elseif ($format == '.pptx' || $format == '.ppt')
						<img src="/image/icon/powerpoint.png" class="icon-for-file"/>
					@elseif ($format == '.pdf')
						<img src="/image/icon/pdf.png" class="icon-for-file"/>
					@elseif ($format == '.csv')
						<img src="/image/icon/csv.png" class="icon-for-file"/>
					@endif
					</p>
				</div>
			</div>
		</section>
	</div>	
	<script>
		function back(){
			const url = "{{ route('admin.filemanage.index', $user->id) }}"
            location.href = url;
        }
	</script>
@endsection