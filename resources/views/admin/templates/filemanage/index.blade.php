@extends('admin.layouts.app')

@section('content')
@include('admin.parts.left-menu')
<!---------- admin-edit main section ---------->
<div class="admin-edit">
	<section class="main">
		@include('admin.parts.alert')
		<div class="col-12">
			<div class="row">
				<div style="margin: .4rem 0;width: 100%;margin-bottom: 2%">
					<form action="{{ route('admin.filemanage.index', $id['id']) }}" method="GET" autocomplete="off">
						<div style="float: left; width: 20%;">
							<p style="font-size: 16px; font-weight: 600;">{{ $user->name }} 様</p>
							<!--<p>notices</p>-->
						</div>
					    <div style="margin-left: 0px;margin-top: 8px;">
						    <div style="display:inline;">
						        <div style="margin-left: 5%;float: left;">ファイル名</div>
						        <div style="margin: 0 12% 0 12%;float: left;">登録日</div>
						        <div style="margin-left: 10%;">送信日</div>
						    </div>
							<div>
							    <input type="text" class="form-control" placeholder="ファイル名" name="filename" value="{{ request()->filename }}"style="display: inline-block; height: 35px; width:15%;border-radius: 0;">
							    <input type="text" class="datepicker form-control" placeholder="YYYY-MM-DD" name="from" value="{{ request()->from }}" style="display: inline-block; height: 35px; width:15%;margin-left: 10px;border-radius: 0;">
								<input type="text" class="datepicker form-control" placeholder="YYYY-MM-DD" name="to" value="{{ request()->to }}" style="display: inline-block; height: 35px; width:15%;margin-left: 10px;border-radius: 0;">	
						        <button class="btn" style="background:#D34725 ;color: #ffffff;margin-left:25px;width: 100px">検索
							    </button>
							</div>				
						</div>
					</form>

					<div style="display: inline";>	
						<button class="btn" data-toggle="modal" data-target="#setting-post-del-modal" style="float: right;margin-right:0%;width: 100px"><i class="far fa-trash-alt" style="float: right;font-size: 35px;margin: 4% 2% 0 0;"></i>
						</button>						
						<div class="setting-file-delete mt-3">
							<form action="{{ route('admin.filemanage.changeAll') }}" method="POST">
								{{ csrf_field() }}
								<input type="hidden" name="_action" value="download">
								<input type="hidden" name="post_ids" value="" />
								<input type="hidden" name="redirect" value="{{ url()->full() }}" />								
								<button type="submit" class="btn btn-download" data-toggle="modal" data-target="#setting-post-download-modal" style="float: right;margin-right:0%;width: 100px"><i class="fas fa-download" style="float: right;font-size: 35px;margin: -15% 2% 0 0;"></i>
								</button>
							</form>							
						</div>
						<div class="setting-file-delete mt-3">
							<form action="{{ route('admin.filemanage.changeAll') }}" method="POST">
								{{ csrf_field() }}
								<input type="hidden" name="_action" value="delete">
								<input type="hidden" name="post_ids" value="" />
								<input type="hidden" name="redirect" value="{{ url()->full() }}" />
								
								<div class="modal fade" id="setting-post-del-modal" tabindex="-1" role="dialog" aria-hidden="true">
									<div class="modal-dialog" role="document">
										<div class="modal-content">
											<div class="modal-header">
												<h5 class="modal-title ml-auto mr-auto">
												ファイルを削除してもよろしいですか？</h5>
											</div>

											<div class="modal-body">
												<div class="list-file text-center">
												</div>
											</div>
											<div class="modal-footer">
												<button type="button" class="btn btn-secondary mr-3" data-dismiss="modal" style="width: 100px;margin-right: 25%;">キャンセル</button>
												<button type="submit" class="btn btn-dark" style="width: 100px;margin-right: 25%;">削除</button>
											</div>
										</div>
									</div>
								</div>
							</form>
						</div>
					</div>

				</div>
			</div>
		</div>
		<div class="container-file">
			<div class="row">
				@if(count($files))
				@foreach($files as $file)
				<!-- <div class="settingfile" id="dot_01" onclick="dot_01(this)" ondblclick="location.href='{{ route('admin.filebrowsing.index',$file) }}'">
				<div class="settingfile" ondblclick="location.href='{{ route('admin.filebrowsing.index',$file) }}'"> -->
				<div class="settingfile">
					<input type="checkbox" style="float:left;margin: 4% 0 0% 4%;transform: scale(1.5);" class="file-checkbox" data-file-id="{{ $file->id }}" data-file-name="{{ ($file->name) ? $file->name : '('.$file->id.')' }}" />					
					<a>
					@php ($format = $file -> name)
					@php ($format = strrchr( $format, '.'))
					@if ($format == '.xlsx' || $format == '.xls')	
						<img src="/image/icon/excel.png" class="icon-for-file"/>
					@elseif ($format == '.docx' || $format == '.doc')
						<img src="/image/icon/word.png" class="icon-for-file"/>
					@elseif ($format == '.pptx' || $format == '.ppt')
						<img src="/image/icon/powerpoint.png" class="icon-for-file"/>
					@elseif ($format == '.pdf')
						<img src="/image/icon/pdf.png" class="icon-for-file"/>
					@elseif ($format == '.csv')
						<img src="/image/icon/csv.png" class="icon-for-file"/>
					@elseif ($format == '.txt')
						<img src="/image/icon/txt.png" class="icon-for-file"/>
					@elseif ($format == '.sql')
						<img src="/image/icon/sql.png" class="icon-for-file"/>
					@else
						<img src="/image/icon/filenormal.png" class="icon-for-file"/>
					@endif
					<p>{{ $file -> name}}</p>
					</a>
				</div>
				@endforeach
				@endif

				@if(count($medias))
				@foreach($medias as $media)
				<div class="settingfile">
				<!-- <div class="media-ratio-library" style="background-image: url('{{ Helper::getMediaUrl($media, 'thumbnail') }}');"></div> -->
					<input type="checkbox" style="float:left;margin: 4% 0 0% 4%;transform: scale(1.5);" class="file-checkbox" data-file-id="{{ $media->id }}_i" data-file-name="{{ ($media->name) ? $media->name : '('.$media->id.')' }}" />	
					<img class="w-75 h-75" src="{{ Helper::getMediaUrl($media, 'thumbnail') }}" style="object-fit: contain; margin: 15px auto;">
					<p>{{ $media->name}}</p>
				</div>
				@endforeach
				@endif
			</div>
			@if(count($medias) == 0)
			<div class="d-flex" style="width: 100px; margin: 500px auto 0 auto;">
				<form action="{{ route('admin.user.index') }}" method="GET" autocomplete="off">
					<a href="{{ route('admin.user.index') }}" class="btn" style="text-align: center;margin-left: 0;width:100px;background:#F2F2F2 ;color: #D34725;border:1px solid #D34725">戻る
					</a>
				</form>	
			</div>	
			@elseif ((count($medias) > 0)  && (count($medias) < 5))
			<div class="d-flex" style="width: 100px; margin: 200px auto 0 auto;">
				<form action="{{ route('admin.user.index') }}" method="GET" autocomplete="off">
					<a href="{{ route('admin.user.index') }}" class="btn" style="text-align: center;margin-left: 0;width:100px;background:#F2F2F2 ;color: #D34725;border:1px solid #D34725">戻る
					</a>
				</form>	
			</div>	
			@elseif ((count($medias) > 4)  && (count($medias) < 11))
			<div class="d-flex" style="width: 100px; margin: 100px auto 0 auto;">
				<form action="{{ route('admin.user.index') }}" method="GET" autocomplete="off">
					<a href="{{ route('admin.user.index') }}" class="btn" style="text-align: center;margin-left: 0;width:100px;background:#F2F2F2 ;color: #D34725;border:1px solid #D34725">戻る
					</a>
				</form>	
			</div>	
			@elseif (count($medias) > 10)
			<div class="d-flex" style="width: 100px; margin: 10px auto 0 auto;">
				<form action="{{ route('admin.user.index') }}" method="GET" autocomplete="off">
					<a href="{{ route('admin.user.index') }}" class="btn" style="text-align: center;margin: 10px 0 0 0;width:100px;background:#F2F2F2 ;color: #D34725;border:1px solid #D34725">戻る
					</a>
				</form>	
			</div>	
			@endif
		</div>
		<div style="clear: both;"></div>
				
		<div class="pagination-menu d-flex" style="margin-top: 35px; float: right;">
			{{ $files->onEachSide(2)->links() }}
		</div>
	</section>
</div>
<!--<script>-->
<!--document.getElementById("dot_01").style.backgroundColor = '#F2F2F2';-->
<!--function dot_01(el) -->
<!--{-->
<!--	if (el.style.backgroundColor === 'red') {-->
<!--    	el.style.backgroundColor = '#F2F2F2';   -->
<!--  	}-->
<!--  	else el.style.backgroundColor = 'red';-->
<!--}-->
<!--</script>-->
<!---------- admin-edit main section ---------->
<!-- <script>
	$(function() {
	  var checkboxes = $("input[type='checkbox']"),submitButt = $("input[type='submit']");
	  enableSub(submitButt );
	  checkboxes.on("click",function() {
	    enableSub(submitButt );
	    if (this.checked) window.open('{site_url}downloads/resellers/Standard_Terms_and_Conditions.pdf');
	  });
	});
</script> -->
@endsection