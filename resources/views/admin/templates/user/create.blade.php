@extends('admin.layouts.app')

@section('content')
	@include('admin.parts.left-menu')

	<!-- <div class="admin-edit" style="background: #F2F2F2;width: 60%;margin-left:20%">
		<section class="main">			
			<div class="content-table-list">
				<table style="width: 50%;margin-left: 25%;margin-top: 20px">
					<tr>
						<td style="width: 20%">名称</td>
						<td><input type="text" name="" placeholder="平松" style="border: none; width: 100%;"></td>
					</tr>
					<tr>
						<td>ジャンル</td>
						<td>
							<select class="w-100 border-0">
								<option>⽇本</option>
							</select>
						</td>
					</tr>									
				</table>	
			</div>
			<button class="btn" style="background:#D34725 ; width: 105px; color: #ffffff;height:32px;border-radius:5px;font-size: 14px;margin-left:45%;margin-top: 5%">検索
			</button>
		</section>
	</div> -->
	<!---------- admin-edit main section ---------->
	<!-- <section class="main">
		<div class="change-avatar">
			<div>
				<h3>管理者情報</h3>
				<p>管理者情報を編集します。</p>
				@include('admin.parts.alert')
			</div>
		</div>		
		<div class="admin-edit-info" style="margin-top: 30px;">
			<form class="edit-info" action="{{ route('admin.user.store') }}" method="post" enctype="multipart/form-data">
				{{ csrf_field() }}
				<div class="form-group edit-info-email row" style="background: #f8f8f8; padding-top: 1rem;">
					<div class="label-admin-edit col-lg-2 col-md-3 col-12">
						<label>メールアドレス</label>
					</div>
					<div class="col-md-9 col-12">
						<input type="email" class="form-control" name="email" value="{{ old('email') }}" required="" />
						<div class="form-check">
							<input type="checkbox" class="form-check-input active" name="receive_notification" id="receive_notification" value="1" {{old('receive_notification') ? 'checked' : ''}} />
							<label class="form-check-label" for="receive_notification">メールで管理者通知を受け取る</label>
						</div>
					</div>
				</div>
				<div class="form-group edit-info-warning row">
					<div class="label-admin-edit col-lg-2 col-md-3 col-12">
						<label>パスワード</label>
					</div>
					<div class="col-md-9 col-12">
						<input type="password" class="form-control" name="password" value="{{ old('password') }}" required="" />
					</div>
				</div>
				<div class="form-group row" style="padding: 1rem 0 2.5rem 0; background: #f8f8f8; margin-top: 2rem;">
					<div class="label-admin-edit col-lg-2 col-md-3 col-12">
						<label>名前</label>
					</div>
					<div class="col-md-9 col-12" style="margin-bottom: 10px;">
						<input type="text" class="form-control" name="name" value="{{ old('name') }}" required=""/>
					</div>
				</div>
				<div class="form-group edit-info-sns row"style="margin-top:18px !important;">
					<div class="label-admin-edit col-lg-2 col-md-3 col-12">
						<label>アバター</label>
					</div>
					<div class="col-md-9 col-12" style="margin-top: 7px;">
						<img id="changeimg" src="{{ asset('image/no-img.png') }}" style="height: 116px;width: 116px; object-fit: cover;"><br>
						<div class="row" style="margin-top: 16px;">
								<input type="file" name="file" id="file" />
						</div>
					</div>
				</div>
				<div class="form-group row" style="background: #f8f8f8;">
					<div class="label-admin-edit col-lg-2 col-md-3 col-12" style="padding-top: 1rem;">
						<label>プロフィール</label>
					</div>
					<div class="col-md-9 col-12" style="padding-top: 1rem;">
						<textarea type="text" class="edit-your-info form-control" rows="8" name="writer_profile_html">{{ old('writer_profile_html') }}</textarea>
						<label class="text-info-txt row" style="margin-left: 0; padding: .5rem 0; background: #ffffff; width: 75%; margin-top: 18px;">
							<i class="fas fa-info-circle" style="margin-left: .5rem;"></i>メディアの<span style="color: #c43638;">ユーザーページ</span>に記載する紹介文です。HTMLが使用できます。
						</label>
					</div>
				</div>
				<div class="form-group row">
					<div class="label-admin-edit col-lg-2 col-md-3 col-12">
						<label style="margin-top: 8px;">権限</label>
					</div>
					<div class="col-md-9 col-12">
						<select name="role_id" style=" width: 215px;" required="">
							<option value="">権限</option>
							@if(count($roles))
								@foreach($roles as $role)
									<option value="{{ $role->id }}" {{ old('role_id') == $role->id ? 'selected' : '' }}>{{ $role->name }}</option>
								@endforeach
							@endif
						</select>
						
						<label class="text-info-txt row" style="margin-left: 17px">
							<i class="fas fa-info-circle"></i>「記事の公開を不可にする」をチェックすると、ユーザーは下きき状態の記事だ<br>けを扱える状態になります。<br>
							この設定はユーザーの権限が「記事作成者」の場合にのみ指定可能です。
						</label>
					</div>
				</div>
				<div style="margin-top: 70px;">
					<a href="{{ route('admin.user.index') }}" class="btn-back btn btn-no-radius" style="font-size: 12px; width: 100px; line-height: 28px;">
						<i class="fas fa-arrow-left"></i>
						戻る
					</a>
					<button class="btn-submit btn btn-no-radius" style="font-size: 12px; width: 100px;">更新</button>
				</div>
			</form>
		</div>
	</section> -->
	<!-----x----- admin-edit main section -----x----->
@endsection