@extends('admin.layouts.app')

@section('content')
	@include('admin.parts.left-menu')
	<!---------- admin-edit main section ---------->
	<section class="main-writer" >
		<div class="list-of-content-title" style="margin-top: 30px;margin-bottom: 50px;">
			<h3 class="font-weight-bold">公式ライター管理</h3>
			<p class="font-weight-bold">メディアの公式ライターを管理します。</p>
			@include('admin.parts.alert')
		</div>
		<div class="wirter-management-table table-responsive-md">
			<table class="table" style="width:100%;background-color: #DDDDDD">
				<tr>
					<th style="width: 13%">表示順</th>
					<th style="width: 47%">名前</th>
					<th></th>
				</tr>
				@if(count($users))
					@foreach($users as $user)
						<tr>
							<td>{{ $user->id }}</td>
							<td>{{ $user->name }}</td>
							<td></td>
						</tr>
					@endforeach
				@endif
			</table>
		</div>
		<div>
			<div class="list-wirter-management d-flex">
				<div>
					<i class="fas fa-info-circle" style="color:#98908A;"></i>
				</div>
				<div style="margin-left: 10px;">
					<p>ライターをメディアの「公式ライター」として登録しておくと、以下の内容が適用されます。</p>
					<ul>
						<li>
							メディアの「<span>公式ライター一覧</span>」ベージに、ライターとして表示されます。
						</li>
						<li>「公式ライター」として登録されているライターの作成した記事には、自動的に「<span>優先コンテンツ</span>」の設定が適用されます。これにより公開後24時間はトップページに優先表示されます。
						</li>
					</ul>
					<p>
						公式ライターを追加する場合め手順は、以下のとおりです。
					</p>
					<ul>
						<li>「<span>コンテンツー院</span>」にアクセスします。</li>
						<li>公式ライター設定を変更したいライターの執筆した記事の、ライター名をクリックします。表示されたメーニューから「公式ライターにする」をクリックします。</li>
					</ul>
				</div>
			</div>
		</div>
	</section>
	<!---------- admin-edit main section ---------->
@endsection