
<form method="post" action="{{ route('admin.user.store') }}" class="modal fade" id="create-user" tabindex="-1" role="dialog" aria-labelledby="ModalCreatLabel" aria-hidden="true" enctype="multipart/form-data">
	{{ csrf_field() }}
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="ModalCreatLabel" style="margin-left: 30%">ユーザー新規登録</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span style="font-size: 30px;" aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<table class="w-100">
					<tr>
						<th>ユーザー名</th>
						<td><input style="height:40px;" class="w-100 border-0" type="text" name="name" placeholder="ユーザー名"></td>
					</tr>
					<tr>
						<th>ジャンル</th>
						<td>
							<select class="w-100 border-0" name="genre">
								<!-- <option value="">⽇本</option> -->
								@if(isset($genres) && count($genres))
								@foreach($genres as $genre)
								<option value="{{ $genre->id }}" {{ (request()->genre == $genre->id) ? 'selected' : '' }}>{{ $genre->name }}</option>
								@endforeach
								@endif
							</select>
						</td>
					</tr>								
				</table>
			</div>
			<div class="modal-footer">
				<button class="btn" style="background:#D34725;width:105px;color:#ffffff;height:32px;border-radius:5px;font-size:14px;margin:5% 40% 5% 0;">登録
				</button>
			</div>
		</div>
	</div>
</form>