<form method="post" action="{{ route('admin.user.update', $user) }}" class="modal fade" id="edit-user-{{ $user->id }}" tabindex="-1" role="dialog" aria-labelledby="ModalUpdateLabel" aria-hidden="true" enctype="multipart/form-data">
	{{ csrf_field() }}
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="ModalUpdateLabel" style="margin-left: 30%">ユーザー情報編集</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span style="font-size: 30px;" aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<table class="h-50">
					<tr>
						<th>ユーザー名</th>
						<td><input style="height:40px;" class="w-100 border-0" type="text" name="name" placeholder="{{ $user->name }}"></td>
					</tr>
					<tr>
						<th>ジャンル</th>
						<td>
							<select class="w-100 border-0" name="genre">
								<!-- <option value="">⽇本</option> -->
								@if(isset($genres) && count($genres))
								@foreach($genres as $genre)
								<option value="{{ $genre->id }}" {{ (old('genre', $user->genre) == $genre->id ) ? 'selected' : '' }}>{{ $genre->name }}</option>
								@endforeach
								@endif
							</select>
						</td>
					</tr>
					<tr style="height:65px;">
						<th>ID</th>
						<td>{{ $user->id }}</td>
					</tr>
					<tr style="height:65px;">
						<th>PASS</th>
						<td>{{ base64_decode($user->password_end) }}</td>
					</tr>							
				</table>
			</div>
			<div class="modal-footer">
				<button class="btn" style="background:#D34725 ; width: 105px; color: #ffffff;height:32px;border-radius:5px;font-size: 14px;margin:5% 40% 5% 0;">保存
				</button>
			</div>
		</div>
	</div>
</form>