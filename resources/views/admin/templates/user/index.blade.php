@extends('admin.layouts.app')

@section('content')
	@include('admin.parts.left-menu')

	<div class="admin-edit">
		<section class="main">
			<div class="col-12">
				<div class="row">
					<div style="margin: .4rem 0; width: 100%">
						<form action="{{ route('admin.user.index') }}" method="GET" autocomplete="off">
						    	
							<div class="dropdown" style="margin-right: .5rem;margin-top: 8px;">
							<input type="text" class="form-control" placeholder="ユーザー名" name="username" value="{{ request()->username }}" style="display: inline-block; height: 35px; width:15%;border-radius: 0;">
							<!-- <i class="fas fa-user-alt" style="font-size:30px;float: right;"></i> -->
								<select style="width:15%; text-align: left;" name="genre">ジャンル
									<option value="">ジャンル</option>
									@if(isset($genres) && count($genres))
										@foreach($genres as $genre)
										<option value="{{ $genre->id }}" {{ (request()->genre == $genre->id) ? 'selected' : '' }}>{{ $genre->name }}</option>
										@endforeach
									@endif
								</select>
							<!--</div>
							<div class="d-flex" style="margin-left: 0px;margin-top: 8px;">-->
								<!--<input type="text" class="datepicker form-control" placeholder="YYYY-MM-DD" name="from" value="{{ request()->from }}" style="display: inline-block; height: 35px; width:15%;border-radius: 0;">
								<input type="text" class="datepicker form-control" placeholder="YYYY-MM-DD" name="to" value="{{ request()->to }}" style="display: inline-block; height: 35px; width:15%;margin-left: 10px;border-radius: 0;">-->	
								<button class="btn" style="background:#D34725 ;color: #ffffff;margin-left:25px;width: 100px">検索
								</button>				
							</div>
							<!--<div style="margin-top:1%;margin-bottom: 2%">
								<input type="radio" id="create" class="checkbox" name="date" value="create" {{ (request()->date == 'create' ) ? 'checked' : '' }}>
								<label for="checkbox" style="color:#3D507A;">登録⽇</label>
								<input type="radio" id="update" class="checkbox" name="date" value="update" style="margin-left:45px" {{ (request()->date == 'update') ? 'checked' : '' }}>
								<label for="checkbox" style="color:#3D507A;">最新送信⽇</label>
							</div>-->
						</form>
						<!-- <form action="{{ route('admin.chatmanage.index') }}" method="GET" autocomplete="off"> -->
						<a href="{{ route('admin.chatmanage.index') }}" class="btn" style="width:100px;background:#F2F2F2 ;color: #D34725; border:1px solid #D34725;float: right;">チャット</a>
						<!-- {{csrf_field()}}
						</form> -->	
						<a href="#" data-toggle="modal" data-target="#create-user">
							<button class="btn" style="background:#D34725; color: #ffffff;float: right;margin-right:2%;width: 100px">新規
							</button>
						</a>

						@include('admin.templates.user.parts.modal-create')

					</div>
				</div>
			</div>
			
			@include('admin.templates.user.parts.modal-confirm')

			@include('admin.parts.alert')
			
			<div class="content-table-list">
				<table>
					<thead>
						<tr>
							<th>No.</th>
							<th>ユーザー名</th>
							<th>ジャンル</th>
							<!-- <th>メモ</th> -->
							<th>ID</th>
							<th>PASS</th>
							<th>登録⽇@sortablelink('created_at','')</th>
							<th>最新送信⽇@sortablelink('updated_at','')</th>
							<th></th>
						</tr>
					</thead>
					<tbody>
						@if(count($users))
						@php ($no = 1)
						@foreach($users as $user)
							@if($user->id != '10001')
							<tr>
								<td>{{ $no }}</td>
								<td>{{ $user->name }}</td>
								<td>{{ $genres[$user->genre-1]->name }}</td>
								<!-- <td>....</td> -->
								<td>{{ $user->id }}</td>
								<td style="width: 150px;"><input disabled type="password" style="overflow:hidden;white-space:nowrap;text-overflow:Ellipsis; width: 100%; border: none;" value="{{ $user->password }}"></td>
								<td>{{ $user->created_at->format('Y/m/d') }}</td>
								<td>{{ $user->updated_at->format('Y/m/d') }}</td>
								<td>
									<a style="margin-left: 8%;" href="{{ route('admin.user.update', $user) }}"  data-toggle="modal" data-target="#edit-user-{{ $user->id }}"><i class="fa fa-pencil"></i></a>
									
									<!-- MODAL EDIT USER -->
									
									@include('admin.templates.user.parts.modal-update')

									<a style="margin: 0 9% 0 9%;" href="{{ route('admin.filemanage.index',$user) }}"><i class="fa fa-folder" style="color: red"></i></a>
									<a href="{{ route('admin.user.destroy',$user) }}" data-toggle="modal" data-target="#del-modal-{{ $user->id }}"><i class="far fa-trash-alt"></i></a>
									
									<!-- MODAL DELETE USER -->

									<div class="modal fade" id="del-modal-{{ $user->id }}" tabindex="-1" role="dialog" aria-labelledby="ModalDelLabel" aria-hidden="true">
										<div class="modal-dialog" role="document">
											<div class="modal-content">
												<div class="modal-header">
													<h5 class="modal-title" id="ModalDelLabel"  style="margin-left: 10%">ユーザーを削除してもよろしいですか？</h5>
													<button type="button" class="close" data-dismiss="modal" aria-label="Close">
														<span style="font-size: 30px;" aria-hidden="true">&times;</span>
													</button>
												</div>
												<div class="modal-body" style="text-align: center;">
													{{ $user->name }}
												</div>
												<div class="modal-footer">
													<button style="margin-right: 5%" type="button" class="btn btn-secondary" data-dismiss="modal">閉じる</button>
													<a style="margin-right: 30%" href="{{ route('admin.user.destroy', $user) }}"><button type="button" class="btn btn-primary">削除</button></a>
												</div>
											</div>
										</div>
									</div>
								</td>
							</tr>
							@php ($no++)
							@endif
						@endforeach
						@endif
					</tbody>									
				</table>

			<div class="pagination-menu d-flex" style="margin-top: 10px; float: right;">
				{{ $users->onEachSide(2)->links() }}
			</div>
		</section>
	</div>
<script type="text/javascript">
	// $(document).ready(function(){
	//     $('.alert').addClass('notify-delete');
	// });
 	//$('.alert').addClass('notify-delete');
</script>
@endsection
