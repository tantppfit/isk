<footer>
	<div class="container">
		@if(isset($settings['footer_menu_appearance']) && ($settings['footer_menu_appearance'] == 'show'))
		  <div class="footer-content row" style="padding-top: 4rem;">
		    <div class="col-md-6 col-12 my-auto">
		      @if((new \Jenssegers\Agent\Agent())->isMobile() && isset($settings['general_logo_mobile']) && !empty($settings['general_logo_mobile']))
						<img src="{{ $settings['general_logo_mobile'] }}" style="max-width: 240px; max-height: 64px;">
					@elseif(isset($settings['general_logo']) && !empty($settings['general_logo']))
						<img src="{{ $settings['general_logo'] }}" style="max-width: 240px; max-height: 64px;">
					@else
						<img src="{{ asset('image/isk_logo.png') }}" style="max-width: 240px; max-height: 64px;">
					@endif
		    </div>
		    <div class="col-md-3 col-12">
		      <div class="section-menu-footer">
		        <ul class="menu-footer row">
		        	@if(count($footerMenus1))
						@foreach($footerMenus1 as $menuItem)
							<li class="menu-footer-item col-md-12 col-6"><a href="{{ $menuItem->url }}" class="menu-footer-link">{{ $menuItem->name }}</a></li>
						@endforeach
					@endif
		        </ul>
		      </div>
		    </div>
		    <div class="col-md-3 col-12">
		      <div class="section-footer-list">
		        <ul class="footer-list row">
	        		@if(count($footerMenus2))
			          <li class="footer-list-item col-12">
			            <div style="color: #fff; border-left: 2px solid #fff; padding-left: .5rem;">連載</div>
			          </li>
								@foreach($footerMenus2 as $menuItem)
									<li class="footer-list-item col-12"><a href="{{ $menuItem->url }}" class="footer-list-link">{{ $menuItem->name }}</a></li>
								@endforeach
							@endif
		        </ul>
		      </div>
		    </div>
		  </div>
		@endif
	</div>
	
	<div class="container" style="padding: 0; max-width: 100%;">
		<div class="col-12 d-md-none" style="padding-left: 0; padding-right: 0;">
			<div class="text-center" id="page-top">
				Page top
				<i class="fa fa-caret-up" aria-hidden="true"></i>
			</div>
		</div>
	</div>
	<div class="container">
		@if(isset($settings['footer_banner_appearance']) && ($settings['footer_banner_appearance'] == 'show'))
			<div class="footer-logo">
				@if(isset($settings['footer_banner_image_1']) && !empty($settings['footer_banner_image_1']))
			  	<a href="{{ (isset($settings['footer_banner_link_1']) && !empty($settings['footer_banner_link_1'])) ? $settings['footer_banner_link_1'] : '' }}"><img src="{{ $settings['footer_banner_image_1'] }}"></a>
			  @endif
				@if(isset($settings['footer_banner_image_2']) && !empty($settings['footer_banner_image_2']))
			  	<a href="{{ (isset($settings['footer_banner_link_2']) && !empty($settings['footer_banner_link_2'])) ? $settings['footer_banner_link_2'] : '' }}"><img src="{{ $settings['footer_banner_image_2'] }}"></a>
			  @endif
			</div>
		@endif
	</div>

	<div style="padding-top: 4rem;"></div>
	<div class="footer-line"></div>

	<div class="text-center" style="padding: 1.25rem 0;">
		<label class="footer-link-menu" style="color: #fff;">
			@if(count($footerMenus3))
				@foreach($footerMenus3 as $menuItem)
					<a href="{{ $menuItem->url }}" style="color: #fff;">{{ $menuItem->name }}</a>{{ (!$loop->last) ? ' | ' : ''}}
				@endforeach
			@endif
		</label>
	</div>

	<div class="footer-bottom text-center" style="font-size: 16px;">
		<label class="text-copy-right">&copy; {{date("Y")}}-{{ isset($settings['general_copyright']) ? $settings['general_copyright'] : '' }} All right reserved</label>
	</div>
</footer>