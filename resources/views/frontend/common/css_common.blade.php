<!-- bootstrap -->
<link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Lobster+Two">
<link rel="stylesheet" type="text/css" href="{{ asset('css/frontend/style.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('css/frontend/flexslider/flexslider.css') }}">
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
<link rel="stylesheet" type="text/css" href="{{ asset('css/frontend/custom.css') }}">