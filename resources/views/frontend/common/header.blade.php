<header id="main-header">
	<div class="top-header"></div>
	
	<div class="container">
		<div class="logo">
			<div>
				<a class="main-logo" href="{{ route('frontend.home.index') }}">
					@if((new \Jenssegers\Agent\Agent())->isMobile() && isset($settings['general_logo_mobile']) && !empty($settings['general_logo_mobile']))
						<img src="{{ $settings['general_logo_mobile'] }}">
					@elseif(isset($settings['general_logo']) && !empty($settings['general_logo']))
						<img src="{{ $settings['general_logo'] }}">
					@else
						<img src="{{ asset('image/isk_logo.png') }}">
					@endif
				</a>
			</div>

			<div class="social">
				<a href="https://www.facebook.com/adivajapan"><i class="fab fa-facebook-f"></i></a>
				<a href="https://twitter.com/adivajapan"><i class="fab fa-twitter"></i></a>
				<a href="https://www.instagram.com/adivajapan/"><i class="fab fa-instagram"></i></a>
				<a href="{{ route('frontend.search.index') }}"><i class="fas fa-search"></i></a>
			</div>
		</div>

		<nav class="nav-menu navbar navbar-expand-lg" style="font-size: 14px;">
			<button class="navbar-toggler navbar-show-menu" id="menu-bar" onclick="changeHambugerBar()" type="button" data-toggle="collapse" data-target="#navbar-main"
				aria-controls="navbar-main" aria-expanded="false" aria-label="Toggle navigation">
				<!-- <span><i class="fas fa-bars"></i></span> -->
				<div id="bar1" class="bar"></div>
				<div id="bar2" class="bar"></div>
				<div id="bar3" class="bar"></div>
			</button>
		
			<div class="collapse navbar-collapse justify-content-center" id="navbar-main">
				<ul class="navbar-nav">
					<!-- <li class="nav-item d-lg-none">
						<a href="#" class="nav-link navbar-toggler navbar-hide-menu" data-toggle="collapse" data-target="#navbar-main"
							aria-controls="navbar-main" aria-expanded="false" aria-label="Toggle navigation">
							<span><i class="fas fa-times"></i></span>
						</a>
					</li> -->
					<li class="nav-item d-lg-none">
						<a href="{{ route('frontend.home.index') }}" class="nav-link">トップ</a>
					</li>
					@if(count($menuTags))
						@foreach($menuTags as $menuTag)
							<li class="nav-item {{ request()->is('tag/'.$menuTag->tag->id) ? 'active' : '' }}">
								<a href="{{ route('frontend.tag.show', $menuTag->tag) }}" class="nav-link">{{ $menuTag->tag->name }}</a>
							</li>
						@endforeach
					@endif
					<li class="nav-item d-lg-none">
						<!-- <a href="{{ route('frontend.page.show', 'inquiry') }}" class="nav-link">お問い合わせ</a> -->
						<a href="https://aidea.net/contact" class="nav-link">お問い合わせ</a>
					</li>
					<li class="nav-item d-lg-none">
						<a href="{{ route('frontend.page.show', 'terms-of-service') }}" class="nav-link">利用規約</a>
					</li>
					<li class="nav-item d-lg-none">
						<a href="{{ route('frontend.page.show', 'privacy-policy') }}" class="nav-link">プライバシーポリシー</a>
					</li>
					<li class="nav-item d-lg-none">
						<a href="#" class="nav-link">運営会社</a>
					</li>
				</ul>
			</div>
		</nav>
	</div>
</header>