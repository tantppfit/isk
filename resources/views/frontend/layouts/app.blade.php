<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<!-- CSRF Token -->
  <meta name="csrf-token" content="{{ csrf_token() }}">
	<title>{{ isset($metaTitle) ? $metaTitle . ' | ' : '' }}{{ isset($settings['general_seo_title']) ? $settings['general_seo_title'] : '' }}</title>
	<meta name="description" content="{{ isset($metaDescription) && !empty($metaDescription) ? $metaDescription : (isset($settings['general_seo_description']) ? $settings['general_seo_description'] : '') }}">
  <meta name="keywords" content="{{ isset($settings['general_seo_keywords']) ? $settings['general_seo_keywords'] : '' }}">
  @if(isset($settings['general_favicon']) && !empty($settings['general_favicon']))
  	<link rel="icon" href="{{ Helper::getMediaUrlById($settings['general_favicon'], 'thumbnail') }}" />
  @endif
	<link rel="canonical" href="{{url()->current()}}" />
	<meta property="og:type" content="website" />
	<meta property="og:title" content="{{ isset($metaTitle) ? $metaTitle . ' | ' : '' }}{{ isset($settings['general_seo_title']) ? $settings['general_seo_title'] : '' }}" />
	<meta property="og:description" content="{{ isset($metaDescription) && !empty($metaDescription) ? $metaDescription : (isset($settings['general_seo_description']) ? $settings['general_seo_description'] : '') }}" />
	<meta property="og:url" content="{{url()->current()}}" />
	<meta property="og:site_name" content="{{ isset($settings['general_seo_title']) ? $settings['general_seo_title'] : '' }}" />
	@if(isset($metaImage) && !empty($metaImage))
		<meta property="og:image" content="{{ $metaImage }}" />
		<meta property="og:image:secure_url" content="{{ $metaImage }}" />
		<meta name="twitter:image" content="{{ $metaImage }}" />
	@elseif(isset($settings['general_seo_image']) && !empty($settings['general_seo_image']))
		<meta property="og:image" content="{{ $settings['general_seo_image'] }}" />
		<meta property="og:image:secure_url" content="{{ $settings['general_seo_image'] }}" />
		<meta name="twitter:image" content="{{ $settings['general_seo_image'] }}" />
	@endif
	<meta name="twitter:card" content="summary_large_image" />
	<meta name="twitter:title" content="{{ isset($metaTitle) ? $metaTitle . ' | ' : '' }}{{ isset($settings['general_seo_title']) ? $settings['general_seo_title'] : '' }}" />
	<meta name="twitter:description" content="{{ isset($metaDescription) && !empty($metaDescription) ? $metaDescription : (isset($settings['general_seo_description']) ? $settings['general_seo_description'] : '') }}" />
	@if(isset($settings['general_fb_id']) && !empty($settings['general_fb_id']))
		<meta property="fb:app_id" content="{{ $settings['general_fb_id'] }}" />
	@endif
	@include('frontend.common.css_common')
	@yield('header_css')
	@if(isset($settings['stylesheet']) && !empty($settings['stylesheet']))
		<style>
			{!! $settings['stylesheet'] !!}
		</style>
	@endif
</head>
<body>
	@include('frontend.common.header')
	@yield('content')	
	@include('frontend.common.footer')
	@include('frontend.common.js_common')
	@yield('footer_js')
</body>
</html>