<div class="category-content-right col-lg-4 col-md-4 col-12">

	<!-- feature -->
	<h2 class="font-weight-bold">Feature <small style="font-size: 50%;" class="d-block d-md-inline-block font-weight-bold">特集</small></h2>
	
	<div class="category-content-right-feature-list">
		@if($featureCategories && count($featureCategories))
			@foreach($featureCategories as $featureCategory)
				<!-- <a href="{{ route('frontend.category.show', $featureCategory) }}"><div class="category-content-right-feature-item" style="background: url('{{ $featureCategory->sub_featured_image }}'); background-size: contain !important; background-repeat: no-repeat;">
					<img src="{{ $featureCategory->sub_featured_image }}">
				</div></a> -->
				<a href="{{ route('frontend.category.show', $featureCategory) }}">
					<img src="{{ $featureCategory->sub_featured_image }}" style="margin: .5rem 0;">
				</a>
			@endforeach
		@endif

		<div class="d-flex flex-row-reverse">
			<a class="right-feature-link" href="{{ route('frontend.page.show', 'feature') }}" style="font-weight: 600;">特集一覧<i class="fas fa-chevron-right"></i></a>
		</div>
	</div>

	<!---x--- feature ---x--->
	
	<div class="line"></div>

	<!-- rank -->
	<h2 class="font-weight-bold">Ranking <small style="font-size: 50%;" class="d-block d-md-inline-block font-weight-bold">ランキング</small></h2>

	<div class="category-content-right-rank-list">
		@if(isset($topPosts) && count($topPosts))
			@foreach($topPosts as $topPost)
				<div class="category-content-right-rank-item">
					<div>
						<div class="rank-item-img-wrap" style="background-image: url('{{ ($topPost->featureImage) ? Helper::getMediaUrl($topPost->featureImage, 'thumbnail') : Helper::getDefaultCover($topPost) }}');">
							<a href="{{ route('frontend.post.show', $topPost->slug) }}">
								@if($topPost->featureImage)
									<img src="{{ Helper::getMediaUrl($topPost->featureImage, 'thumbnail') }}" />
								@else
									<img src="{{ Helper::getDefaultCover($topPost) }}">
								@endif
							</a>
						</div>
						<div class="category-content-right-rank-item-number">
							<span>{{ $loop->index+1 }}</span>
						</div>
					</div>
					<a href="{{ route('frontend.post.show', $topPost->slug) }}"><p>{{ $topPost->title }}</p></a>
				</div>
			@endforeach
		@endif
	</div>

	<!---x--- rank ---x--->

	<div class="line"></div>

	<!-- series -->
	<h2 class="font-weight-bold">Series <small style="font-size: 50%;" class="d-block d-md-inline-block font-weight-bold">連載</small></h2>

	<div class="category-content-right-series">
		@if($seriesCategories && count($seriesCategories))
			@foreach($seriesCategories as $seriesCategory)
				<div class="category-content-right-series-item">
				<i class="fas fa-tag"></i>
					<div>
						<p class="w-100 h-100 mb-0"><a href="{{ route('frontend.category.show', $seriesCategory) }}">{{ $seriesCategory->name }}</a></p>
					</div>

					<a href="{{ route('frontend.category.show', $seriesCategory) }}"><div class="series-item-img-wrap" style="background-image: url('{{ $seriesCategory->featured_image }}');">
						<img src="{{ $seriesCategory->featured_image }}">
					</div></a>
				</div>
			@endforeach
		@endif
		
		<div class="d-flex flex-row-reverse">
			<a class="right-rank-link" href="{{ route('frontend.page.show', 'series') }}" style="font-weight: 600;">連載一覧<i class="fas fa-chevron-right"></i></a>
		</div>							
	</div>

	<!---x--- series ---x--->

	<div class="line"></div>

	<!-- follow -->
	<h2 class="font-weight-bold">Follow me!</h2>

	<div class="category-content-right-follow row">
		@if(isset($instaFeeds) && is_array($instaFeeds) && count($instaFeeds))
			<div class="row">
				@foreach($instaFeeds as $instaFeed)
					<div class="category-content-right-follow-item">
						<a href="{{ $instaFeed['link'] }}" target="_blank">
							<img src="{{ $instaFeed['images']['low_resolution']['url'] }}">
						</a>
					</div>
				@endforeach
			</div>
		@endif

		<div class="follow-me" style="border: 1px solid #111; font-size: 14px; width: 100%; margin-left: 15px; padding: 20px 35px;">
			<a class="d-flex w-100" href="https://www.instagram.com/adivajapan/" target="_blank">
				<img src="{{ asset('image/icon/insta-icon.png') }}" style="width: 60px; height: 60px;">

				<div class="text-center" style="width: calc(100% - 60px);">
					<h4>Follow me!</h4>
					<h4>@adivajapan</h4>
				</div>
			</a>
		</div>
	</div>

	<!---x--- follow ---x--->

</div>