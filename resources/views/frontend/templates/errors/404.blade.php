@extends('frontend.layouts.app')
@section('content')
	<section class="main">
		<!-- main privacy policy page content -->
		<div class="page-404">
			<div class="container">
				<h4>お探しの記事は掲載が終了しました。よろしければ他の記事をご覧ください。</h4>
				<a href="{{ route('frontend.home.index') }}" style="color:black ;font-family:Herlvetica Neue', Helvetica, Arial, 'Hiragino Kaku Gothic ProN', Meiryo, 'MS PGothic', sans-serif !important;">AIDEA STYLEトップへ行きます。</a>		
			</div>
		</div>

	</section>
@endsection
@section('footer_js')
	<script text="">
		window.setTimeout(function() {
			location.href = '{{ route('frontend.home.index') }}';
		}, 10000);
	</script>
@endsection
