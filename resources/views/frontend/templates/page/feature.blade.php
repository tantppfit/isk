@extends('frontend.layouts.app')
@section('content')
	<section class="main">

		<!-- main privacy policy page content -->

		<div class="main-normal-page">
			<div class="container">
				<div class="row page-title">
				@if($page->featureImage)
					<div class="col-md-6 no-padding margin-20 img-preview">
						<img src="{{ Helper::getMediaUrl($page->featureImage, 'original') }}" alt="{{ $page->title }}">
					</div>
					<div class="col-md-6">
						<h3 class="midle-postion text-center text-md-left tag-post-title">{{ $page->title }}</h3>
					</div>
				@else
					<div class="col-md-12">
						<h3 class="tag-post-title">{{ $page->title }}</h3>
					</div>
				@endif
				</div>
			
				<div class="line"></div>

				<div class="row">
					
					<!-- left feature list page content -->

					<div class="left-feature-list-page col-lg-8 col-md-8 col-12">
						<div class="feature-list-page row">
							@if(count($categories))
								@foreach($categories as $category)
									<div class="feature-list-page-item col-md-6 col-12">
										<a href="{{ route('frontend.category.show', $category) }}">
											<div class="feature-list-page-item-content">
												<img src="{{ $category->featured_image }}" class="w-100">
											</div>
										</a>
									</div>
								@endforeach
							@endif
						</div>
					</div>

					<!--x-- left feature list page content --x-->

					<!-- right category content -->

					@include('frontend.parts.right_sidebar')

					<!--x-- right category content --x-->

				</div>
			</div>
		</div>

		<!--x-- main privacy policy page content --x-->

	</section>
@endsection