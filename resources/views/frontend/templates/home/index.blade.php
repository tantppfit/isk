@extends('frontend.layouts.app')
@section('content')
	<section class="main" id="#main">

		<div class="main-content">
			<div class="container">
				@if(count($sliderPosts))
					<div class="bd-example">
						<div id="carouselExampleCaptions" class="carousel slide" data-ride="carousel">
							<ol class="carousel-indicators">
								@foreach($sliderPosts as $sliderPost)
									<li data-target="#carouselExampleCaptions" data-slide-to="{{ $loop->index }}" class="{{ $loop->first ? 'active' : '' }}"><span>{{ $loop->index+1 }}</span></li>
								@endforeach
							</ol>
							<div class="carousel-inner">
								@foreach($sliderPosts as $sliderPost)
									<div class="carousel-item {{ $loop->first ? 'active' : '' }}">
										@if($sliderPost->featureImage)
											@if((new \Jenssegers\Agent\Agent())->isMobile())
												<img src="{{ Helper::getMediaUrl($sliderPost->featureImage, 'ratio4x3') }}" class="d-block w-100" alt="{{ $sliderPost->title }}">
											@else
												<img src="{{ Helper::getMediaUrl($sliderPost->featureImage, 'ratio3x2') }}" class="d-block w-100" alt="{{ $sliderPost->title }}">
											@endif
										@else
											<img src="{{ Helper::getDefaultCover($sliderPost) }}" class="d-block w-100" alt="{{ $sliderPost->title }}">
										@endif
										<a href="{{ route('frontend.post.show', $sliderPost->slug) }}"><div class="carousel-caption d-block">
											<h5>{{ $sliderPost->title }}</h5>
								        </div></a>
									</div>
								@endforeach
							</div>

							<a class="carousel-control-prev" href="#carouselExampleCaptions" role="button" data-slide="prev">
								<div>
									<label>PREV</label>
								</div>
							</a>

							<a class="carousel-control-next" href="#carouselExampleCaptions" role="button" data-slide="next">
								<div>
									<label>NEXT</label>
								</div>
							</a>
						</div>
					</div>
				@endif
				<div class="new-content row">
					@if((new \Jenssegers\Agent\Agent())->isDesktop())
						@php
							$posts = $postsPC;
						@endphp
					@else
						@php
							$posts = $postsSP;
						@endphp
					@endif
					@if(count($posts))
						@foreach($posts as $post)
							<div class="card-content col-lg-4 col-6">
								<div class="card-new-content">
									<a href="{{ route('frontend.post.show', $post->slug) }}"><div class="card-new-content-img img-wrap" style="background: url('{{ ($post->featureImage) ? Helper::getMediaUrl($post->featureImage, 'medium') : Helper::getDefaultCover($post) }}');">
										@if($post->featureImage)
											<img src="{{ Helper::getMediaUrl($post->featureImage, 'medium') }}" />
										@else
											<img src="{{ Helper::getDefaultCover($post) }}">
										@endif
									</div></a>

									<div class="post-man d-flex justify-content-between">
										<label class="posted-date d-none d-md-block">{{ date('Y-m-d', strtotime($post->created_at)) }}</label>
										<label class="card-category"><a href="{{ route('frontend.user.show', $post->user) }}">{{ $post->user->name }}</a></label>
									</div>
									<div class="artical-title">
										<a href="{{ route('frontend.post.show', $post->slug) }}">{{ $post->title }}</a>
									</div>
								</div>
							</div>
						@endforeach
					@endif
				</div>
				<div class="row">
					{{ $posts->appends(request()->query())->links('frontend.parts.pagination') }}
				</div>
			</div>
		</div>
	</section>
	<section class="feature">
		<div class="container">
			<h2 class="font-weight-bold">Feature <small class="d-block d-md-inline-block font-weight-bold" style="font-size: 14px;">特集</small></h2>

			<div class="feature-content row">
				@if($featureCategories && count($featureCategories))
					@foreach($featureCategories as $featureCategory)
						<div class="card-feature col-lg-4 col-md-6 col-12">
							<a href="{{ route('frontend.category.show', $featureCategory) }}">
								<img src="{{ $featureCategory->featured_image }}">
							</a>
						</div>
					@endforeach
				@endif
			</div>

			<label class="float-right font-weight-bold"><a href="{{ route('frontend.page.show', 'feature') }}" style="color: #111;">連載一覧 ></a></label>
		</div>
	</section>
		
	<section class="rank">
		<div class="container">
			<h2 class="font-weight-bold">Ranking <small class="d-block d-md-inline-block font-weight-bold" style="font-size: 14px;">ランキング</small></h2>

			<div class="rank-content row">
				@if(isset($topPosts) && count($topPosts))
					@foreach($topPosts as $topPost)
					<a href="{{ route('frontend.post.show', $topPost->slug) }}" class="card-ranking-content">
						<div class="card-ranking">
							<div class="ranking-number">
								<h3 class="font-weight-bold">
									<span>{{ $loop->index+1 }}</span>
								</h3>
							</div>
							
							<div class="d-flex d-sm-block">
								<div class="card-ranking-img">
									@if($topPost->featureImage)
										<img src="{{ Helper::getMediaUrl($topPost->featureImage, 'medium') }}" />
									@else
										<img src="{{ Helper::getDefaultCover($topPost) }}">
									@endif
								</div>
								
								<div class="card-ranking-txt">
									<label>{{ $topPost->title }}</label>
								</div>
							</div>
						</div>
					</a>
					@endforeach
				@endif
			</div>
		</div>
	</section>

	<section class="series">
		<div class="container">
			<h2 class="font-weight-bold text-center text-lg-left">Series <small class="d-block d-md-inline-block font-weight-bold" style="font-size: 14px;">連載</small></h2>

			<div class="series-content row">
				@if($seriesCategories && count($seriesCategories))
					@foreach($seriesCategories as $seriesCategory)
						<div class="series-card-content col-lg-4 col-md-6 col-12">
							<a href="{{ route('frontend.category.show', $seriesCategory) }}">
								<div class="text-center">
									<label class="mb-0"><i class="fas fa-tag"></i>{{ $seriesCategory->name }}</label>
									<img class="w-100" src="{{ $seriesCategory->featured_image }}">
								</div>
							</a>
						</div>
					@endforeach
				@endif
			</div>

			<label class="float-right font-weight-bold"><a href="{{ route('frontend.page.show', 'series') }}" style="color: #111;">連載一覧 ></a></label>
		</div>
	</section>
	
	<section class="follow">
		<div class="text-center">
			<h2 class="font-weight-bold">Follow me!</h2>
			<a href="https://www.instagram.com/adivajapan/"><i class="fab fa-instagram"></i></a>
		</div>
		@if(isset($instaFeeds) && is_array($instaFeeds) && count($instaFeeds))
			<div class="instagram-posts">
				<ul class="instagram-posts-scrolls">
					@foreach($instaFeeds as $instaFeed)
						<li class="instagram-posts-item">
							<a href="{{ $instaFeed['link'] }}" target="_blank">
								<div class="img-wrap" style="background: url('{{ $instaFeed['images']['low_resolution']['url'] }}');">
									<img src="{{ $instaFeed['images']['low_resolution']['url'] }}">
								</div>
							</a>
						</li>
					@endforeach
				</ul>
			</div>
		@endif
	</section>
@endsection