@if(isset($post) && $post->vote && $post->vote_status && (!$post->vote_expire_time || ( $post->vote_expire_time && ($post->vote_expire_time >= date('Y-m-d')))) )
	@php
		$vote = $post->vote;
		$voteData = $post->getVoteCountData();
		$voteTotal = array_sum($voteData);
	@endphp
	<div class="container" style="padding: 0; margin: 2rem 0;">
		<h5 class="font-weight-bold vote-title">緊急アンケート！</h5>
		@if($vote->content)
			<p>{{ str_replace('post_name', $post->title, $vote->content) }}</p>
		@endif
		<form action="{{ route('frontend.post.submitVote', $post) }}" method="POST" id="form-vote">
			{{ csrf_field() }}
			<input type="hidden" class="un-vote" name="un_vote_opt" value="" />
			<div class="vote-option">
				@if(!empty($post->vote->opt_1))
					<div class="vote-select d-flex">
						<input type="radio" name="vote_opt" value="opt_1" id="post-{{ $post->id }}-vote-opt-1">
						<label class="border w-100 ml-2 p-1" for="post-{{ $post->id }}-vote-opt-1">{{ $post->vote->opt_1 }}<span class="float-right">{{ ($voteTotal > 0) ? number_format(($voteData['opt_1']/$voteTotal)*100, 2) : '0' }}%({{ $voteData['opt_1'] }}人)</span></label>
					</div>
				@endif
				@if(!empty($post->vote->opt_2))
					<div class="vote-select d-flex">
						<input type="radio" name="vote_opt" value="opt_2" id="post-{{ $post->id }}-vote-opt-2">
						<label class="border w-100 ml-2 p-1" for="post-{{ $post->id }}-vote-opt-2">{{ $post->vote->opt_2 }}<span class="float-right">{{ ($voteTotal > 0) ? number_format(($voteData['opt_2']/$voteTotal)*100, 2) : '0' }}%({{ $voteData['opt_2'] }}人)</span></label>
					</div>
				@endif
				@if(!empty($post->vote->opt_3))
					<div class="vote-select d-flex">
						<input type="radio" name="vote_opt" value="opt_3" id="post-{{ $post->id }}-vote-opt-3">
						<label class="border w-100 ml-2 p-1" for="post-{{ $post->id }}-vote-opt-3">{{ $post->vote->opt_3 }}<span class="float-right">{{ ($voteTotal > 0) ? number_format(($voteData['opt_3']/$voteTotal)*100, 2) : '0' }}%({{ $voteData['opt_3'] }}人)</span></label>
					</div>
				@endif
				@if(!empty($post->vote->opt_4))
					<div class="vote-select d-flex">
						<input type="radio" name="vote_opt" value="opt_4" id="post-{{ $post->id }}-vote-opt-4">
						<label class="border w-100 ml-2 p-1" for="post-{{ $post->id }}-vote-opt-4">{{ $post->vote->opt_4 }}<span class="float-right">{{ ($voteTotal > 0) ? number_format(($voteData['opt_4']/$voteTotal)*100, 2) : '0' }}%({{ $voteData['opt_4'] }}人)</span></label>
					</div>
				@endif
				@if(!empty($post->vote->opt_5))
					<div class="vote-select d-flex">
						<input type="radio" name="vote_opt" value="opt_5" id="post-{{ $post->id }}-vote-opt-5">
						<label class="border w-100 ml-2" for="post-{{ $post->id }}-vote-opt-5">{{ $post->vote->opt_5 }}<span class="float-right">{{ ($voteTotal > 0) ? number_format(($voteData['opt_5']/$voteTotal)*100, 2) : '0' }}%({{ $voteData['opt_5'] }}人)</span></label>
					</div>
				@endif
			</div>
			<p class="message">みなさま投票をお願いいたします。</p>
		</form>
	</div>
@endif