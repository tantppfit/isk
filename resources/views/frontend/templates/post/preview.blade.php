@extends('frontend.layouts.app')
@section('content')

	<!-- main article details page content -->
	@php
		$social_share_option = (array_key_exists('general_social_share_option', $settings)) ? json_decode($settings['general_social_share_option'], true) : [];
	@endphp
<section class="main">
	
	<div class="main-article-details-page-content">
		<section class="article-page">
			<div class="article-details-img {{($post->cover_image_style) ? $post->cover_image_style : ''}}">
				<div class="resize-img">
				@if($post->featureImage)
					<img class="img-resize" src="{{ Helper::getMediaUrl($post->featureImage, 'original') }}" class="d-block w-100">
				@elseif(Helper::getDefaultCover($post))
					<img class="img-resize" src="{{ Helper::getDefaultCover($post) }}" class="d-block w-100">
				@endif
				@if($post->cover_via_text)
					<a href="{{ (!empty($post->cover_via_href) && substr($post->cover_via_href, 0, 4) != 'http') ? 'http://'.$post->cover_via_href : $post->cover_via_href }}" target="_blank" class="cover-text">{{ $post->cover_via_text }}</a>
				@endif
				</div>
			</div>
			<div class="container-article-page">
				<div class="article-details-page-item">
					<div class="article-details-page-item-header">
						
						<h1>
							@if(!empty($post->vote_id) && array_sum($post->getVoteCountData()) > 0)
								<span class="voted">VOTE</span>
							@endif
							{{ $post->title }}
						</h1>
					</div>

					<div class="article-details-page-item-post col-12">
						<div class="row">
							<div class="post-man-avatar d-flex">
								@if($post->user->avatar)
									<img src="{{ Helper::getUserAvatarUrl($post->user->avatar) }}">
								@else
									<img src="{{ asset('image/no-img.png') }}">
								@endif
								<div style="padding: 8px 0; line-height: 1rem;">
									<div for="" class="post-man-name col-12">
										{{ $post->user->name }}
									</div>
									<div for="" class="post-man-date col-12">
										{{ date('Y-m-d', strtotime($post->created_at)) }}
									</div>
								</div>
							</div>
							<a href="#" class="button-share-post" data-toggle="modal" data-target="#modelSharePost-{{ $post->id }}"><i class="fas fa-share"></i> 共有</a>
						</div>
					</div>

					<div class="article-details-page-tags col-12 row">
						@if(count($post->tags))
							@foreach($post->tags as $tag)
								<div class="article-details-page-tags-item">
									<a href="{{ route('frontend.tag.show', $tag) }}"><i class="fas fa-tag"></i>{{ $tag->name }}</a>
								</div>
							@endforeach
						@endif
					</div>

					@php
						$summary = Helper::getPostSummary($post->content);
					@endphp
					<div class="summary-content {{ (!empty($summary)) ? 'line-bottom' : ''}}">
						@if(!empty($summary))
							<p>Summary</p>
						@endif
						<div class="d-flex justify-content-between">
							<div style="width:95%;">{!! $summary !!}</div>
							
							<div class="sns-share d-flex">
								@if(is_array($social_share_option) && in_array('facebook', $social_share_option))
									<a href="https://facebook.com/sharer/sharer.php?u={{ ($post->slug) ? route('frontend.post.show', $post->slug) : '' }}" target="_blank" class="sns-share-fb text-center"><i class="fab fa-facebook-f"></i></a>
								@endif
								@if(is_array($social_share_option) && in_array('twitter', $social_share_option))
									<a href="https://twitter.com/share?url={{ ($post->slug) ? route('frontend.post.show', $post->slug) : '' }}" target="_blank" class="sns-share-twt text-center"><i class="fab fa-twitter"></i></a>
								@endif
							</div>
						</div>
					</div>

					<div class="clear" style="clear: both;"></div>

					<div id="article-content-{{ $post->id }}" class="article-content post-content ck-content">
						@php
							$content = Helper::generatePostContent($post->content);
						@endphp
						{!! $content !!}
					</div>
					
					<!-- Main Album -->
					@if(isset($mainMedias) && count($mainMedias))
						<div class="main-album">
							<h5 class="heading"><i class="fa fa-picture-o"></i> Media ({{ count($mainMedias) }})</h5>
						  <div class="list-medias">
						  	@foreach($mainMedias as $media)
							    <div class="media-item">
							      <a href="{{ route('frontend.post.album', ['slug' => $post->slug, 'image' => $media->id])}}"><img src="{{ Helper::getMediaUrl($media, 'medium') }}" /></a>
							    </div>
							  @endforeach
						  </div>
						</div>
					@endif
					<!--x- Main Album -->

					<div class="vote-section">
						@include('frontend.templates.post.parts.vote')
					</div>
					
					<div class="article-details-page-item-post col-12 row" style="margin: 2rem 0 1.5rem 0; padding: 0;">
						<div class="post-man-avatar d-flex">
							@if($post->user->avatar)
								<img src="{{ Helper::getUserAvatarUrl($post->user->avatar) }}" />
							@else
								<img src="{{ asset('image/no-img.png') }}" />
							@endif
							<div style="padding: 8px 0; line-height: 1rem;">
								<div for="" class="post-man-name col-12">
									{{ $post->user->name }}
								</div>
								<div for="" class="post-man-date col-12">
									{{ date('Y-m-d', strtotime($post->created_at)) }}
								</div>
							</div>
						</div>
					</div>
					
					<div class="article-details-page-tags col-12 row">
						@if(count($post->tags))
							@foreach($post->tags as $tag)
								<div class="article-details-page-tags-item">
									<a href="{{ route('frontend.tag.show', $tag) }}"><i class="fas fa-tag"></i>{{ $tag->name }}</a>
								</div>
							@endforeach
						@endif
					</div>

					<div class="sns-share-end col-12 row">
						@if(is_array($social_share_option) && in_array('facebook', $social_share_option))
							<a href="https://facebook.com/sharer/sharer.php?u={{ ($post->slug) ? route('frontend.post.show', $post->slug) : '' }}" target="_blank" class="sns-share-fb text-center"><i class="fab fa-facebook-f"></i>Facebook</a>
						@endif
						@if(is_array($social_share_option) && in_array('twitter', $social_share_option))
							<a href="https://twitter.com/share?url={{ ($post->slug) ? route('frontend.post.show', $post->slug) : '' }}" target="_blank" class="sns-share-twt text-center"><i class="fab fa-twitter"></i>Twitter</a>
						@endif
					</div>
				</div>
			</div>
		</section>

		<div class="content-space">
			<a href="#main-header" class="scroll-top">
				<i class="fas fa-chevron-up"></i>
			</a>
		</div>
	</div>

	<!--x-- main article details page content --x-->
</section>
@endsection
@section('header_css')
	<link rel="stylesheet" type="text/css" href="{{ asset('css/frontend/ckeditor-custom-content.css') }}">
@endsection