<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<meta charset="UTF-8">
	<!-- CSRF Token -->
  <meta name="csrf-token" content="{{ csrf_token() }}">
	
	<link rel="stylesheet" type="text/css" href="{{ asset('css/admin/style.css') }}">
	
	<!-- bootstrap -->
	
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
	
	<title>{{ config('app.name', 'Laravel') }}</title>
</head>

<body>
	<div class="admin-login">
		<div class="text-center">
			<!-- <img src="{{ asset('image/logo-black.png') }}"> -->
			@if((new \Jenssegers\Agent\Agent())->isMobile() && isset($settings['general_logo_mobile']) && !empty($settings['general_logo_mobile']))
				<img src="{{ $settings['general_logo_mobile'] }}">
			@elseif(isset($settings['general_logo']) && !empty($settings['general_logo']))
				<img src="{{ $settings['general_logo'] }}">
			@else
				<img src="{{ asset('image/admin_logo.png') }}">
			@endif
		</div>
		@yield('content')
	</div>
	
	<!-- script, bootstrap, ... -->

	<script src="https://kit.fontawesome.com/3973fa3cec.js" crossorigin="anonymous"></script>
	
	<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
	
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
	
	<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
</body>
</html>