@extends('auth.layouts.app')

@section('content')
<form method="POST" action="{{ route('login') }}">
    @csrf
    <div class="admin-login-input col-12">
        <input type="email" placeholder="{{ __('ID') }}" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required />
        @error('email')
            <span class="invalid-feedback" role="alert">
                <strong>{{ $message }}</strong>
            </span>
        @enderror
    </div>
    <div class="admin-login-pass col-12">
        <input type="password" placeholder="{{ __('PASS') }}" class="form-control @error('password') is-invalid @enderror" name="password" required />
        @error('password')
            <span class="invalid-feedback" role="alert">
                <strong>{{ $message }}</strong>
            </span>
        @enderror
    </div>
    <div class="col-12 text-center">
        <button class="btn btn-no-radius admin-login-btn" style="width:160px">{{ __('ログイン') }}</button>
    </div>
</form>
<!-- @if (Route::has('password.request'))
    <div class="col-12 text-center">
        <label class="forget-txt">
            <a href="{{ route('password.request') }}">{{ __('パスワードを忘れた方はこちら') }}</a>
        </label>
    </div>
@endif -->
@endsection