<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMenuTagsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('menu_tags', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('tag_id');
            $table->foreign('tag_id')->references('id')->on('tags')->onDelete('cascade');
            $table->unsignedInteger('order');
            $table->boolean('shown_in_header_menu')->default(false);
            $table->boolean('shown_in_feed_label')->default(false);
            $table->boolean('display_text_enabled')->default(false);
            $table->string('display_detail')->nullable();
            $table->boolean('shown_in_editor')->default(false);
            $table->boolean('reverse_order')->default(false);
            $table->boolean('hide_from_display')->default(false);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('menu_tags');
    }
}
